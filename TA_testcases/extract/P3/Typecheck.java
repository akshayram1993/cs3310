import java.util.*;
import syntaxtree.*;
import visitor.*;

public class Typecheck {
	@SuppressWarnings("rawtypes")
	static HashMap classNames = new HashMap();
	
	@SuppressWarnings({ "static-access", "unchecked", "rawtypes" })
	public static void main(String[] args) {
		try {
			Node root = new MiniJavaParser(System.in).Goal();
			classNames.putAll((HashMap) root.accept(new GJNoArguDepthFirst()));
			isCyclic();
			isOverLoading();
			GJNoArguDepthFirstType typeCheck = new GJNoArguDepthFirstType();
			typeCheck.setClassNames(classNames);
			root.accept(typeCheck);
			System.out.println("Program type checked successfully");
		} catch (ParseException e) {
			System.out.println(e.toString());
		}
	}
	
	@SuppressWarnings("rawtypes")
	public static void isOverLoading() {
		Set set = classNames.entrySet();
		Iterator i = set.iterator();
		while(i.hasNext()) {
			Map.Entry me = (Map.Entry)i.next();
			classEntries c = (classEntries)me.getValue();
			Set set2 = c.getMethods().entrySet();
			Iterator j = set2.iterator();
			while(j.hasNext()) {
				Map.Entry me2 = (Map.Entry)j.next();
				methodEntries m = (methodEntries)me2.getValue();
				while(c.getExtendsClass() != null) {
					c = (classEntries)classNames.get(c.getExtendsClass());
					methodEntries m2 = (methodEntries)c.getMethods().get(me2.getKey());
					
					if(m2 != null) {
						Vector p1 = m.getParams();
						Vector p2 = m2.getParams();
						
						if(p1.size() == p2.size()) {
							Enumeration vEnum1 = p1.elements();
							Enumeration vEnum2 = p2.elements();
						    while(vEnum1.hasMoreElements()) {
						    	variableEntries v1 = (variableEntries)vEnum1.nextElement();
						    	variableEntries v2 = (variableEntries)vEnum2.nextElement();
						    	
						    	// if(!isSubType(v2.getType(), v1.getType())) {
						    	if(!v2.getType().equals(v1.getType())) {
						    		System.out.println("Type error");
								System.exit(0);
						    	}
						    }
						    
						    if(!isSubType(m2.getRetType(), m.getRetType())) {
					    		System.out.println("Type error");
								System.exit(0);
					    	}
						} else {
							System.out.println("Type error");
							System.exit(0);
						}
					}
 				}
			}
		}
	}
	
	public static boolean isSubType(String t1, String t2) {
		if(t1.equals(t2)) {
			return true;
		}
		
		if(t1.equals("int[]") || t1.equals("int") || t1.equals("boolean") || t2.equals("int[]") || t2.equals("int") || t2.equals("boolean")) {
			return false;
		}
		
		classEntries c = (classEntries)classNames.get(t2);
		
		while(c.getExtendsClass() != null) {
			if(c.getExtendsClass().equals(t1)) {
				return true;
			}
			
			c = (classEntries)classNames.get(c.getExtendsClass());
		}
		
		return false;
	}
	
	@SuppressWarnings("rawtypes")
	public static void isCyclic() {
		Set set = classNames.entrySet();
		Iterator i = set.iterator();
		while(i.hasNext()) {
			Map.Entry me = (Map.Entry)i.next();
			classEntries c = (classEntries)me.getValue();
			String curClass = (String)me.getKey();
			while(c.getExtendsClass() != null) {
				if(c.getExtendsClass().equals(curClass)) {
					System.out.println("Type error");
					System.exit(0);
				}
				c = (classEntries)classNames.get(c.getExtendsClass());
				if(c == null) {
					System.out.println("Type error");
					System.exit(0);
				}
			}
		}
	}	
}
