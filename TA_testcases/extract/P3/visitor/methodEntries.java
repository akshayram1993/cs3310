package visitor;
import java.util.*;

public class methodEntries {
	@SuppressWarnings("rawtypes")
	Vector params = new Vector(1);
	@SuppressWarnings("rawtypes")
	HashMap localVars = new HashMap();
	String retType;
	
	@SuppressWarnings("rawtypes")
	public Vector getParams() {
		return params;
	}
	@SuppressWarnings("rawtypes")
	public void setParams(Vector v) {
		this.params = v;
	}
	@SuppressWarnings("rawtypes")
	public HashMap getLocalVars() {
		return localVars;
	}
	@SuppressWarnings("rawtypes")
	public void setLocalVars(HashMap localVars) {
		this.localVars = localVars;
	}
	public String getRetType() {
		return retType;
	}
	public void setRetType(String retType) {
		this.retType = retType;
	}
}
