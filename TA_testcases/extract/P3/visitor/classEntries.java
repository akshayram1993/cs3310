package visitor;
import java.util.*;

public class classEntries {
	String extendsClass;
	@SuppressWarnings("rawtypes")
	HashMap localVars = new HashMap();
	@SuppressWarnings("rawtypes")
	HashMap methods = new HashMap();
	
	public String getExtendsClass() {
		return extendsClass;
	}
	public void setExtendsClass(String extendsClass) {
		this.extendsClass = extendsClass;
	}
	@SuppressWarnings("rawtypes")
	public HashMap getLocalVars() {
		return localVars;
	}
	@SuppressWarnings("rawtypes")
	public void setLocalVars(HashMap localVars) {
		this.localVars = localVars;
	}
	@SuppressWarnings("rawtypes")
	public HashMap getMethods() {
		return methods;
	}
	@SuppressWarnings("rawtypes")
	public void setMethods(HashMap methods) {
		this.methods = methods;
	}
}
