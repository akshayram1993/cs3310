declare -f FILES
declare -f i

ant build

FILES=tests/*.microIR
echo hello
for i in $FILES
do
	java P5 <$i >out.miniRA
	java -jar kgi.jar <out.miniRA >out.text
	echo $(basename ${i::-8})
	DIFF= `diff -B -w -i out.text tests/$(basename ${i::-8}).out`
	if [ "$DIFF" == "" ]
	then
		echo Pass
	fi
done

ant clean