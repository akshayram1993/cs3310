.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $t0, 36
move $a0, $t0
jal _halloc
move $t1, $v0
move $t2, $t1
li $t3, 32
move $a0, $t3
jal _halloc
move $t4, $v0
move $t5, $t4
move $t6, $t2
la $t7, __MYTRANSLATE__C23__setI1__
sw $t7, 32($t6)
move $t8, $t2
la $t9, __MYTRANSLATE__C23__init__
sw $t9, 24($t8)
move $s0, $t2
la $s1, __MYTRANSLATE__C23__getI1__
sw $s1, 28($s0)
move $s2, $t2
la $s3, __MYTRANSLATE__C23__setI1__
sw $s3, 20($s2)
move $s4, $t2
la $s5, __MYTRANSLATE__C23__init__
sw $s5, 12($s4)
move $s6, $t2
la $s7, __MYTRANSLATE__C23__getI1__
sw $s7, 16($s6)
move $t0, $t2
la $t1, __MYTRANSLATE__C23__setI1__
sw $t1, 8($t0)
move $t3, $t2
la $t4, __MYTRANSLATE__C23__init__
sw $t4, 0($t3)
move $t6, $t2
la $t7, __MYTRANSLATE__C23__getI1__
sw $t7, 4($t6)
li $t8, 4
move $t9, $t8
MAINL0: 
move $s0, $t9
li $s1, 32
slt $v0, $s0, $s1
move $s2, $v0
beqz $s2, MAINL1
move $s3, $t5
move $s4, $t9
addu $v0, $s3, $s4
move $s5, $v0
li $s6, 0
sw $s6, 0($s5)
move $s7, $t9
li $t0, 4
addu $v0, $s7, $t0
move $t1, $v0
move $t9, $t1
b MAINL0
MAINL1: 
move $t3, $t5
move $t4, $t2
sw $t4, 0($t3)
move $t6, $t5
move $t7, $t6
move $t8, $t7
move $s0, $t8
lw  $s1, 0($s0)
move $s2, $s1
lw  $s3, 24($s2)
move $s4, $s3
move $s5, $s4
move $s6, $t8
li $s7, 24
move $a0, $s7
jal _halloc
move $t0, $v0
move $t1, $t0
li $t9, 24
move $a0, $t9
jal _halloc
move $t2, $v0
move $t3, $t2
move $t4, $t1
la $t5, __MYTRANSLATE__B23__setI1__
sw $t5, 20($t4)
move $t6, $t1
la $t7, __MYTRANSLATE__B23__init__
sw $t7, 12($t6)
move $s0, $t1
la $s1, __MYTRANSLATE__B23__getI1__
sw $s1, 16($s0)
move $s2, $t1
la $s3, __MYTRANSLATE__B23__setI1__
sw $s3, 8($s2)
move $s4, $t1
la $t8, __MYTRANSLATE__B23__init__
sw $t8, 0($s4)
move $s7, $t1
la $t0, __MYTRANSLATE__B23__getI1__
sw $t0, 4($s7)
li $t9, 4
move $t2, $t9
MAINL2: 
move $t4, $t2
li $t5, 24
slt $v0, $t4, $t5
move $t6, $v0
beqz $t6, MAINL3
move $t7, $t3
move $s0, $t2
addu $v0, $t7, $s0
move $s1, $v0
li $s2, 0
sw $s2, 0($s1)
move $s3, $t2
li $s4, 4
addu $v0, $s3, $s4
move $t8, $v0
move $t2, $t8
b MAINL2
MAINL3: 
move $s7, $t3
move $t0, $t1
sw $t0, 0($s7)
move $t9, $t3
move $t4, $t9
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s6
move $a1, $t4
jalr $s5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t5, $v0
move $a0 ,$t5
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	__MYTRANSLATE__A23__init__
__MYTRANSLATE__A23__init__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a0
move $t1, $a1
move $t2, $t0
move $t3, $t1
move $t4, $t3
move $t5, $t4
lw  $t6, 0($t5)
move $t7, $t6
lw  $t8, 4($t7)
move $t9, $t8
move $s0, $t9
move $s1, $t4
li $s2, 24
move $a0, $s2
jal _halloc
move $s3, $v0
move $s4, $s3
li $s5, 24
move $a0, $s5
jal _halloc
move $s6, $v0
move $s7, $s6
move $t1, $s4
la $t3, __MYTRANSLATE__B23__setI1__
sw $t3, 20($t1)
move $t5, $s4
la $t6, __MYTRANSLATE__B23__init__
sw $t6, 12($t5)
move $t7, $s4
la $t8, __MYTRANSLATE__B23__getI1__
sw $t8, 16($t7)
move $t9, $s4
la $t4, __MYTRANSLATE__B23__setI1__
sw $t4, 8($t9)
move $s2, $s4
la $s3, __MYTRANSLATE__B23__init__
sw $s3, 0($s2)
move $s5, $s4
la $s6, __MYTRANSLATE__B23__getI1__
sw $s6, 4($s5)
li $t1, 4
move $t3, $t1
__MYTRANSLATE__A23__init__L2: 
move $t5, $t3
li $t6, 24
slt $v0, $t5, $t6
move $t7, $v0
beqz $t7, __MYTRANSLATE__A23__init__L3
move $t8, $s7
move $t9, $t3
addu $v0, $t8, $t9
move $t4, $v0
li $s2, 0
sw $s2, 0($t4)
move $s3, $t3
li $s5, 4
addu $v0, $s3, $s5
move $s6, $v0
move $t3, $s6
b __MYTRANSLATE__A23__init__L2
__MYTRANSLATE__A23__init__L3: 
move $t1, $s7
move $t5, $s4
sw $t5, 0($t1)
move $t6, $s7
move $t7, $t6
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s1
move $a1, $t7
jalr $s0
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t8, $v0
sw $t8, 8($t2)
move $t9, $t0
li $t4, 222
sw $t4, 12($t9)
move $s2, $t0
move $s3, $t0
move $s5, $s3
move $s6, $s5
lw  $t3, 0($s6)
move $s4, $t3
lw  $t1, 8($s4)
move $t5, $t1
move $s7, $t5
move $t6, $s5
move $s1, $t0
lw  $s0, 8($s1)
move $t7, $s0
move $t2, $t7
move $t8, $t0
lw  $t9, 12($t8)
move $t4, $t9
move $s3, $t4
addu $v0, $t2, $s3
move $s6, $v0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t6
move $a1, $s6
jalr $s7
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t3, $v0
sw $t3, 4($s2)
move $s4, $t0
lw  $t1, 4($s4)
move $t5, $t1
move $s5, $t5
move $s1, $s5
move $v0, $s1
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__A23__getI1__
__MYTRANSLATE__A23__getI1__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1, $a0
move $t0, $t1
lw  $t2, 4($t0)
move $t3, $t2
move $t4, $t3
move $t5, $t4
move $v0, $t5
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__A23__setI1__
__MYTRANSLATE__A23__setI1__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1, $a1
move $t0, $t1
move $t2, $t0
move $v0, $t2
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__B23__init__
__MYTRANSLATE__B23__init__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a0
move $t1, $a1
li $t2, 12
move $a0, $t2
jal _halloc
move $t3, $v0
move $t4, $t3
li $t5, 16
move $a0, $t5
jal _halloc
move $t6, $v0
move $t7, $t6
move $t8, $t4
la $t9, __MYTRANSLATE__A23__setI1__
sw $t9, 8($t8)
move $s0, $t4
la $s1, __MYTRANSLATE__A23__init__
sw $s1, 0($s0)
move $s2, $t4
la $s3, __MYTRANSLATE__A23__getI1__
sw $s3, 4($s2)
li $s4, 4
move $s5, $s4
__MYTRANSLATE__B23__init__L4: 
move $s6, $s5
li $s7, 16
slt $v0, $s6, $s7
move $t2, $v0
beqz $t2, __MYTRANSLATE__B23__init__L5
move $t3, $t7
move $t5, $s5
addu $v0, $t3, $t5
move $t6, $v0
li $t8, 0
sw $t8, 0($t6)
move $t9, $s5
li $s0, 4
addu $v0, $t9, $s0
move $s1, $v0
move $s5, $s1
b __MYTRANSLATE__B23__init__L4
__MYTRANSLATE__B23__init__L5: 
move $s2, $t7
move $s3, $t4
sw $s3, 0($s2)
move $s4, $t7
move $s6, $s4
move $s7, $s6
move $t2, $t0
move $t3, $t1
move $t5, $t3
move $t6, $t5
lw  $t8, 0($t6)
move $t9, $t8
lw  $s0, 4($t9)
move $s1, $s0
move $s5, $s1
move $t4, $t5
move $s2, $t0
lw  $s3, 8($s2)
move $t7, $s3
move $s4, $t7
move $s6, $t0
lw  $t1, 12($s6)
move $t3, $t1
move $t6, $t3
addu $v0, $s4, $t6
move $t8, $v0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t4
move $a1, $t8
jalr $s5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t9, $v0
sw $t9, 20($t2)
move $s0, $t0
move $s1, $t0
move $t5, $s1
move $s2, $t5
lw  $s3, 0($s2)
move $t7, $s3
lw  $s6, 20($t7)
move $t1, $s6
move $t3, $t1
move $s4, $t5
move $t6, $t0
lw  $s5, 20($t6)
move $t4, $s5
move $t8, $t4
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s4
move $a1, $t8
jalr $t3
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t2, $v0
sw $t2, 16($s0)
move $t9, $s7
move $s1, $t9
move $s2, $s1
lw  $s3, 0($s2)
move $t7, $s3
lw  $s6, 0($t7)
move $t1, $s6
move $t5, $t1
move $t6, $s1
move $s5, $t0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t6
move $a1, $s5
jalr $t5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t4, $v0
move $s4, $t4
move $v0, $s4
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__B23__getI1__
__MYTRANSLATE__B23__getI1__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1, $a0
move $t0, $t1
lw  $t2, 16($t0)
move $t3, $t2
move $t4, $t3
move $t5, $t4
move $v0, $t5
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__B23__setI1__
__MYTRANSLATE__B23__setI1__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a1
move $t1, $t0
li $t2, 111
addu $v0, $t1, $t2
move $t3, $v0
move $t4, $t3
move $v0, $t4
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__C23__init__
__MYTRANSLATE__C23__init__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a0
move $t1, $a1
move $t2, $t0
li $t3, 333
sw $t3, 28($t2)
move $t4, $t0
move $t5, $t0
move $t6, $t5
move $t7, $t6
lw  $t8, 0($t7)
move $t9, $t8
lw  $s0, 32($t9)
move $s1, $s0
move $s2, $s1
move $s3, $t6
move $s4, $t0
lw  $s5, 28($s4)
move $s6, $s5
move $s7, $s6
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s3
move $a1, $s7
jalr $s2
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t2, $v0
sw $t2, 24($t4)
move $t3, $t1
move $t5, $t3
move $t7, $t5
lw  $t8, 0($t7)
move $t9, $t8
lw  $s0, 0($t9)
move $s1, $s0
move $t6, $s1
move $s4, $t5
move $s5, $t0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s4
move $a1, $s5
jalr $t6
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s6, $v0
move $s3, $s6
move $v0, $s3
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__C23__getI1__
__MYTRANSLATE__C23__getI1__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1, $a0
move $t0, $t1
lw  $t2, 24($t0)
move $t3, $t2
move $t4, $t3
move $t5, $t4
move $v0, $t5
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__C23__setI1__
__MYTRANSLATE__C23__setI1__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a1
move $t1, $t0
li $t2, 2
mult $t1, $t2
mflo $v0
move $t3, $v0
move $t4, $t3
move $v0, $t4
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
