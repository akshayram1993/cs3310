.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $a0, 1
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	Finder_Find
Finder_Find:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
li $t0, 10
li $v1, 1
addu $v0, $t0, $v1
move $t1, $v0
li $v1, 4
mult $t1, $v1
mflo $v0
move $t2, $v0
move $a0, $t2
jal _halloc
move $t3, $v0
move $t4, $t3
li $t5, 4
Finder_FindL0: 
nop
li $v1, 1
addu $v0, $t0, $v1
move $t6, $v0
li $v1, 4
mult $t6, $v1
mflo $v0
move $t7, $v0
slt $v0, $t5, $t7
move $t8, $v0
beqz $t8, Finder_FindL1
addu $v0, $t4, $t5
move $t9, $v0
li $s0, 0
sw $s0, 0($t9)
li $v1, 4
addu $v0, $t5, $v1
move $s1, $v0
move $t5, $s1
b Finder_FindL0
Finder_FindL1: 
nop
sw $t0, 0($t4)
li $s2, 10
li $v1, 1
addu $v0, $s2, $v1
move $s3, $v0
li $v1, 4
mult $s3, $v1
mflo $v0
move $s4, $v0
move $a0, $s4
jal _halloc
move $s5, $v0
move $s6, $s5
li $s7, 4
Finder_FindL2: 
nop
li $v1, 1
addu $v0, $s2, $v1
move $t1, $v0
li $v1, 4
mult $t1, $v1
mflo $v0
move $t2, $v0
slt $v0, $s7, $t2
move $t3, $v0
beqz $t3, Finder_FindL3
addu $v0, $s6, $s7
move $t6, $v0
li $t7, 0
sw $t7, 0($t6)
li $v1, 4
addu $v0, $s7, $v1
move $t8, $v0
move $s7, $t8
b Finder_FindL2
Finder_FindL3: 
nop
sw $s2, 0($s6)
move $t9, $s6
li $s0, 0
move $v0, $s0
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
