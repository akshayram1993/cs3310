.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $a0, 4
jal _halloc
move $t0, $v0
li $a0, 4
jal _halloc
move $t1, $v0
la $t2, simpleLookup_look
sw $t2, 0($t0)
sw $t0, 0($t1)
move $t3, $t1
lw  $t4, 0($t3)
lw  $t5, 0($t4)
li $t6, 2
li $t7, 4
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t3
move $a1, $t6
move $a2, $t7
jalr $t5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t8, $v0
move $a0 ,$t8
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	simpleLookup_look
simpleLookup_look:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1, $a1
move $t2, $a2
move $t0, $t2
li $v1, 1
addu $v0, $t0, $v1
move $t3, $v0
li $v1, 4
mult $t3, $v1
mflo $v0
move $t4, $v0
move $a0, $t4
jal _halloc
move $t5, $v0
move $t6, $t5
li $t7, 4
simpleLookup_lookL0: 
nop
li $v1, 1
addu $v0, $t0, $v1
move $t8, $v0
li $v1, 4
mult $t8, $v1
mflo $v0
move $t9, $v0
slt $v0, $t7, $t9
move $s0, $v0
beqz $s0, simpleLookup_lookL1
addu $v0, $t6, $t7
move $s1, $v0
li $s2, 0
sw $s2, 0($s1)
li $v1, 4
addu $v0, $t7, $v1
move $s3, $v0
move $t7, $s3
b simpleLookup_lookL0
simpleLookup_lookL1: 
nop
sw $t0, 0($t6)
move $s4, $t6
move $s5, $s4
move $s6, $t1
li $v1, 0
slt $v0, $s6, $v1
move $s7, $v0
beqz $s7, simpleLookup_lookL2
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
simpleLookup_lookL2: 
nop
lw  $t2, 0($s5)
slt $v0, $s6, $t2
move $t3, $v0
li $t4, 1
subu $v0, $t4, $t3
move $t5, $v0
beqz $t5, simpleLookup_lookL3
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
simpleLookup_lookL3: 
nop
li $t8, 4
mult $t8, $s6
mflo $v0
move $t9, $v0
move $s6, $t9
li $v1, 4
addu $v0, $s6, $v1
move $s0, $v0
addu $v0, $s5, $s0
move $s1, $v0
li $s2, 6
sw $s2, 0($s1)
move $s3, $s4
move $t7, $t1
li $v1, 0
slt $v0, $t7, $v1
move $t0, $v0
beqz $t0, simpleLookup_lookL4
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
simpleLookup_lookL4: 
nop
lw  $t6, 0($s3)
slt $v0, $t7, $t6
move $s7, $v0
li $t2, 1
subu $v0, $t2, $s7
move $t3, $v0
beqz $t3, simpleLookup_lookL5
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
simpleLookup_lookL5: 
nop
li $t4, 4
mult $t4, $t7
mflo $v0
move $t5, $v0
move $t7, $t5
li $v1, 4
addu $v0, $t7, $v1
move $t8, $v0
addu $v0, $s3, $t8
move $t9, $v0
lw  $s6, 0($t9)
move $s5, $s6
move $v0, $s5
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
