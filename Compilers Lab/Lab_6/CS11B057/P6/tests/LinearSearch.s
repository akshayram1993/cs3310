.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $a0, 16
jal _halloc
move $t0, $v0
li $a0, 12
jal _halloc
move $t1, $v0
la $t2, LS_Init
sw $t2, 12($t0)
la $t3, LS_Search
sw $t3, 8($t0)
la $t4, LS_Print
sw $t4, 4($t0)
la $t5, LS_Start
sw $t5, 0($t0)
li $t6, 4
MAINL0: 
nop
li $t7, 12
slt $v0, $t6, $t7
move $t8, $v0
beqz $t8, MAINL1
addu $v0, $t1, $t6
move $t9, $v0
li $s0, 0
sw $s0, 0($t9)
li $v1, 4
addu $v0, $t6, $v1
move $t6, $v0
b MAINL0
MAINL1: 
nop
sw $t0, 0($t1)
move $s1, $t1
lw  $s2, 0($s1)
lw  $s3, 0($s2)
li $s4, 10
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s1
move $a1, $s4
jalr $s3
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s5, $v0
move $a0 ,$s5
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	LS_Start
LS_Start:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a0
move $t1, $a1
move $t2, $t0
lw  $t3, 0($t2)
lw  $t4, 12($t3)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t2
move $a1, $t1
jalr $t4
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t5, $v0
move $t6, $t0
lw  $t7, 0($t6)
lw  $t8, 4($t7)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t6
jalr $t8
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t9, $v0
li $s0, 9999
move $a0 ,$s0
jal _print
move $s1, $t0
lw  $s2, 0($s1)
lw  $s3, 8($s2)
li $s4, 8
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s1
move $a1, $s4
jalr $s3
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s5, $v0
move $a0 ,$s5
jal _print
move $s6, $t0
lw  $s7, 0($s6)
lw  $t3, 8($s7)
li $t2, 12
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s6
move $a1, $t2
jalr $t3
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t1, $v0
move $a0 ,$t1
jal _print
move $t4, $t0
lw  $t5, 0($t4)
lw  $t7, 8($t5)
li $t6, 17
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t4
move $a1, $t6
jalr $t7
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t8, $v0
move $a0 ,$t8
jal _print
move $t9, $t0
lw  $s0, 0($t9)
lw  $s2, 8($s0)
li $s1, 50
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t9
move $a1, $s1
jalr $s2
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s3, $v0
move $a0 ,$s3
jal _print
li $s4, 55
move $v0, $s4
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	LS_Print
LS_Print:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a0
li $t1, 1
LS_PrintL2: 
nop
lw  $t2, 8($t0)
slt $v0, $t1, $t2
move $t3, $v0
beqz $t3, LS_PrintL3
lw  $t5, 4($t0)
li $v1, 4
mult $t1, $v1
mflo $v0
move $t4, $v0
lw  $t5, 4($t0)
lw  $t6, 0($t5)
li $t7, 1
slt $v0, $t4, $t6
move $t8, $v0
subu $v0, $t7, $t8
move $t9, $v0
beqz $t9, LS_PrintL4
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
LS_PrintL4: 
nop
li $s0, 4
move $s1, $s0
addu $v0, $t4, $s1
move $s2, $v0
move $s3, $s2
addu $v0, $t5, $s3
move $s4, $v0
lw  $s5, 0($s4)
move $a0 ,$s5
jal _print
li $v1, 1
addu $v0, $t1, $v1
move $t1, $v0
b LS_PrintL2
LS_PrintL3: 
nop
li $s6, 0
move $v0, $s6
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	LS_Search
LS_Search:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a0
move $t1, $a1
li $t2, 1
li $t3, 0
LS_SearchL5: 
nop
lw  $t4, 8($t0)
slt $v0, $t2, $t4
move $t5, $v0
beqz $t5, LS_SearchL6
lw  $t7, 4($t0)
li $v1, 4
mult $t2, $v1
mflo $v0
move $t6, $v0
lw  $t7, 4($t0)
lw  $t8, 0($t7)
li $t9, 1
slt $v0, $t6, $t8
move $s0, $v0
subu $v0, $t9, $s0
move $s1, $v0
beqz $s1, LS_SearchL7
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
LS_SearchL7: 
nop
li $s2, 4
move $s3, $s2
addu $v0, $t6, $s3
move $s4, $v0
move $s5, $s4
addu $v0, $t7, $s5
move $s6, $v0
lw  $s7, 0($s6)
move $t4, $s7
li $v1, 1
addu $v0, $t1, $v1
move $t5, $v0
slt $v0, $t4, $t1
move $t8, $v0
beqz $t8, LS_SearchL8
b LS_SearchL9
LS_SearchL8: 
nop
li $t9, 1
slt $v0, $t4, $t5
move $s0, $v0
subu $v0, $t9, $s0
move $s1, $v0
beqz $s1, LS_SearchL10
b LS_SearchL11
LS_SearchL10: 
nop
li $t3, 1
lw  $s2, 8($t0)
move $t2, $s2
LS_SearchL11: 
nop
LS_SearchL9: 
nop
li $v1, 1
addu $v0, $t2, $v1
move $t2, $v0
b LS_SearchL5
LS_SearchL6: 
nop
move $v0, $t3
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	LS_Init
LS_Init:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a0
move $t1, $a1
sw $t1, 8($t0)
li $v1, 1
addu $v0, $t1, $v1
move $t2, $v0
li $t3, 4
mult $t2, $t3
mflo $v0
move $t4, $v0
move $a0, $t4
jal _halloc
move $t5, $v0
li $t6, 4
LS_InitL12: 
nop
li $t7, 1
addu $v0, $t1, $t7
move $t8, $v0
li $t9, 4
move $s0, $t9
mult $t8, $s0
mflo $v0
move $s1, $v0
slt $v0, $t6, $s1
move $s2, $v0
beqz $s2, LS_InitL13
addu $v0, $t5, $t6
move $s3, $v0
li $s4, 0
sw $s4, 0($s3)
li $v1, 4
addu $v0, $t6, $v1
move $t6, $v0
b LS_InitL12
LS_InitL13: 
nop
li $s5, 4
mult $t1, $s5
mflo $v0
move $s6, $v0
sw $s6, 0($t5)
sw $t5, 4($t0)
li $s7, 1
lw  $t2, 8($t0)
li $t3, 1
addu $v0, $t2, $t3
move $t4, $v0
LS_InitL14: 
nop
lw  $t7, 8($t0)
slt $v0, $s7, $t7
move $t9, $v0
beqz $t9, LS_InitL15
li $t8, 2
mult $t8, $s7
mflo $v0
move $s0, $v0
li $v1, 3
subu $v0, $t4, $v1
move $s1, $v0
li $s2, 1
li $v1, 4
mult $s2, $v1
mflo $v0
move $s3, $v0
addu $v0, $t0, $s3
move $s4, $v0
lw  $s6, 0($s4)
li $v1, 4
mult $s7, $v1
mflo $v0
move $t6, $v0
li $t1, 1
li $v1, 4
mult $t1, $v1
mflo $v0
move $s3, $v0
addu $v0, $t0, $s3
move $s5, $v0
lw  $s6, 0($s5)
lw  $t5, 0($s6)
li $t2, 1
slt $v0, $t6, $t5
move $t3, $v0
subu $v0, $t2, $t3
move $t7, $v0
beqz $t7, LS_InitL16
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
LS_InitL16: 
nop
li $t9, 4
move $t8, $t9
addu $v0, $t6, $t8
move $s2, $v0
move $s4, $s2
addu $v0, $s6, $s4
move $t1, $v0
addu $v0, $s0, $s1
move $s3, $v0
sw $s3, 0($t1)
li $v1, 1
addu $v0, $s7, $v1
move $s7, $v0
li $v1, 1
subu $v0, $t4, $v1
move $t4, $v0
b LS_InitL14
LS_InitL15: 
nop
li $s5, 0
move $v0, $s5
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
