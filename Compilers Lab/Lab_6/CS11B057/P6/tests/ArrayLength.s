.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $a0, 4
jal _halloc
move $t0, $v0
li $a0, 4
jal _halloc
move $t1, $v0
la $t2, Fac_Compute
sw $t2, 0($t0)
sw $t0, 0($t1)
move $t3, $t1
lw  $t4, 0($t3)
lw  $t5, 0($t4)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t3
jalr $t5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t6, $v0
move $a0 ,$t6
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	Fac_Compute
Fac_Compute:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
li $t0, 10
li $v1, 1
addu $v0, $t0, $v1
move $t1, $v0
li $v1, 4
mult $t1, $v1
mflo $v0
move $t2, $v0
move $a0, $t2
jal _halloc
move $t3, $v0
move $t4, $t3
li $t5, 4
Fac_ComputeL0: 
nop
li $v1, 1
addu $v0, $t0, $v1
move $t6, $v0
li $v1, 4
mult $t6, $v1
mflo $v0
move $t7, $v0
slt $v0, $t5, $t7
move $t8, $v0
beqz $t8, Fac_ComputeL1
addu $v0, $t4, $t5
move $t9, $v0
li $s0, 0
sw $s0, 0($t9)
li $v1, 4
addu $v0, $t5, $v1
move $s1, $v0
move $t5, $s1
b Fac_ComputeL0
Fac_ComputeL1: 
nop
sw $t0, 0($t4)
move $s2, $t4
move $s3, $s2
lw  $s4, 0($s3)
move $v0, $s4
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
