declare -f FILES
declare -f i

javac P6.java

FILES=tests/*.miniRA
echo hello
for i in $FILES
do
	java P6 <$i >tests/$(basename ${i::-7}).s
	

	spim -file tests/$(basename ${i::-7}).s | awk 'NR > 5'>out.text

	echo $(basename ${i::-7})
	cd debug
	
	java Main <../tests/$(basename ${i::-7}).s
	cd ..

	DIFF= `diff -B -w -i out.text tests/$(basename ${i::-7}).out`
	if [ "$DIFF" == "" ]
	then
		echo Pass
	else
		echo Fail
	fi
done

# ant clean