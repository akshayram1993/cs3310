class Factorial{
    public static void main(String[] a){
        System.out.println(new Fac1().foo(10));
    }
}

class Fac {
	int x;
    public int ComputeFac(int num){
        int num_aux ;
        if (num < 1)
            num_aux = 1 ;
        else
            num_aux = num * (this.ComputeFac(num-1)) ;
        return num_aux ;
    }
    public int bar(int num) {
    	x = 1;
    	return x;
    }
}
class Fac1 extends Fac {
	int x;
	int y;
    public int foo(int num){
       x = 2;
       y = this.bar(10);
       return x;
    }
}