import syntaxtree.*;
import visitor.*;

public class Main {
   public static void main(String [] args) {
      try {
         Node root = new MiniJavaParser(System.in).Goal();
        // System.out.println("Program parsed successfully");
         FirstVisitor f = new FirstVisitor();
         root.accept(f); // Your assignment part is invoked here.
         
         FVisitor s = new FVisitor();
    	 s.classes = f.classes;
    	 root.accept(s);
        
      }
      catch (ParseException e) {
         System.out.println(e.toString());
      }
   }
} 



