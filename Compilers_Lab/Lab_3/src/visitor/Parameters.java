package visitor;
import java.util.*;

public class Parameters {
	public String idName;
	public String type;
	public Parameters(String idName, String type) {
		this.idName = idName;
		this.type = type;
	}
}