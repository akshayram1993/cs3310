class Factorial{
    public static void main(String[] a){
        System.out.println(new Fac1().foo(10));
    }
}

class Fac {
    public int ComputeFac(int num){
        int num_aux ;
        if (num < 1)
            num_aux = 1 ;
        else
            num_aux = num * (this.ComputeFac(num-1)) ;
        return num_aux ;
    }
}
class Fac1 extends Fac {
    public int foo(int num){
        return (this.ComputeFac(10));
    }
}