.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $t0, 8
move $a0, $t0
jal _halloc
move $t1, $v0
move $t2, $t1
li $t3, 8
move $a0, $t3
jal _halloc
move $t4, $v0
move $t5, $t4
move $t6, $t2
la $t7, __MYTRANSLATE__Test__start__
sw $t7, 0($t6)
move $t8, $t2
la $t9, __MYTRANSLATE__Test__next__
sw $t9, 4($t8)
li $s0, 4
move $s1, $s0
MAINL0: 
move $s2, $s1
li $s3, 8
slt $v0, $s2, $s3
move $s4, $v0
beqz $s4, MAINL1
move $s5, $t5
move $s6, $s1
addu $v0, $s5, $s6
move $s7, $v0
li $t0, 0
sw $t0, 0($s7)
move $t1, $s1
li $t3, 4
addu $v0, $t1, $t3
move $t4, $v0
move $s1, $t4
b MAINL0
MAINL1: 
move $t6, $t5
move $t7, $t2
sw $t7, 0($t6)
move $t8, $t5
move $t9, $t8
move $s0, $t9
move $s2, $s0
lw  $s3, 0($s2)
move $s4, $s3
lw  $s5, 0($s4)
move $s6, $s5
move $s7, $s6
move $t0, $s0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t0
jalr $s7
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t1, $v0
move $a0 ,$t1
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	__MYTRANSLATE__Test__start__
__MYTRANSLATE__Test__start__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1, $a0
move $t0, $t1
li $t2, 10
sw $t2, 4($t0)
li $t3, 8
move $a0, $t3
jal _halloc
move $t4, $v0
move $t5, $t4
li $t6, 8
move $a0, $t6
jal _halloc
move $t7, $v0
move $t8, $t7
move $t9, $t5
la $s0, __MYTRANSLATE__Test__start__
sw $s0, 0($t9)
move $s1, $t5
la $s2, __MYTRANSLATE__Test__next__
sw $s2, 4($s1)
li $s3, 4
move $s4, $s3
__MYTRANSLATE__Test__start__L2: 
move $s5, $s4
li $s6, 8
slt $v0, $s5, $s6
move $s7, $v0
beqz $s7, __MYTRANSLATE__Test__start__L3
move $t0, $t8
move $t2, $s4
addu $v0, $t0, $t2
move $t3, $v0
li $t4, 0
sw $t4, 0($t3)
move $t6, $s4
li $t7, 4
addu $v0, $t6, $t7
move $t9, $v0
move $s4, $t9
b __MYTRANSLATE__Test__start__L2
__MYTRANSLATE__Test__start__L3: 
move $s0, $t8
move $s1, $t5
sw $s1, 0($s0)
move $s2, $t8
move $s3, $s2
move $s5, $s3
move $s6, $t1
move $s7, $s5
move $t0, $s7
move $t2, $t0
lw  $t3, 0($t2)
move $t4, $t3
lw  $t6, 4($t4)
move $t7, $t6
move $t9, $t7
move $s4, $t0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s4
jalr $t9
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t5, $v0
sw $t5, 4($s6)
move $s0, $t1
lw  $s1, 4($s0)
move $t8, $s1
move $s2, $t8
move $s3, $s2
move $v0, $s3
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__Test__next__
__MYTRANSLATE__Test__next__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
li $t0, 20
move $t1, $t0
move $v0, $t1
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
