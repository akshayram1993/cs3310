.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $t0, 4
move $a0, $t0
jal _halloc
move $t1, $v0
move $t2, $t1
li $t3, 8
move $a0, $t3
jal _halloc
move $t4, $v0
move $t5, $t4
move $t6, $t2
la $t7, __MYTRANSLATE__Test__start__
sw $t7, 0($t6)
li $t8, 4
move $t9, $t8
MAINL0: 
move $s0, $t9
li $s1, 8
slt $v0, $s0, $s1
move $s2, $v0
beqz $s2, MAINL1
move $s3, $t5
move $s4, $t9
addu $v0, $s3, $s4
move $s5, $v0
li $s6, 0
sw $s6, 0($s5)
move $s7, $t9
li $t0, 4
addu $v0, $s7, $t0
move $t1, $v0
move $t9, $t1
b MAINL0
MAINL1: 
move $t3, $t5
move $t4, $t2
sw $t4, 0($t3)
move $t6, $t5
move $t7, $t6
move $t8, $t7
move $s0, $t8
lw  $s1, 0($s0)
move $s2, $s1
lw  $s3, 0($s2)
move $s4, $s3
move $s5, $s4
move $s6, $t8
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s6
jalr $s5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s7, $v0
move $a0 ,$s7
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	__MYTRANSLATE__Test__start__
__MYTRANSLATE__Test__start__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a0
move $t1, $t0
li $t2, 10
li $t3, 1
addu $v0, $t2, $t3
move $t4, $v0
li $t5, 4
mult $t4, $t5
mflo $v0
move $t6, $v0
move $a0, $t6
jal _halloc
move $t7, $v0
move $t8, $t7
li $t9, 4
move $s0, $t9
__MYTRANSLATE__Test__start__L2: 
move $s1, $s0
li $s2, 10
li $s3, 1
addu $v0, $s2, $s3
move $s4, $v0
li $s5, 4
mult $s4, $s5
mflo $v0
move $s6, $v0
slt $v0, $s1, $s6
move $s7, $v0
beqz $s7, __MYTRANSLATE__Test__start__L3
move $t2, $t8
move $t3, $s0
addu $v0, $t2, $t3
move $t4, $v0
li $t5, 0
sw $t5, 0($t4)
move $t6, $s0
li $t7, 4
addu $v0, $t6, $t7
move $t9, $v0
move $s0, $t9
b __MYTRANSLATE__Test__start__L2
__MYTRANSLATE__Test__start__L3: 
move $s2, $t8
li $s3, 10
sw $s3, 0($s2)
move $s4, $t8
move $s5, $s4
sw $s5, 4($t1)
move $s1, $t0
lw  $s6, 4($s1)
move $s7, $s6
move $t2, $s7
li $t3, 1
li $t4, 4
mult $t3, $t4
mflo $v0
move $t5, $v0
move $t6, $t5
move $t7, $t6
move $t9, $t7
li $s0, 4
addu $v0, $t9, $s0
move $s2, $v0
addu $v0, $t2, $s2
move $s3, $v0
li $t8, 40
sw $t8, 0($s3)
move $s4, $t0
lw  $t1, 4($s4)
move $s5, $t1
move $s1, $s5
li $s6, 3
li $s7, 4
mult $s6, $s7
mflo $v0
move $t3, $v0
li $t4, 15
subu $v0, $t3, $t4
move $t5, $v0
li $t6, 4
addu $v0, $t5, $t6
move $t7, $v0
li $t9, 4
mult $t7, $t9
mflo $v0
move $s0, $v0
move $t2, $s0
move $s2, $t2
move $s3, $s2
li $t8, 4
addu $v0, $s3, $t8
move $s4, $v0
addu $v0, $s1, $s4
move $t1, $v0
li $s5, 80
sw $s5, 0($t1)
move $s6, $t0
lw  $s7, 4($s6)
move $t3, $s7
move $t4, $t3
li $t5, 1
li $t6, 4
mult $t5, $t6
mflo $v0
move $t7, $v0
move $t9, $t7
move $s0, $t9
move $t2, $s0
li $s2, 4
addu $v0, $t2, $s2
move $s3, $v0
addu $v0, $t4, $s3
move $t8, $v0
lw  $s1, 0($t8)
move $s4, $s1
move $t1, $s4
move $s5, $t1
move $v0, $s5
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
