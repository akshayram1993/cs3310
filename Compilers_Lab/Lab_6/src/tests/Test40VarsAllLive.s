.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $a0, 4
jal _halloc
move $t0, $v0
li $a0, 4
jal _halloc
move $t1, $v0
la $t2, Finder_Find
sw $t2, 0($t0)
sw $t0, 0($t1)
move $t3, $t1
lw  $t4, 0($t3)
lw  $t5, 0($t4)
li $t6, 1
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t3
move $a1, $t6
jalr $t5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t7, $v0
move $a0 ,$t7
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	Finder_Find
Finder_Find:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 248
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a1
move $v1, $t0
sw $v1, 32($sp)
li $t2, 1
lw $v1, 32($sp)
subu $v0, $t2, $v1
move $t3, $v0
move $v1, $t3
sw $v1, 36($sp)
lw $v1, 32($sp)
lw $v1, 36($sp)
addu $v0, $v1, $v1
move $t5, $v0
li $t6, 1
subu $v0, $t6, $t5
move $t7, $v0
move $v1, $t7
sw $v1, 40($sp)
lw $v1, 36($sp)
lw $v1, 40($sp)
subu $v0, $v1, $v1
move $t9, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t9
move $s0, $v0
li $s1, 1
subu $v0, $s1, $s0
move $s2, $v0
move $v1, $s2
sw $v1, 44($sp)
lw $v1, 40($sp)
lw $v1, 44($sp)
addu $v0, $v1, $v1
move $s4, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s4
move $s5, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s5
move $s6, $v0
li $s7, 1
subu $v0, $s7, $s6
move $t0, $v0
move $v1, $t0
sw $v1, 48($sp)
lw $v1, 44($sp)
lw $v1, 48($sp)
subu $v0, $v1, $v1
move $t3, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t3
move $t5, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t5
move $t6, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t6
move $t7, $v0
li $t9, 1
subu $v0, $t9, $t7
move $s0, $v0
move $v1, $s0
sw $v1, 52($sp)
lw $v1, 48($sp)
lw $v1, 52($sp)
addu $v0, $v1, $v1
move $s2, $v0
lw $v1, 44($sp)
subu $v0, $v1, $s2
move $s4, $v0
lw $v1, 40($sp)
addu $v0, $v1, $s4
move $s5, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s5
move $s6, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s6
move $s7, $v0
li $t0, 1
subu $v0, $t0, $s7
move $t3, $v0
move $v1, $t3
sw $v1, 56($sp)
lw $v1, 52($sp)
lw $v1, 56($sp)
subu $v0, $v1, $v1
move $t6, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t6
move $t7, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t7
move $t9, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t9
move $s0, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s0
move $s2, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s2
move $s4, $v0
li $s5, 1
subu $v0, $s5, $s4
move $s6, $v0
move $v1, $s6
sw $v1, 60($sp)
lw $v1, 56($sp)
lw $v1, 60($sp)
addu $v0, $v1, $v1
move $t0, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t0
move $t3, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t3
move $t6, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t6
move $t7, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t7
move $t9, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t9
move $s0, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s0
move $s2, $v0
li $s4, 1
subu $v0, $s4, $s2
move $s5, $v0
move $v1, $s5
sw $v1, 64($sp)
lw $v1, 60($sp)
lw $v1, 64($sp)
subu $v0, $v1, $v1
move $t0, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t0
move $t3, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t3
move $t6, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t6
move $t7, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t7
move $t9, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t9
move $s0, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s0
move $s2, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s2
move $s4, $v0
li $s5, 1
subu $v0, $s5, $s4
move $t0, $v0
move $v1, $t0
sw $v1, 68($sp)
lw $v1, 64($sp)
lw $v1, 68($sp)
addu $v0, $v1, $v1
move $t6, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t6
move $t7, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t7
move $t9, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t9
move $s0, $v0
lw $v1, 48($sp)
addu $v0, $v1, $s0
move $s2, $v0
lw $v1, 44($sp)
subu $v0, $v1, $s2
move $s4, $v0
lw $v1, 40($sp)
addu $v0, $v1, $s4
move $s5, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s5
move $t0, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t0
move $t6, $v0
li $t7, 1
subu $v0, $t7, $t6
move $t9, $v0
move $v1, $t9
sw $v1, 72($sp)
lw $v1, 68($sp)
lw $v1, 72($sp)
subu $v0, $v1, $v1
move $s2, $v0
lw $v1, 64($sp)
addu $v0, $v1, $s2
move $s4, $v0
lw $v1, 60($sp)
subu $v0, $v1, $s4
move $s5, $v0
lw $v1, 56($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t0
move $t6, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t6
move $t7, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t7
move $t9, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t9
move $s2, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s2
move $s4, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s4
move $s5, $v0
li $t0, 1
subu $v0, $t0, $s5
move $t6, $v0
move $v1, $t6
sw $v1, 76($sp)
lw $v1, 72($sp)
lw $v1, 76($sp)
addu $v0, $v1, $v1
move $t9, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t9
move $s2, $v0
lw $v1, 64($sp)
addu $v0, $v1, $s2
move $s4, $v0
lw $v1, 60($sp)
subu $v0, $v1, $s4
move $s5, $v0
lw $v1, 56($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t0
move $t6, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t6
move $t9, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t9
move $s2, $v0
lw $v1, 40($sp)
addu $v0, $v1, $s2
move $s4, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s4
move $s5, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s5
move $t0, $v0
li $t6, 1
subu $v0, $t6, $t0
move $t9, $v0
move $v1, $t9
sw $v1, 80($sp)
lw $v1, 76($sp)
lw $v1, 80($sp)
subu $v0, $v1, $v1
move $s4, $v0
lw $v1, 72($sp)
addu $v0, $v1, $s4
move $s5, $v0
lw $v1, 68($sp)
subu $v0, $v1, $s5
move $t0, $v0
lw $v1, 64($sp)
addu $v0, $v1, $t0
move $t6, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t6
move $t9, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t9
move $s4, $v0
lw $v1, 52($sp)
subu $v0, $v1, $s4
move $s5, $v0
lw $v1, 48($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t0
move $t6, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t6
move $t9, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t9
move $s4, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s4
move $s5, $v0
li $t0, 1
subu $v0, $t0, $s5
move $t6, $v0
move $v1, $t6
sw $v1, 84($sp)
lw $v1, 80($sp)
lw $v1, 84($sp)
addu $v0, $v1, $v1
move $s4, $v0
lw $v1, 76($sp)
subu $v0, $v1, $s4
move $s5, $v0
lw $v1, 72($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t0
move $t6, $v0
lw $v1, 64($sp)
addu $v0, $v1, $t6
move $s4, $v0
lw $v1, 60($sp)
subu $v0, $v1, $s4
move $s5, $v0
lw $v1, 56($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t0
move $t6, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t6
move $s4, $v0
lw $v1, 44($sp)
subu $v0, $v1, $s4
move $s5, $v0
lw $v1, 40($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t0
move $t6, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t6
move $s4, $v0
li $s5, 1
subu $v0, $s5, $s4
move $t0, $v0
move $v1, $t0
sw $v1, 88($sp)
lw $v1, 84($sp)
lw $v1, 88($sp)
subu $v0, $v1, $v1
move $s4, $v0
lw $v1, 80($sp)
addu $v0, $v1, $s4
move $s5, $v0
lw $v1, 76($sp)
subu $v0, $v1, $s5
move $t0, $v0
lw $v1, 72($sp)
addu $v0, $v1, $t0
move $s4, $v0
lw $v1, 68($sp)
subu $v0, $v1, $s4
move $s5, $v0
lw $v1, 64($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t0
move $s4, $v0
lw $v1, 56($sp)
addu $v0, $v1, $s4
move $s5, $v0
lw $v1, 52($sp)
subu $v0, $v1, $s5
move $t0, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t0
move $s4, $v0
lw $v1, 44($sp)
subu $v0, $v1, $s4
move $s5, $v0
lw $v1, 40($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t0
move $s4, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s4
move $s5, $v0
li $t0, 1
subu $v0, $t0, $s5
move $s4, $v0
move $v1, $s4
sw $v1, 92($sp)
lw $v1, 88($sp)
lw $v1, 92($sp)
addu $v0, $v1, $v1
move $t0, $v0
lw $v1, 84($sp)
subu $v0, $v1, $t0
move $s4, $v0
lw $v1, 80($sp)
addu $v0, $v1, $s4
move $t0, $v0
lw $v1, 76($sp)
subu $v0, $v1, $t0
move $s4, $v0
lw $v1, 72($sp)
addu $v0, $v1, $s4
move $t0, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t0
move $s4, $v0
lw $v1, 64($sp)
addu $v0, $v1, $s4
move $t0, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t0
move $s4, $v0
lw $v1, 56($sp)
addu $v0, $v1, $s4
move $t0, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t0
move $s4, $v0
lw $v1, 48($sp)
addu $v0, $v1, $s4
move $t0, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t0
move $s4, $v0
lw $v1, 40($sp)
addu $v0, $v1, $s4
move $t0, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t0
move $s4, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s4
move $t0, $v0
li $s4, 1
subu $v0, $s4, $t0
move $t1, $v0
move $v1, $t1
sw $v1, 96($sp)
lw $v1, 92($sp)
lw $v1, 96($sp)
subu $v0, $v1, $v1
move $s4, $v0
lw $v1, 88($sp)
addu $v0, $v1, $s4
move $t1, $v0
lw $v1, 84($sp)
subu $v0, $v1, $t1
move $s4, $v0
lw $v1, 80($sp)
addu $v0, $v1, $s4
move $t1, $v0
lw $v1, 76($sp)
subu $v0, $v1, $t1
move $s4, $v0
lw $v1, 72($sp)
addu $v0, $v1, $s4
move $t1, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t1
move $s4, $v0
lw $v1, 64($sp)
addu $v0, $v1, $s4
move $t1, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t1
move $s4, $v0
lw $v1, 56($sp)
addu $v0, $v1, $s4
move $t1, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t1
move $s4, $v0
lw $v1, 48($sp)
addu $v0, $v1, $s4
move $t1, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t1
move $s4, $v0
lw $v1, 40($sp)
addu $v0, $v1, $s4
move $t1, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t1
move $s4, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s4
move $t1, $v0
li $s4, 1
subu $v0, $s4, $t1
move $t4, $v0
move $v1, $t4
sw $v1, 100($sp)
lw $v1, 96($sp)
lw $v1, 100($sp)
addu $v0, $v1, $v1
move $s4, $v0
lw $v1, 92($sp)
subu $v0, $v1, $s4
move $t4, $v0
lw $v1, 88($sp)
addu $v0, $v1, $t4
move $s4, $v0
lw $v1, 84($sp)
subu $v0, $v1, $s4
move $t4, $v0
lw $v1, 80($sp)
addu $v0, $v1, $t4
move $s4, $v0
lw $v1, 76($sp)
subu $v0, $v1, $s4
move $t4, $v0
lw $v1, 72($sp)
addu $v0, $v1, $t4
move $s4, $v0
lw $v1, 68($sp)
subu $v0, $v1, $s4
move $t4, $v0
lw $v1, 64($sp)
addu $v0, $v1, $t4
move $s4, $v0
lw $v1, 60($sp)
subu $v0, $v1, $s4
move $t4, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t4
move $s4, $v0
lw $v1, 52($sp)
subu $v0, $v1, $s4
move $t4, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t4
move $s4, $v0
lw $v1, 44($sp)
subu $v0, $v1, $s4
move $t4, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t4
move $s4, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s4
move $t4, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t4
move $s4, $v0
li $t4, 1
subu $v0, $t4, $s4
move $t8, $v0
move $v1, $t8
sw $v1, 104($sp)
lw $v1, 100($sp)
lw $v1, 104($sp)
subu $v0, $v1, $v1
move $t4, $v0
lw $v1, 96($sp)
addu $v0, $v1, $t4
move $t8, $v0
lw $v1, 92($sp)
subu $v0, $v1, $t8
move $t4, $v0
lw $v1, 88($sp)
addu $v0, $v1, $t4
move $t8, $v0
lw $v1, 84($sp)
subu $v0, $v1, $t8
move $t4, $v0
lw $v1, 80($sp)
addu $v0, $v1, $t4
move $t8, $v0
lw $v1, 76($sp)
subu $v0, $v1, $t8
move $t4, $v0
lw $v1, 72($sp)
addu $v0, $v1, $t4
move $t8, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t8
move $t4, $v0
lw $v1, 64($sp)
addu $v0, $v1, $t4
move $t8, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t8
move $t4, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t4
move $t8, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t8
move $t4, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t4
move $t8, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t8
move $t4, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t4
move $t8, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t8
move $t4, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t4
move $t8, $v0
li $t4, 1
subu $v0, $t4, $t8
move $s3, $v0
move $v1, $s3
sw $v1, 108($sp)
lw $v1, 104($sp)
lw $v1, 108($sp)
addu $v0, $v1, $v1
move $t4, $v0
lw $v1, 100($sp)
subu $v0, $v1, $t4
move $s3, $v0
lw $v1, 96($sp)
addu $v0, $v1, $s3
move $t4, $v0
lw $v1, 92($sp)
subu $v0, $v1, $t4
move $s3, $v0
lw $v1, 88($sp)
addu $v0, $v1, $s3
move $t4, $v0
lw $v1, 84($sp)
subu $v0, $v1, $t4
move $s3, $v0
lw $v1, 80($sp)
addu $v0, $v1, $s3
move $t4, $v0
lw $v1, 76($sp)
subu $v0, $v1, $t4
move $s3, $v0
lw $v1, 72($sp)
addu $v0, $v1, $s3
move $t4, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t4
move $s3, $v0
lw $v1, 64($sp)
addu $v0, $v1, $s3
move $t4, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t4
move $s3, $v0
lw $v1, 56($sp)
addu $v0, $v1, $s3
move $t4, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t4
move $s3, $v0
lw $v1, 48($sp)
addu $v0, $v1, $s3
move $t4, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t4
move $s3, $v0
lw $v1, 40($sp)
addu $v0, $v1, $s3
move $t4, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t4
move $s3, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s3
move $t4, $v0
li $s3, 1
subu $v0, $s3, $t4
move $t2, $v0
move $v1, $t2
sw $v1, 112($sp)
lw $v1, 108($sp)
lw $v1, 112($sp)
subu $v0, $v1, $v1
move $s3, $v0
lw $v1, 104($sp)
addu $v0, $v1, $s3
move $t2, $v0
lw $v1, 100($sp)
subu $v0, $v1, $t2
move $s3, $v0
lw $v1, 96($sp)
addu $v0, $v1, $s3
move $t2, $v0
lw $v1, 92($sp)
subu $v0, $v1, $t2
move $s3, $v0
lw $v1, 88($sp)
addu $v0, $v1, $s3
move $t2, $v0
lw $v1, 84($sp)
subu $v0, $v1, $t2
move $s3, $v0
lw $v1, 80($sp)
addu $v0, $v1, $s3
move $t2, $v0
lw $v1, 76($sp)
subu $v0, $v1, $t2
move $s3, $v0
lw $v1, 72($sp)
addu $v0, $v1, $s3
move $t2, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t2
move $s3, $v0
lw $v1, 64($sp)
addu $v0, $v1, $s3
move $t2, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t2
move $s3, $v0
lw $v1, 56($sp)
addu $v0, $v1, $s3
move $t2, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t2
move $s3, $v0
lw $v1, 48($sp)
addu $v0, $v1, $s3
move $t2, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t2
move $s3, $v0
lw $v1, 40($sp)
addu $v0, $v1, $s3
move $t2, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t2
move $s3, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s3
move $t2, $v0
li $s3, 1
subu $v0, $s3, $t2
move $s1, $v0
move $v1, $s1
sw $v1, 116($sp)
lw $v1, 112($sp)
lw $v1, 116($sp)
addu $v0, $v1, $v1
move $s3, $v0
lw $v1, 108($sp)
subu $v0, $v1, $s3
move $s1, $v0
lw $v1, 104($sp)
addu $v0, $v1, $s1
move $s3, $v0
lw $v1, 100($sp)
subu $v0, $v1, $s3
move $s1, $v0
lw $v1, 96($sp)
addu $v0, $v1, $s1
move $s3, $v0
lw $v1, 92($sp)
subu $v0, $v1, $s3
move $s1, $v0
lw $v1, 88($sp)
addu $v0, $v1, $s1
move $s3, $v0
lw $v1, 84($sp)
subu $v0, $v1, $s3
move $s1, $v0
lw $v1, 80($sp)
addu $v0, $v1, $s1
move $s3, $v0
lw $v1, 76($sp)
subu $v0, $v1, $s3
move $s1, $v0
lw $v1, 72($sp)
addu $v0, $v1, $s1
move $s3, $v0
lw $v1, 68($sp)
subu $v0, $v1, $s3
move $s1, $v0
lw $v1, 64($sp)
addu $v0, $v1, $s1
move $s3, $v0
lw $v1, 60($sp)
subu $v0, $v1, $s3
move $s1, $v0
lw $v1, 56($sp)
addu $v0, $v1, $s1
move $s3, $v0
lw $v1, 52($sp)
subu $v0, $v1, $s3
move $s1, $v0
lw $v1, 48($sp)
addu $v0, $v1, $s1
move $s3, $v0
lw $v1, 44($sp)
subu $v0, $v1, $s3
move $s1, $v0
lw $v1, 40($sp)
addu $v0, $v1, $s1
move $s3, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s3
move $s1, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s1
move $s3, $v0
li $s1, 1
subu $v0, $s1, $s3
move $t5, $v0
move $v1, $t5
sw $v1, 120($sp)
lw $v1, 116($sp)
lw $v1, 120($sp)
subu $v0, $v1, $v1
move $s1, $v0
lw $v1, 112($sp)
addu $v0, $v1, $s1
move $t5, $v0
lw $v1, 108($sp)
subu $v0, $v1, $t5
move $s1, $v0
lw $v1, 104($sp)
addu $v0, $v1, $s1
move $t5, $v0
lw $v1, 100($sp)
subu $v0, $v1, $t5
move $s1, $v0
lw $v1, 96($sp)
addu $v0, $v1, $s1
move $t5, $v0
lw $v1, 92($sp)
subu $v0, $v1, $t5
move $s1, $v0
lw $v1, 88($sp)
addu $v0, $v1, $s1
move $t5, $v0
lw $v1, 84($sp)
subu $v0, $v1, $t5
move $s1, $v0
lw $v1, 80($sp)
addu $v0, $v1, $s1
move $t5, $v0
lw $v1, 76($sp)
subu $v0, $v1, $t5
move $s1, $v0
lw $v1, 72($sp)
addu $v0, $v1, $s1
move $t5, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t5
move $s1, $v0
lw $v1, 64($sp)
addu $v0, $v1, $s1
move $t5, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t5
move $s1, $v0
lw $v1, 56($sp)
addu $v0, $v1, $s1
move $t5, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t5
move $s1, $v0
lw $v1, 48($sp)
addu $v0, $v1, $s1
move $t5, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t5
move $s1, $v0
lw $v1, 40($sp)
addu $v0, $v1, $s1
move $t5, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t5
move $s1, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s1
move $t5, $v0
li $s1, 1
subu $v0, $s1, $t5
move $s7, $v0
move $t5, $s7
lw $v1, 120($sp)
addu $v0, $v1, $t5
move $s1, $v0
lw $v1, 116($sp)
subu $v0, $v1, $s1
move $s7, $v0
lw $v1, 112($sp)
addu $v0, $v1, $s7
move $s1, $v0
lw $v1, 108($sp)
subu $v0, $v1, $s1
move $s7, $v0
lw $v1, 104($sp)
addu $v0, $v1, $s7
move $s1, $v0
lw $v1, 100($sp)
subu $v0, $v1, $s1
move $s7, $v0
lw $v1, 96($sp)
addu $v0, $v1, $s7
move $s1, $v0
lw $v1, 92($sp)
subu $v0, $v1, $s1
move $s7, $v0
lw $v1, 88($sp)
addu $v0, $v1, $s7
move $s1, $v0
lw $v1, 84($sp)
subu $v0, $v1, $s1
move $s7, $v0
lw $v1, 80($sp)
addu $v0, $v1, $s7
move $s1, $v0
lw $v1, 76($sp)
subu $v0, $v1, $s1
move $s7, $v0
lw $v1, 72($sp)
addu $v0, $v1, $s7
move $s1, $v0
lw $v1, 68($sp)
subu $v0, $v1, $s1
move $s7, $v0
lw $v1, 64($sp)
addu $v0, $v1, $s7
move $s1, $v0
lw $v1, 60($sp)
subu $v0, $v1, $s1
move $s7, $v0
lw $v1, 56($sp)
addu $v0, $v1, $s7
move $s1, $v0
lw $v1, 52($sp)
subu $v0, $v1, $s1
move $s7, $v0
lw $v1, 48($sp)
addu $v0, $v1, $s7
move $s1, $v0
lw $v1, 44($sp)
subu $v0, $v1, $s1
move $s7, $v0
lw $v1, 40($sp)
addu $v0, $v1, $s7
move $s1, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s1
move $s7, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s7
move $s1, $v0
li $s7, 1
subu $v0, $s7, $s1
move $s6, $v0
move $s1, $s6
subu $v0, $t5, $s1
move $s7, $v0
lw $v1, 120($sp)
addu $v0, $v1, $s7
move $s6, $v0
lw $v1, 116($sp)
subu $v0, $v1, $s6
move $s7, $v0
lw $v1, 112($sp)
addu $v0, $v1, $s7
move $s6, $v0
lw $v1, 108($sp)
subu $v0, $v1, $s6
move $s7, $v0
lw $v1, 104($sp)
addu $v0, $v1, $s7
move $s6, $v0
lw $v1, 100($sp)
subu $v0, $v1, $s6
move $s7, $v0
lw $v1, 96($sp)
addu $v0, $v1, $s7
move $s6, $v0
lw $v1, 92($sp)
subu $v0, $v1, $s6
move $s7, $v0
lw $v1, 88($sp)
addu $v0, $v1, $s7
move $s6, $v0
lw $v1, 84($sp)
subu $v0, $v1, $s6
move $s7, $v0
lw $v1, 80($sp)
addu $v0, $v1, $s7
move $s6, $v0
lw $v1, 76($sp)
subu $v0, $v1, $s6
move $s7, $v0
lw $v1, 72($sp)
addu $v0, $v1, $s7
move $s6, $v0
lw $v1, 68($sp)
subu $v0, $v1, $s6
move $s7, $v0
lw $v1, 64($sp)
addu $v0, $v1, $s7
move $s6, $v0
lw $v1, 60($sp)
subu $v0, $v1, $s6
move $s7, $v0
lw $v1, 56($sp)
addu $v0, $v1, $s7
move $s6, $v0
lw $v1, 52($sp)
subu $v0, $v1, $s6
move $s7, $v0
lw $v1, 48($sp)
addu $v0, $v1, $s7
move $s6, $v0
lw $v1, 44($sp)
subu $v0, $v1, $s6
move $s7, $v0
lw $v1, 40($sp)
addu $v0, $v1, $s7
move $s6, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s6
move $s7, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s7
move $s6, $v0
li $s7, 1
subu $v0, $s7, $s6
move $t3, $v0
move $s6, $t3
addu $v0, $s1, $s6
move $s7, $v0
subu $v0, $t5, $s7
move $t3, $v0
lw $v1, 120($sp)
addu $v0, $v1, $t3
move $s7, $v0
lw $v1, 116($sp)
subu $v0, $v1, $s7
move $t3, $v0
lw $v1, 112($sp)
addu $v0, $v1, $t3
move $s7, $v0
lw $v1, 108($sp)
subu $v0, $v1, $s7
move $t3, $v0
lw $v1, 104($sp)
addu $v0, $v1, $t3
move $s7, $v0
lw $v1, 100($sp)
subu $v0, $v1, $s7
move $t3, $v0
lw $v1, 96($sp)
addu $v0, $v1, $t3
move $s7, $v0
lw $v1, 92($sp)
subu $v0, $v1, $s7
move $t3, $v0
lw $v1, 88($sp)
addu $v0, $v1, $t3
move $s7, $v0
lw $v1, 84($sp)
subu $v0, $v1, $s7
move $t3, $v0
lw $v1, 80($sp)
addu $v0, $v1, $t3
move $s7, $v0
lw $v1, 76($sp)
subu $v0, $v1, $s7
move $t3, $v0
lw $v1, 72($sp)
addu $v0, $v1, $t3
move $s7, $v0
lw $v1, 68($sp)
subu $v0, $v1, $s7
move $t3, $v0
lw $v1, 64($sp)
addu $v0, $v1, $t3
move $s7, $v0
lw $v1, 60($sp)
subu $v0, $v1, $s7
move $t3, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t3
move $s7, $v0
lw $v1, 52($sp)
subu $v0, $v1, $s7
move $t3, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t3
move $s7, $v0
lw $v1, 44($sp)
subu $v0, $v1, $s7
move $t3, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t3
move $s7, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s7
move $t3, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t3
move $s7, $v0
li $t3, 1
subu $v0, $t3, $s7
move $s0, $v0
move $s7, $s0
subu $v0, $s6, $s7
move $t3, $v0
addu $v0, $s1, $t3
move $s0, $v0
subu $v0, $t5, $s0
move $t3, $v0
lw $v1, 120($sp)
addu $v0, $v1, $t3
move $s0, $v0
lw $v1, 116($sp)
subu $v0, $v1, $s0
move $t3, $v0
lw $v1, 112($sp)
addu $v0, $v1, $t3
move $s0, $v0
lw $v1, 108($sp)
subu $v0, $v1, $s0
move $t3, $v0
lw $v1, 104($sp)
addu $v0, $v1, $t3
move $s0, $v0
lw $v1, 100($sp)
subu $v0, $v1, $s0
move $t3, $v0
lw $v1, 96($sp)
addu $v0, $v1, $t3
move $s0, $v0
lw $v1, 92($sp)
subu $v0, $v1, $s0
move $t3, $v0
lw $v1, 88($sp)
addu $v0, $v1, $t3
move $s0, $v0
lw $v1, 84($sp)
subu $v0, $v1, $s0
move $t3, $v0
lw $v1, 80($sp)
addu $v0, $v1, $t3
move $s0, $v0
lw $v1, 76($sp)
subu $v0, $v1, $s0
move $t3, $v0
lw $v1, 72($sp)
addu $v0, $v1, $t3
move $s0, $v0
lw $v1, 68($sp)
subu $v0, $v1, $s0
move $t3, $v0
lw $v1, 64($sp)
addu $v0, $v1, $t3
move $s0, $v0
lw $v1, 60($sp)
subu $v0, $v1, $s0
move $t3, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t3
move $s0, $v0
lw $v1, 52($sp)
subu $v0, $v1, $s0
move $t3, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t3
move $s0, $v0
lw $v1, 44($sp)
subu $v0, $v1, $s0
move $t3, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t3
move $s0, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s0
move $t3, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t3
move $s0, $v0
li $t3, 1
subu $v0, $t3, $s0
move $t7, $v0
move $s0, $t7
addu $v0, $s7, $s0
move $t3, $v0
subu $v0, $s6, $t3
move $t7, $v0
addu $v0, $s1, $t7
move $t3, $v0
subu $v0, $t5, $t3
move $t7, $v0
lw $v1, 120($sp)
addu $v0, $v1, $t7
move $t3, $v0
lw $v1, 116($sp)
subu $v0, $v1, $t3
move $t7, $v0
lw $v1, 112($sp)
addu $v0, $v1, $t7
move $t3, $v0
lw $v1, 108($sp)
subu $v0, $v1, $t3
move $t7, $v0
lw $v1, 104($sp)
addu $v0, $v1, $t7
move $t3, $v0
lw $v1, 100($sp)
subu $v0, $v1, $t3
move $t7, $v0
lw $v1, 96($sp)
addu $v0, $v1, $t7
move $t3, $v0
lw $v1, 92($sp)
subu $v0, $v1, $t3
move $t7, $v0
lw $v1, 88($sp)
addu $v0, $v1, $t7
move $t3, $v0
lw $v1, 84($sp)
subu $v0, $v1, $t3
move $t7, $v0
lw $v1, 80($sp)
addu $v0, $v1, $t7
move $t3, $v0
lw $v1, 76($sp)
subu $v0, $v1, $t3
move $t7, $v0
lw $v1, 72($sp)
addu $v0, $v1, $t7
move $t3, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t3
move $t7, $v0
lw $v1, 64($sp)
addu $v0, $v1, $t7
move $t3, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t3
move $t7, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t7
move $t3, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t3
move $t7, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t7
move $t3, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t3
move $t7, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t7
move $t3, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t3
move $t7, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t7
move $t3, $v0
li $t7, 1
subu $v0, $t7, $t3
move $s2, $v0
move $t3, $s2
subu $v0, $s0, $t3
move $t7, $v0
addu $v0, $s7, $t7
move $s2, $v0
subu $v0, $s6, $s2
move $t7, $v0
addu $v0, $s1, $t7
move $s2, $v0
subu $v0, $t5, $s2
move $t7, $v0
lw $v1, 120($sp)
addu $v0, $v1, $t7
move $s2, $v0
lw $v1, 116($sp)
subu $v0, $v1, $s2
move $t7, $v0
lw $v1, 112($sp)
addu $v0, $v1, $t7
move $s2, $v0
lw $v1, 108($sp)
subu $v0, $v1, $s2
move $t7, $v0
lw $v1, 104($sp)
addu $v0, $v1, $t7
move $s2, $v0
lw $v1, 100($sp)
subu $v0, $v1, $s2
move $t7, $v0
lw $v1, 96($sp)
addu $v0, $v1, $t7
move $s2, $v0
lw $v1, 92($sp)
subu $v0, $v1, $s2
move $t7, $v0
lw $v1, 88($sp)
addu $v0, $v1, $t7
move $s2, $v0
lw $v1, 84($sp)
subu $v0, $v1, $s2
move $t7, $v0
lw $v1, 80($sp)
addu $v0, $v1, $t7
move $s2, $v0
lw $v1, 76($sp)
subu $v0, $v1, $s2
move $t7, $v0
lw $v1, 72($sp)
addu $v0, $v1, $t7
move $s2, $v0
lw $v1, 68($sp)
subu $v0, $v1, $s2
move $t7, $v0
lw $v1, 64($sp)
addu $v0, $v1, $t7
move $s2, $v0
lw $v1, 60($sp)
subu $v0, $v1, $s2
move $t7, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t7
move $s2, $v0
lw $v1, 52($sp)
subu $v0, $v1, $s2
move $t7, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t7
move $s2, $v0
lw $v1, 44($sp)
subu $v0, $v1, $s2
move $t7, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t7
move $s2, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s2
move $t7, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t7
move $s2, $v0
li $t7, 1
subu $v0, $t7, $s2
move $t9, $v0
move $s2, $t9
addu $v0, $t3, $s2
move $t7, $v0
subu $v0, $s0, $t7
move $t9, $v0
addu $v0, $s7, $t9
move $t7, $v0
subu $v0, $s6, $t7
move $t9, $v0
addu $v0, $s1, $t9
move $t7, $v0
subu $v0, $t5, $t7
move $t9, $v0
lw $v1, 120($sp)
addu $v0, $v1, $t9
move $t7, $v0
lw $v1, 116($sp)
subu $v0, $v1, $t7
move $t9, $v0
lw $v1, 112($sp)
addu $v0, $v1, $t9
move $t7, $v0
lw $v1, 108($sp)
subu $v0, $v1, $t7
move $t9, $v0
lw $v1, 104($sp)
addu $v0, $v1, $t9
move $t7, $v0
lw $v1, 100($sp)
subu $v0, $v1, $t7
move $t9, $v0
lw $v1, 96($sp)
addu $v0, $v1, $t9
move $t7, $v0
lw $v1, 92($sp)
subu $v0, $v1, $t7
move $t9, $v0
lw $v1, 88($sp)
addu $v0, $v1, $t9
move $t7, $v0
lw $v1, 84($sp)
subu $v0, $v1, $t7
move $t9, $v0
lw $v1, 80($sp)
addu $v0, $v1, $t9
move $t7, $v0
lw $v1, 76($sp)
subu $v0, $v1, $t7
move $t9, $v0
lw $v1, 72($sp)
addu $v0, $v1, $t9
move $t7, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t7
move $t9, $v0
lw $v1, 64($sp)
addu $v0, $v1, $t9
move $t7, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t7
move $t9, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t9
move $t7, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t7
move $t9, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t9
move $t7, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t7
move $t9, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t9
move $t7, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t7
move $t9, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t9
move $t7, $v0
li $t9, 1
subu $v0, $t9, $t7
move $t6, $v0
move $t7, $t6
subu $v0, $s2, $t7
move $t9, $v0
addu $v0, $t3, $t9
move $t6, $v0
subu $v0, $s0, $t6
move $t9, $v0
addu $v0, $s7, $t9
move $t6, $v0
subu $v0, $s6, $t6
move $t9, $v0
addu $v0, $s1, $t9
move $t6, $v0
subu $v0, $t5, $t6
move $t9, $v0
lw $v1, 120($sp)
addu $v0, $v1, $t9
move $t6, $v0
lw $v1, 116($sp)
subu $v0, $v1, $t6
move $t9, $v0
lw $v1, 112($sp)
addu $v0, $v1, $t9
move $t6, $v0
lw $v1, 108($sp)
subu $v0, $v1, $t6
move $t9, $v0
lw $v1, 104($sp)
addu $v0, $v1, $t9
move $t6, $v0
lw $v1, 100($sp)
subu $v0, $v1, $t6
move $t9, $v0
lw $v1, 96($sp)
addu $v0, $v1, $t9
move $t6, $v0
lw $v1, 92($sp)
subu $v0, $v1, $t6
move $t9, $v0
lw $v1, 88($sp)
addu $v0, $v1, $t9
move $t6, $v0
lw $v1, 84($sp)
subu $v0, $v1, $t6
move $t9, $v0
lw $v1, 80($sp)
addu $v0, $v1, $t9
move $t6, $v0
lw $v1, 76($sp)
subu $v0, $v1, $t6
move $t9, $v0
lw $v1, 72($sp)
addu $v0, $v1, $t9
move $t6, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t6
move $t9, $v0
lw $v1, 64($sp)
addu $v0, $v1, $t9
move $t6, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t6
move $t9, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t9
move $t6, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t6
move $t9, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t9
move $t6, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t6
move $t9, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t9
move $t6, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t6
move $t9, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t9
move $t6, $v0
li $t9, 1
subu $v0, $t9, $t6
move $s5, $v0
move $t6, $s5
addu $v0, $t7, $t6
move $t9, $v0
subu $v0, $s2, $t9
move $s5, $v0
addu $v0, $t3, $s5
move $t9, $v0
subu $v0, $s0, $t9
move $s5, $v0
addu $v0, $s7, $s5
move $t9, $v0
subu $v0, $s6, $t9
move $s5, $v0
addu $v0, $s1, $s5
move $t9, $v0
subu $v0, $t5, $t9
move $s5, $v0
lw $v1, 120($sp)
addu $v0, $v1, $s5
move $t9, $v0
lw $v1, 116($sp)
subu $v0, $v1, $t9
move $s5, $v0
lw $v1, 112($sp)
addu $v0, $v1, $s5
move $t9, $v0
lw $v1, 108($sp)
subu $v0, $v1, $t9
move $s5, $v0
lw $v1, 104($sp)
addu $v0, $v1, $s5
move $t9, $v0
lw $v1, 100($sp)
subu $v0, $v1, $t9
move $s5, $v0
lw $v1, 96($sp)
addu $v0, $v1, $s5
move $t9, $v0
lw $v1, 92($sp)
subu $v0, $v1, $t9
move $s5, $v0
lw $v1, 88($sp)
addu $v0, $v1, $s5
move $t9, $v0
lw $v1, 84($sp)
subu $v0, $v1, $t9
move $s5, $v0
lw $v1, 80($sp)
addu $v0, $v1, $s5
move $t9, $v0
lw $v1, 76($sp)
subu $v0, $v1, $t9
move $s5, $v0
lw $v1, 72($sp)
addu $v0, $v1, $s5
move $t9, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t9
move $s5, $v0
lw $v1, 64($sp)
addu $v0, $v1, $s5
move $t9, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t9
move $s5, $v0
lw $v1, 56($sp)
addu $v0, $v1, $s5
move $t9, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t9
move $s5, $v0
lw $v1, 48($sp)
addu $v0, $v1, $s5
move $t9, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t9
move $s5, $v0
lw $v1, 40($sp)
addu $v0, $v1, $s5
move $t9, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t9
move $s5, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s5
move $t9, $v0
li $s5, 1
subu $v0, $s5, $t9
move $t0, $v0
move $t9, $t0
subu $v0, $t6, $t9
move $s5, $v0
addu $v0, $t7, $s5
move $t0, $v0
subu $v0, $s2, $t0
move $s5, $v0
addu $v0, $t3, $s5
move $t0, $v0
subu $v0, $s0, $t0
move $s5, $v0
addu $v0, $s7, $s5
move $t0, $v0
subu $v0, $s6, $t0
move $s5, $v0
addu $v0, $s1, $s5
move $t0, $v0
subu $v0, $t5, $t0
move $s5, $v0
lw $v1, 120($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 116($sp)
subu $v0, $v1, $t0
move $s5, $v0
lw $v1, 112($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 108($sp)
subu $v0, $v1, $t0
move $s5, $v0
lw $v1, 104($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 100($sp)
subu $v0, $v1, $t0
move $s5, $v0
lw $v1, 96($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 92($sp)
subu $v0, $v1, $t0
move $s5, $v0
lw $v1, 88($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 84($sp)
subu $v0, $v1, $t0
move $s5, $v0
lw $v1, 80($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 76($sp)
subu $v0, $v1, $t0
move $s5, $v0
lw $v1, 72($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t0
move $s5, $v0
lw $v1, 64($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t0
move $s5, $v0
lw $v1, 56($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t0
move $s5, $v0
lw $v1, 48($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t0
move $s5, $v0
lw $v1, 40($sp)
addu $v0, $v1, $s5
move $t0, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t0
move $s5, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s5
move $t0, $v0
li $s5, 1
subu $v0, $s5, $t0
move $t1, $v0
move $t0, $t1
addu $v0, $t9, $t0
move $s5, $v0
subu $v0, $t6, $s5
move $t1, $v0
addu $v0, $t7, $t1
move $s5, $v0
subu $v0, $s2, $s5
move $t1, $v0
addu $v0, $t3, $t1
move $s5, $v0
subu $v0, $s0, $s5
move $t1, $v0
addu $v0, $s7, $t1
move $s5, $v0
subu $v0, $s6, $s5
move $t1, $v0
addu $v0, $s1, $t1
move $s5, $v0
subu $v0, $t5, $s5
move $t1, $v0
lw $v1, 120($sp)
addu $v0, $v1, $t1
move $s5, $v0
lw $v1, 116($sp)
subu $v0, $v1, $s5
move $t1, $v0
lw $v1, 112($sp)
addu $v0, $v1, $t1
move $s5, $v0
lw $v1, 108($sp)
subu $v0, $v1, $s5
move $t1, $v0
lw $v1, 104($sp)
addu $v0, $v1, $t1
move $s5, $v0
lw $v1, 100($sp)
subu $v0, $v1, $s5
move $t1, $v0
lw $v1, 96($sp)
addu $v0, $v1, $t1
move $s5, $v0
lw $v1, 92($sp)
subu $v0, $v1, $s5
move $t1, $v0
lw $v1, 88($sp)
addu $v0, $v1, $t1
move $s5, $v0
lw $v1, 84($sp)
subu $v0, $v1, $s5
move $t1, $v0
lw $v1, 80($sp)
addu $v0, $v1, $t1
move $s5, $v0
lw $v1, 76($sp)
subu $v0, $v1, $s5
move $t1, $v0
lw $v1, 72($sp)
addu $v0, $v1, $t1
move $s5, $v0
lw $v1, 68($sp)
subu $v0, $v1, $s5
move $t1, $v0
lw $v1, 64($sp)
addu $v0, $v1, $t1
move $s5, $v0
lw $v1, 60($sp)
subu $v0, $v1, $s5
move $t1, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t1
move $s5, $v0
lw $v1, 52($sp)
subu $v0, $v1, $s5
move $t1, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t1
move $s5, $v0
lw $v1, 44($sp)
subu $v0, $v1, $s5
move $t1, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t1
move $s5, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s5
move $t1, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t1
move $s5, $v0
li $t1, 1
subu $v0, $t1, $s5
move $s4, $v0
move $s5, $s4
subu $v0, $t0, $s5
move $t1, $v0
addu $v0, $t9, $t1
move $s4, $v0
subu $v0, $t6, $s4
move $t1, $v0
addu $v0, $t7, $t1
move $s4, $v0
subu $v0, $s2, $s4
move $t1, $v0
addu $v0, $t3, $t1
move $s4, $v0
subu $v0, $s0, $s4
move $t1, $v0
addu $v0, $s7, $t1
move $s4, $v0
subu $v0, $s6, $s4
move $t1, $v0
addu $v0, $s1, $t1
move $s4, $v0
subu $v0, $t5, $s4
move $t1, $v0
lw $v1, 120($sp)
addu $v0, $v1, $t1
move $s4, $v0
lw $v1, 116($sp)
subu $v0, $v1, $s4
move $t1, $v0
lw $v1, 112($sp)
addu $v0, $v1, $t1
move $s4, $v0
lw $v1, 108($sp)
subu $v0, $v1, $s4
move $t1, $v0
lw $v1, 104($sp)
addu $v0, $v1, $t1
move $s4, $v0
lw $v1, 100($sp)
subu $v0, $v1, $s4
move $t1, $v0
lw $v1, 96($sp)
addu $v0, $v1, $t1
move $s4, $v0
lw $v1, 92($sp)
subu $v0, $v1, $s4
move $t1, $v0
lw $v1, 88($sp)
addu $v0, $v1, $t1
move $s4, $v0
lw $v1, 84($sp)
subu $v0, $v1, $s4
move $t1, $v0
lw $v1, 80($sp)
addu $v0, $v1, $t1
move $s4, $v0
lw $v1, 76($sp)
subu $v0, $v1, $s4
move $t1, $v0
lw $v1, 72($sp)
addu $v0, $v1, $t1
move $s4, $v0
lw $v1, 68($sp)
subu $v0, $v1, $s4
move $t1, $v0
lw $v1, 64($sp)
addu $v0, $v1, $t1
move $s4, $v0
lw $v1, 60($sp)
subu $v0, $v1, $s4
move $t1, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t1
move $s4, $v0
lw $v1, 52($sp)
subu $v0, $v1, $s4
move $t1, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t1
move $s4, $v0
lw $v1, 44($sp)
subu $v0, $v1, $s4
move $t1, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t1
move $s4, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s4
move $t1, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t1
move $s4, $v0
li $t1, 1
subu $v0, $t1, $s4
move $t8, $v0
move $s4, $t8
addu $v0, $s5, $s4
move $t1, $v0
subu $v0, $t0, $t1
move $t8, $v0
addu $v0, $t9, $t8
move $t1, $v0
subu $v0, $t6, $t1
move $t8, $v0
addu $v0, $t7, $t8
move $t1, $v0
subu $v0, $s2, $t1
move $t8, $v0
addu $v0, $t3, $t8
move $t1, $v0
subu $v0, $s0, $t1
move $t8, $v0
addu $v0, $s7, $t8
move $t1, $v0
subu $v0, $s6, $t1
move $t8, $v0
addu $v0, $s1, $t8
move $t1, $v0
subu $v0, $t5, $t1
move $t8, $v0
lw $v1, 120($sp)
addu $v0, $v1, $t8
move $t1, $v0
lw $v1, 116($sp)
subu $v0, $v1, $t1
move $t8, $v0
lw $v1, 112($sp)
addu $v0, $v1, $t8
move $t1, $v0
lw $v1, 108($sp)
subu $v0, $v1, $t1
move $t8, $v0
lw $v1, 104($sp)
addu $v0, $v1, $t8
move $t1, $v0
lw $v1, 100($sp)
subu $v0, $v1, $t1
move $t8, $v0
lw $v1, 96($sp)
addu $v0, $v1, $t8
move $t1, $v0
lw $v1, 92($sp)
subu $v0, $v1, $t1
move $t8, $v0
lw $v1, 88($sp)
addu $v0, $v1, $t8
move $t1, $v0
lw $v1, 84($sp)
subu $v0, $v1, $t1
move $t8, $v0
lw $v1, 80($sp)
addu $v0, $v1, $t8
move $t1, $v0
lw $v1, 76($sp)
subu $v0, $v1, $t1
move $t8, $v0
lw $v1, 72($sp)
addu $v0, $v1, $t8
move $t1, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t1
move $t8, $v0
lw $v1, 64($sp)
addu $v0, $v1, $t8
move $t1, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t1
move $t8, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t8
move $t1, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t1
move $t8, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t8
move $t1, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t1
move $t8, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t8
move $t1, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t1
move $t8, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t8
move $t1, $v0
li $t8, 1
subu $v0, $t8, $t1
move $t4, $v0
move $t1, $t4
subu $v0, $s4, $t1
move $t8, $v0
addu $v0, $s5, $t8
move $t4, $v0
subu $v0, $t0, $t4
move $t8, $v0
addu $v0, $t9, $t8
move $t4, $v0
subu $v0, $t6, $t4
move $t8, $v0
addu $v0, $t7, $t8
move $t4, $v0
subu $v0, $s2, $t4
move $t8, $v0
addu $v0, $t3, $t8
move $t4, $v0
subu $v0, $s0, $t4
move $t8, $v0
addu $v0, $s7, $t8
move $t4, $v0
subu $v0, $s6, $t4
move $t8, $v0
addu $v0, $s1, $t8
move $t4, $v0
subu $v0, $t5, $t4
move $t8, $v0
lw $v1, 120($sp)
addu $v0, $v1, $t8
move $t4, $v0
lw $v1, 116($sp)
subu $v0, $v1, $t4
move $t8, $v0
lw $v1, 112($sp)
addu $v0, $v1, $t8
move $t4, $v0
lw $v1, 108($sp)
subu $v0, $v1, $t4
move $t8, $v0
lw $v1, 104($sp)
addu $v0, $v1, $t8
move $t4, $v0
lw $v1, 100($sp)
subu $v0, $v1, $t4
move $t8, $v0
lw $v1, 96($sp)
addu $v0, $v1, $t8
move $t4, $v0
lw $v1, 92($sp)
subu $v0, $v1, $t4
move $t8, $v0
lw $v1, 88($sp)
addu $v0, $v1, $t8
move $t4, $v0
lw $v1, 84($sp)
subu $v0, $v1, $t4
move $t8, $v0
lw $v1, 80($sp)
addu $v0, $v1, $t8
move $t4, $v0
lw $v1, 76($sp)
subu $v0, $v1, $t4
move $t8, $v0
lw $v1, 72($sp)
addu $v0, $v1, $t8
move $t4, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t4
move $t8, $v0
lw $v1, 64($sp)
addu $v0, $v1, $t8
move $t4, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t4
move $t8, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t8
move $t4, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t4
move $t8, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t8
move $t4, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t4
move $t8, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t8
move $t4, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t4
move $t8, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t8
move $t4, $v0
li $t8, 1
subu $v0, $t8, $t4
move $t2, $v0
move $t4, $t2
addu $v0, $t1, $t4
move $t8, $v0
subu $v0, $s4, $t8
move $t2, $v0
addu $v0, $s5, $t2
move $t8, $v0
subu $v0, $t0, $t8
move $t2, $v0
addu $v0, $t9, $t2
move $t8, $v0
subu $v0, $t6, $t8
move $t2, $v0
addu $v0, $t7, $t2
move $t8, $v0
subu $v0, $s2, $t8
move $t2, $v0
addu $v0, $t3, $t2
move $t8, $v0
subu $v0, $s0, $t8
move $t2, $v0
addu $v0, $s7, $t2
move $t8, $v0
subu $v0, $s6, $t8
move $t2, $v0
addu $v0, $s1, $t2
move $t8, $v0
subu $v0, $t5, $t8
move $t2, $v0
lw $v1, 120($sp)
addu $v0, $v1, $t2
move $t8, $v0
lw $v1, 116($sp)
subu $v0, $v1, $t8
move $t2, $v0
lw $v1, 112($sp)
addu $v0, $v1, $t2
move $t8, $v0
lw $v1, 108($sp)
subu $v0, $v1, $t8
move $t2, $v0
lw $v1, 104($sp)
addu $v0, $v1, $t2
move $t8, $v0
lw $v1, 100($sp)
subu $v0, $v1, $t8
move $t2, $v0
lw $v1, 96($sp)
addu $v0, $v1, $t2
move $t8, $v0
lw $v1, 92($sp)
subu $v0, $v1, $t8
move $t2, $v0
lw $v1, 88($sp)
addu $v0, $v1, $t2
move $t8, $v0
lw $v1, 84($sp)
subu $v0, $v1, $t8
move $t2, $v0
lw $v1, 80($sp)
addu $v0, $v1, $t2
move $t8, $v0
lw $v1, 76($sp)
subu $v0, $v1, $t8
move $t2, $v0
lw $v1, 72($sp)
addu $v0, $v1, $t2
move $t8, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t8
move $t2, $v0
lw $v1, 64($sp)
addu $v0, $v1, $t2
move $t8, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t8
move $t2, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t2
move $t8, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t8
move $t2, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t2
move $t8, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t8
move $t2, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t2
move $t8, $v0
lw $v1, 36($sp)
subu $v0, $v1, $t8
move $t2, $v0
lw $v1, 32($sp)
addu $v0, $v1, $t2
move $t8, $v0
li $t2, 1
subu $v0, $t2, $t8
move $s3, $v0
move $t8, $s3
subu $v0, $t4, $t8
move $t2, $v0
addu $v0, $t1, $t2
move $s3, $v0
subu $v0, $s4, $s3
move $t4, $v0
addu $v0, $s5, $t4
move $t8, $v0
subu $v0, $t0, $t8
move $t1, $v0
addu $v0, $t9, $t1
move $t2, $v0
subu $v0, $t6, $t2
move $s4, $v0
addu $v0, $t7, $s4
move $s3, $v0
subu $v0, $s2, $s3
move $s5, $v0
addu $v0, $t3, $s5
move $t4, $v0
subu $v0, $s0, $t4
move $t0, $v0
addu $v0, $s7, $t0
move $t8, $v0
subu $v0, $s6, $t8
move $t9, $v0
addu $v0, $s1, $t9
move $t1, $v0
subu $v0, $t5, $t1
move $t6, $v0
lw $v1, 120($sp)
addu $v0, $v1, $t6
move $t2, $v0
lw $v1, 116($sp)
subu $v0, $v1, $t2
move $t7, $v0
lw $v1, 112($sp)
addu $v0, $v1, $t7
move $s4, $v0
lw $v1, 108($sp)
subu $v0, $v1, $s4
move $s2, $v0
lw $v1, 104($sp)
addu $v0, $v1, $s2
move $s3, $v0
lw $v1, 100($sp)
subu $v0, $v1, $s3
move $t3, $v0
lw $v1, 96($sp)
addu $v0, $v1, $t3
move $s5, $v0
lw $v1, 92($sp)
subu $v0, $v1, $s5
move $s0, $v0
lw $v1, 88($sp)
addu $v0, $v1, $s0
move $t4, $v0
lw $v1, 84($sp)
subu $v0, $v1, $t4
move $s7, $v0
lw $v1, 80($sp)
addu $v0, $v1, $s7
move $t0, $v0
lw $v1, 76($sp)
subu $v0, $v1, $t0
move $s6, $v0
lw $v1, 72($sp)
addu $v0, $v1, $s6
move $t8, $v0
lw $v1, 68($sp)
subu $v0, $v1, $t8
move $s1, $v0
lw $v1, 64($sp)
addu $v0, $v1, $s1
move $t9, $v0
lw $v1, 60($sp)
subu $v0, $v1, $t9
move $t5, $v0
lw $v1, 56($sp)
addu $v0, $v1, $t5
move $t1, $v0
lw $v1, 52($sp)
subu $v0, $v1, $t1
move $t6, $v0
lw $v1, 48($sp)
addu $v0, $v1, $t6
move $t2, $v0
lw $v1, 44($sp)
subu $v0, $v1, $t2
move $t7, $v0
lw $v1, 40($sp)
addu $v0, $v1, $t7
move $s4, $v0
lw $v1, 36($sp)
subu $v0, $v1, $s4
move $s2, $v0
lw $v1, 32($sp)
addu $v0, $v1, $s2
move $s3, $v0
li $t3, 1
subu $v0, $t3, $s3
move $s5, $v0
move $s0, $s5
move $v0, $s0
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 240($sp)
addu $sp, $sp, 248
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
