.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $a0, 4
jal _halloc
move $t0, $v0
li $a0, 4
jal _halloc
move $t1, $v0
la $t2, Fac_ComputeFac
sw $t2, 0($t0)
sw $t0, 0($t1)
move $t3, $t1
lw  $t4, 0($t3)
lw  $t5, 0($t4)
li $t6, 10
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t3
move $a1, $t6
jalr $t5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t7, $v0
move $a0 ,$t7
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	Fac_ComputeFac
Fac_ComputeFac:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1, $a0
move $t2, $a1
li $t0, 10
li $v1, 1
addu $v0, $t0, $v1
move $t3, $v0
li $v1, 4
mult $t3, $v1
mflo $v0
move $t4, $v0
move $a0, $t4
jal _halloc
move $t5, $v0
move $t6, $t5
li $t7, 4
Fac_ComputeFacL0: 
nop
li $v1, 1
addu $v0, $t0, $v1
move $t8, $v0
li $v1, 4
mult $t8, $v1
mflo $v0
move $t9, $v0
slt $v0, $t7, $t9
move $s0, $v0
beqz $s0, Fac_ComputeFacL1
addu $v0, $t6, $t7
move $s1, $v0
li $s2, 0
sw $s2, 0($s1)
li $v1, 4
addu $v0, $t7, $v1
move $s3, $v0
move $t7, $s3
b Fac_ComputeFacL0
Fac_ComputeFacL1: 
nop
sw $t0, 0($t6)
li $v1, 1
slt $v0, $t2, $v1
move $s4, $v0
beqz $s4, Fac_ComputeFacL2
li $s5, 1
b Fac_ComputeFacL3
Fac_ComputeFacL2: 
nop
move $s6, $t1
lw  $s7, 0($s6)
lw  $t3, 0($s7)
li $v1, 1
subu $v0, $t2, $v1
move $t4, $v0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s6
move $a1, $t4
jalr $t3
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t5, $v0
mult $t2, $t5
mflo $v0
move $t8, $v0
move $s5, $t8
Fac_ComputeFacL3: 
nop
move $v0, $s5
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
