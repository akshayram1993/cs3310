.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $a0, 8
jal _halloc
move $t0, $v0
li $a0, 4
jal _halloc
move $t1, $v0
la $t2, somename_init
sw $t2, 0($t0)
la $t3, somename_done
sw $t3, 4($t0)
sw $t0, 0($t1)
move $t4, $t1
lw  $t5, 0($t4)
lw  $t6, 0($t5)
li $t7, 1
li $t8, 2
li $t9, 3
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t4
move $a1, $t7
move $a2, $t8
move $a3, $t9
jalr $t6
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s0, $v0
move $a0 ,$s0
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	somename_init
somename_init:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1, $a0
move $t2, $a1
move $t3, $a2
move $t4, $a3
move $t0, $t4
li $v1, 1
addu $v0, $t0, $v1
move $t5, $v0
li $v1, 4
mult $t5, $v1
mflo $v0
move $t6, $v0
move $a0, $t6
jal _halloc
move $t7, $v0
move $t8, $t7
li $t9, 4
somename_initL0: 
nop
li $v1, 1
addu $v0, $t0, $v1
move $s0, $v0
li $v1, 4
mult $s0, $v1
mflo $v0
move $s1, $v0
slt $v0, $t9, $s1
move $s2, $v0
beqz $s2, somename_initL1
addu $v0, $t8, $t9
move $s3, $v0
li $s4, 0
sw $s4, 0($s3)
li $v1, 4
addu $v0, $t9, $v1
move $s5, $v0
move $t9, $s5
b somename_initL0
somename_initL1: 
nop
sw $t0, 0($t8)
move $s6, $t1
lw  $s7, 0($s6)
lw  $t4, 4($s7)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s6
move $a1, $t2
move $a2, $t3
jalr $t4
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t5, $v0
li $t6, 1
move $v0, $t6
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	somename_done
somename_done:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
li $t0, 0
move $v0, $t0
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
