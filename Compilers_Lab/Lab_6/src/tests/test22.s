.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $t0, 16
move $a0, $t0
jal _halloc
move $t1, $v0
move $t2, $t1
li $t3, 16
move $a0, $t3
jal _halloc
move $t4, $v0
move $t5, $t4
move $t6, $t2
la $t7, __MYTRANSLATE__B25__init__
sw $t7, 12($t6)
move $t8, $t2
la $t9, __MYTRANSLATE__B25__add__
sw $t9, 8($t8)
move $s0, $t2
la $s1, __MYTRANSLATE__B25__init__
sw $s1, 4($s0)
move $s2, $t2
la $s3, __MYTRANSLATE__B25__add__
sw $s3, 0($s2)
li $s4, 4
move $s5, $s4
MAINL0: 
move $s6, $s5
li $s7, 16
slt $v0, $s6, $s7
move $t0, $v0
beqz $t0, MAINL1
move $t1, $t5
move $t3, $s5
addu $v0, $t1, $t3
move $t4, $v0
li $t6, 0
sw $t6, 0($t4)
move $t7, $s5
li $t8, 4
addu $v0, $t7, $t8
move $t9, $v0
move $s5, $t9
b MAINL0
MAINL1: 
move $s0, $t5
move $s1, $t2
sw $s1, 0($s0)
move $s2, $t5
move $s3, $s2
move $s4, $s3
move $s6, $s4
lw  $s7, 0($s6)
move $t0, $s7
lw  $t1, 8($t0)
move $t3, $t1
move $t4, $t3
move $t6, $s4
li $t7, 16
move $a0, $t7
jal _halloc
move $t8, $v0
move $t9, $t8
li $s5, 16
move $a0, $s5
jal _halloc
move $t2, $v0
move $s0, $t2
move $s1, $t9
la $t5, __MYTRANSLATE__C25__init__
sw $t5, 12($s1)
move $s2, $t9
la $s3, __MYTRANSLATE__C25__add__
sw $s3, 8($s2)
move $s6, $t9
la $s7, __MYTRANSLATE__C25__init__
sw $s7, 4($s6)
move $t0, $t9
la $t1, __MYTRANSLATE__C25__add__
sw $t1, 0($t0)
li $t3, 4
move $s4, $t3
MAINL2: 
move $t7, $s4
li $t8, 16
slt $v0, $t7, $t8
move $s5, $v0
beqz $s5, MAINL3
move $t2, $s0
move $s1, $s4
addu $v0, $t2, $s1
move $t5, $v0
li $s2, 0
sw $s2, 0($t5)
move $s3, $s4
li $s6, 4
addu $v0, $s3, $s6
move $s7, $v0
move $s4, $s7
b MAINL2
MAINL3: 
move $t0, $s0
move $t1, $t9
sw $t1, 0($t0)
move $t3, $s0
move $t7, $t3
li $t8, 1
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t6
move $a1, $t7
move $a2, $t8
jalr $t4
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s5, $v0
move $a0 ,$s5
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	__MYTRANSLATE__A25__add__
__MYTRANSLATE__A25__add__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1, $a0
move $t2, $a2
move $t0, $t1
move $t3, $t0
move $t4, $t3
lw  $t5, 0($t4)
move $t6, $t5
lw  $t7, 4($t6)
move $t8, $t7
move $t9, $t8
move $s0, $t3
move $s1, $t2
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s0
move $a1, $s1
jalr $t9
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s2, $v0
move $s3, $s2
move $v0, $s3
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__A25__init__
__MYTRANSLATE__A25__init__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a0
move $t1, $a1
move $t2, $t1
li $t3, 50
slt $v0, $t2, $t3
move $t4, $v0
beqz $t4, __MYTRANSLATE__A25__init__L4
move $t5, $t0
move $t6, $t1
sw $t6, 4($t5)
b __MYTRANSLATE__A25__init__L5
__MYTRANSLATE__A25__init__L4: 
nop
move $t7, $t0
li $t8, 2
move $t9, $t1
mult $t8, $t9
mflo $v0
move $s0, $v0
sw $s0, 4($t7)
__MYTRANSLATE__A25__init__L5: 
nop
move $s1, $t0
lw  $s2, 4($s1)
move $s3, $s2
move $s4, $s3
move $s5, $s4
move $v0, $s5
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__B25__add__
__MYTRANSLATE__B25__add__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1, $a0
move $t2, $a1
move $t3, $a2
move $t0, $t1
move $t4, $t0
move $t5, $t4
lw  $t6, 0($t5)
move $t7, $t6
lw  $t8, 12($t7)
move $t9, $t8
move $s0, $t9
move $s1, $t4
move $s2, $t3
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s1
move $a1, $s2
jalr $s0
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s3, $v0
move $a0 ,$s3
jal _print
move $s4, $t2
move $s5, $s4
move $s6, $s5
lw  $s7, 0($s6)
move $t0, $s7
lw  $t5, 0($t0)
move $t6, $t5
move $t7, $t6
move $t8, $s5
move $t9, $t1
lw  $t4, 8($t9)
move $t3, $t4
move $s0, $t3
move $s1, $t1
lw  $s2, 12($s1)
move $s3, $s2
move $t2, $s3
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t8
move $a1, $s0
move $a2, $t2
jalr $t7
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s4, $v0
move $s6, $s4
move $v0, $s6
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__B25__init__
__MYTRANSLATE__B25__init__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1, $a0
move $t2, $a1
move $t0, $t1
li $t3, 16
move $a0, $t3
jal _halloc
move $t4, $v0
move $t5, $t4
li $t6, 16
move $a0, $t6
jal _halloc
move $t7, $v0
move $t8, $t7
move $t9, $t5
la $s0, __MYTRANSLATE__B25__init__
sw $s0, 12($t9)
move $s1, $t5
la $s2, __MYTRANSLATE__B25__add__
sw $s2, 8($s1)
move $s3, $t5
la $s4, __MYTRANSLATE__B25__init__
sw $s4, 4($s3)
move $s5, $t5
la $s6, __MYTRANSLATE__B25__add__
sw $s6, 0($s5)
li $s7, 4
move $t3, $s7
__MYTRANSLATE__B25__init__L6: 
move $t4, $t3
li $t6, 16
slt $v0, $t4, $t6
move $t7, $v0
beqz $t7, __MYTRANSLATE__B25__init__L7
move $t9, $t8
move $s0, $t3
addu $v0, $t9, $s0
move $s1, $v0
li $s2, 0
sw $s2, 0($s1)
move $s3, $t3
li $s4, 4
addu $v0, $s3, $s4
move $s5, $v0
move $t3, $s5
b __MYTRANSLATE__B25__init__L6
__MYTRANSLATE__B25__init__L7: 
move $s6, $t8
move $s7, $t5
sw $s7, 0($s6)
move $t4, $t8
move $t6, $t4
sw $t6, 8($t0)
move $t7, $t1
li $t9, 2
move $s0, $t2
mult $t9, $s0
mflo $v0
move $s1, $v0
sw $s1, 4($t7)
move $s2, $t1
li $s3, 10
move $s4, $t2
addu $v0, $s3, $s4
move $s5, $v0
sw $s5, 12($s2)
move $t3, $t1
lw  $t5, 4($t3)
move $s6, $t5
move $s7, $s6
move $t8, $t1
lw  $t4, 12($t8)
move $t0, $t4
move $t6, $t0
addu $v0, $s7, $t6
move $t9, $v0
move $s0, $t9
move $v0, $s0
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__C25__add__
__MYTRANSLATE__C25__add__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a0
move $t1, $a1
move $t2, $a2
move $t3, $t0
move $t4, $t3
move $t5, $t4
lw  $t6, 0($t5)
move $t7, $t6
lw  $t8, 12($t7)
move $t9, $t8
move $s0, $t9
move $s1, $t4
move $s2, $t2
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s1
move $a1, $s2
jalr $s0
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s3, $v0
move $a0 ,$s3
jal _print
move $s4, $t0
lw  $s5, 8($s4)
move $s6, $s5
move $s7, $s6
move $t3, $s7
move $t5, $t3
lw  $t6, 0($t5)
move $t7, $t6
lw  $t8, 0($t7)
move $t9, $t8
move $t4, $t9
move $t2, $t3
move $s0, $t1
move $s1, $t0
lw  $s2, 12($s1)
move $s3, $s2
move $s4, $s3
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t2
move $a1, $s0
move $a2, $s4
jalr $t4
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s5, $v0
move $s6, $s5
move $v0, $s6
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__C25__init__
__MYTRANSLATE__C25__init__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a0
move $t1, $a1
move $t2, $t0
li $t3, 8
move $a0, $t3
jal _halloc
move $t4, $v0
move $t5, $t4
li $t6, 12
move $a0, $t6
jal _halloc
move $t7, $v0
move $t8, $t7
move $t9, $t5
la $s0, __MYTRANSLATE__A25__init__
sw $s0, 4($t9)
move $s1, $t5
la $s2, __MYTRANSLATE__A25__add__
sw $s2, 0($s1)
li $s3, 4
move $s4, $s3
__MYTRANSLATE__C25__init__L8: 
move $s5, $s4
li $s6, 12
slt $v0, $s5, $s6
move $s7, $v0
beqz $s7, __MYTRANSLATE__C25__init__L9
move $t3, $t8
move $t4, $s4
addu $v0, $t3, $t4
move $t6, $v0
li $t7, 0
sw $t7, 0($t6)
move $t9, $s4
li $s0, 4
addu $v0, $t9, $s0
move $s1, $v0
move $s4, $s1
b __MYTRANSLATE__C25__init__L8
__MYTRANSLATE__C25__init__L9: 
move $s2, $t8
move $s3, $t5
sw $s3, 0($s2)
move $s5, $t8
move $s6, $s5
sw $s6, 8($t2)
move $s7, $t0
move $t3, $t1
li $t4, 5
subu $v0, $t3, $t4
move $t6, $v0
sw $t6, 4($s7)
move $t7, $t0
move $t9, $t0
lw  $s0, 4($t9)
move $s1, $s0
move $s4, $s1
move $t5, $t1
mult $s4, $t5
mflo $v0
move $s2, $v0
sw $s2, 12($t7)
move $s3, $t0
lw  $t8, 12($s3)
move $s5, $t8
move $t2, $s5
move $s6, $t0
lw  $t3, 4($s6)
move $t4, $t3
move $s7, $t4
subu $v0, $t2, $s7
move $t6, $v0
move $t9, $t6
move $v0, $t9
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
