.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $a0, 4
jal _halloc
move $t0, $v0
li $a0, 4
jal _halloc
move $t1, $v0
la $t2, Fib_ComputeFib
sw $t2, 0($t0)
sw $t0, 0($t1)
move $t3, $t1
lw  $t4, 0($t3)
lw  $t5, 0($t4)
li $t6, 5
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t3
move $a1, $t6
jalr $t5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t7, $v0
move $a0 ,$t7
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	Fib_ComputeFib
Fib_ComputeFib:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a0
move $t1, $a1
li $v1, 0
slt $v0, $t1, $v1
move $t2, $v0
li $t3, 1
subu $v0, $t3, $t2
move $t4, $v0
beqz $t4, Fib_ComputeFibL0
li $t5, 0
b Fib_ComputeFibL1
Fib_ComputeFibL0: 
nop
li $t6, 0
li $v1, 1
slt $v0, $t1, $v1
move $t7, $v0
beqz $t7, Fib_ComputeFibL4
li $v1, 2
slt $v0, $t1, $v1
move $t8, $v0
beqz $t8, Fib_ComputeFibL5
Fib_ComputeFibL5:
nop
li $t6, 1
Fib_ComputeFibL4: 
nop
li $t9, 1
subu $v0, $t9, $t6
move $s0, $v0
beqz $s0, Fib_ComputeFibL2
li $t5, 1
b Fib_ComputeFibL3
Fib_ComputeFibL2: 
nop
move $s1, $t0
lw  $s2, 0($s1)
lw  $s3, 0($s2)
li $v1, 1
subu $v0, $t1, $v1
move $s4, $v0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s1
move $a1, $s4
jalr $s3
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s5, $v0
move $s6, $s5
move $s7, $t0
lw  $t2, 0($s7)
lw  $t3, 0($t2)
li $v1, 2
subu $v0, $t1, $v1
move $t4, $v0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s7
move $a1, $t4
jalr $t3
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t7, $v0
move $t8, $t7
addu $v0, $s6, $t8
move $t6, $v0
move $t5, $t6
Fib_ComputeFibL3: 
nop
Fib_ComputeFibL1: 
nop
move $v0, $t5
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
