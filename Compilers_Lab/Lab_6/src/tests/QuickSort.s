.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $a0, 16
jal _halloc
move $t0, $v0
li $a0, 12
jal _halloc
move $t1, $v0
la $t2, QS_Init
sw $t2, 12($t0)
la $t3, QS_Print
sw $t3, 8($t0)
la $t4, QS_Sort
sw $t4, 4($t0)
la $t5, QS_Start
sw $t5, 0($t0)
li $t6, 4
MAINL0: 
nop
li $t7, 12
slt $v0, $t6, $t7
move $t8, $v0
beqz $t8, MAINL1
addu $v0, $t1, $t6
move $t9, $v0
li $s0, 0
sw $s0, 0($t9)
li $v1, 4
addu $v0, $t6, $v1
move $t6, $v0
b MAINL0
MAINL1: 
nop
sw $t0, 0($t1)
move $s1, $t1
lw  $s2, 0($s1)
lw  $s3, 0($s2)
li $s4, 10
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s1
move $a1, $s4
jalr $s3
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s5, $v0
move $a0 ,$s5
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	QS_Start
QS_Start:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1, $a0
move $t2, $a1
move $t0, $t1
lw  $t3, 0($t0)
lw  $t4, 12($t3)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t0
move $a1, $t2
jalr $t4
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t5, $v0
move $s3, $t5
move $t6, $t1
lw  $t7, 0($t6)
lw  $t8, 8($t7)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t6
jalr $t8
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t9, $v0
move $s3, $t9
li $s0, 9999
move $a0 ,$s0
jal _print
lw  $s1, 8($t1)
li $s2, 1
subu $v0, $s1, $s2
move $s3, $v0
move $s4, $t1
lw  $s5, 0($s4)
lw  $s6, 4($s5)
li $s7, 0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s4
move $a1, $s7
move $a2, $s3
jalr $s6
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t3, $v0
move $s3, $t3
move $t2, $t1
lw  $t0, 0($t2)
lw  $t4, 8($t0)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $t2
jalr $t4
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t5, $v0
move $s3, $t5
li $t7, 0
move $v0, $t7
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	QS_Sort
QS_Sort:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a0
move $t1, $a1
move $t2, $a2
li $t3, 0
slt $v0, $t1, $t2
move $t4, $v0
beqz $t4, QS_SortL2
lw  $t6, 4($t0)
li $v1, 4
mult $t2, $v1
mflo $v0
move $t5, $v0
lw  $t6, 4($t0)
lw  $t7, 0($t6)
li $t8, 1
slt $v0, $t5, $t7
move $t9, $v0
subu $v0, $t8, $t9
move $s0, $v0
beqz $s0, QS_SortL4
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_SortL4: 
nop
li $s1, 4
move $s2, $s1
addu $v0, $t5, $s2
move $s3, $v0
move $s4, $s3
addu $v0, $t6, $s4
move $s5, $v0
lw  $s6, 0($s5)
move $s7, $s6
li $v1, 1
subu $v0, $t1, $v1
move $t4, $v0
move $t7, $t2
li $t8, 1
QS_SortL5: 
nop
beqz $t8, QS_SortL6
li $t9, 1
QS_SortL7: 
nop
beqz $t9, QS_SortL8
li $v1, 1
addu $v0, $t4, $v1
move $t4, $v0
lw  $s1, 4($t0)
li $v1, 4
mult $t4, $v1
mflo $v0
move $s0, $v0
lw  $s1, 4($t0)
lw  $t5, 0($s1)
li $s2, 1
slt $v0, $s0, $t5
move $s3, $v0
subu $v0, $s2, $s3
move $t6, $v0
beqz $t6, QS_SortL9
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_SortL9: 
nop
li $s4, 4
move $s5, $s4
addu $v0, $s0, $s5
move $s6, $v0
move $t5, $s6
addu $v0, $s1, $t5
move $s2, $v0
lw  $s3, 0($s2)
move $t6, $s3
li $s4, 1
slt $v0, $t6, $s7
move $s0, $v0
subu $v0, $s4, $s0
move $s5, $v0
beqz $s5, QS_SortL10
li $t9, 0
b QS_SortL11
QS_SortL10: 
nop
li $t9, 1
QS_SortL11: 
nop
b QS_SortL7
QS_SortL8: 
nop
li $t9, 1
QS_SortL12: 
nop
beqz $t9, QS_SortL13
li $v1, 1
subu $v0, $t7, $v1
move $t7, $v0
lw  $s1, 4($t0)
li $v1, 4
mult $t7, $v1
mflo $v0
move $s6, $v0
lw  $s1, 4($t0)
lw  $t5, 0($s1)
li $s2, 1
slt $v0, $s6, $t5
move $s3, $v0
subu $v0, $s2, $s3
move $s4, $v0
beqz $s4, QS_SortL14
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_SortL14: 
nop
li $s0, 4
move $s5, $s0
addu $v0, $s6, $s5
move $t5, $v0
move $s2, $t5
addu $v0, $s1, $s2
move $s3, $v0
lw  $s4, 0($s3)
move $t6, $s4
li $s0, 1
slt $v0, $s7, $t6
move $s6, $v0
subu $v0, $s0, $s6
move $s5, $v0
beqz $s5, QS_SortL15
li $t9, 0
b QS_SortL16
QS_SortL15: 
nop
li $t9, 1
QS_SortL16: 
nop
b QS_SortL12
QS_SortL13: 
nop
lw  $s1, 4($t0)
li $v1, 4
mult $t4, $v1
mflo $v0
move $t5, $v0
lw  $s1, 4($t0)
lw  $s2, 0($s1)
li $s3, 1
slt $v0, $t5, $s2
move $s4, $v0
subu $v0, $s3, $s4
move $t6, $v0
beqz $t6, QS_SortL17
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_SortL17: 
nop
li $s0, 4
move $s6, $s0
addu $v0, $t5, $s6
move $s5, $v0
move $t9, $s5
addu $v0, $s1, $t9
move $s2, $v0
lw  $s3, 0($s2)
move $t3, $s3
li $s4, 1
li $v1, 4
mult $s4, $v1
mflo $v0
move $t6, $v0
addu $v0, $t0, $t6
move $s0, $v0
lw  $s1, 0($s0)
li $v1, 4
mult $t4, $v1
mflo $v0
move $t5, $v0
li $s6, 1
li $v1, 4
mult $s6, $v1
mflo $v0
move $t6, $v0
addu $v0, $t0, $t6
move $s5, $v0
lw  $s1, 0($s5)
lw  $t9, 0($s1)
li $s2, 1
slt $v0, $t5, $t9
move $s3, $v0
subu $v0, $s2, $s3
move $s4, $v0
beqz $s4, QS_SortL18
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_SortL18: 
nop
li $s0, 4
move $s6, $s0
addu $v0, $t5, $s6
move $t6, $v0
move $s5, $t6
addu $v0, $s1, $s5
move $t9, $v0
lw  $s3, 4($t0)
li $v1, 4
mult $t7, $v1
mflo $v0
move $s2, $v0
lw  $s3, 4($t0)
lw  $s4, 0($s3)
li $s0, 1
slt $v0, $s2, $s4
move $t5, $v0
subu $v0, $s0, $t5
move $s6, $v0
beqz $s6, QS_SortL19
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_SortL19: 
nop
li $t6, 4
move $s1, $t6
addu $v0, $s2, $s1
move $s5, $v0
move $s4, $s5
addu $v0, $s3, $s4
move $s0, $v0
lw  $t5, 0($s0)
sw $t5, 0($t9)
li $s6, 1
li $v1, 4
mult $s6, $v1
mflo $v0
move $t6, $v0
addu $v0, $t0, $t6
move $s2, $v0
lw  $s4, 0($s2)
li $v1, 4
mult $t7, $v1
mflo $v0
move $s1, $v0
li $s5, 1
li $v1, 4
mult $s5, $v1
mflo $v0
move $t6, $v0
addu $v0, $t0, $t6
move $s3, $v0
lw  $s4, 0($s3)
lw  $s0, 0($s4)
li $t9, 1
slt $v0, $s1, $s0
move $t5, $v0
subu $v0, $t9, $t5
move $s6, $v0
beqz $s6, QS_SortL20
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_SortL20: 
nop
li $s2, 4
move $s5, $s2
addu $v0, $s1, $s5
move $t6, $v0
move $s3, $t6
addu $v0, $s4, $s3
move $s0, $v0
sw $t3, 0($s0)
li $t9, 1
addu $v0, $t4, $t9
move $t5, $v0
slt $v0, $t7, $t5
move $s6, $v0
beqz $s6, QS_SortL21
li $t8, 0
b QS_SortL22
QS_SortL21: 
nop
li $t8, 1
QS_SortL22: 
nop
b QS_SortL5
QS_SortL6: 
nop
li $s2, 1
li $v1, 4
mult $s2, $v1
mflo $v0
move $s1, $v0
addu $v0, $t0, $s1
move $s5, $v0
lw  $s0, 0($s5)
li $v1, 4
mult $t7, $v1
mflo $v0
move $t6, $v0
li $s4, 1
li $v1, 4
mult $s4, $v1
mflo $v0
move $s1, $v0
addu $v0, $t0, $s1
move $s3, $v0
lw  $s0, 0($s3)
lw  $t9, 0($s0)
li $t5, 1
slt $v0, $t6, $t9
move $s6, $v0
subu $v0, $t5, $s6
move $t8, $v0
beqz $t8, QS_SortL23
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_SortL23: 
nop
li $s7, 4
move $s2, $s7
addu $v0, $t6, $s2
move $s5, $v0
move $t7, $s5
addu $v0, $s0, $t7
move $s4, $v0
lw  $s3, 4($t0)
li $v1, 4
mult $t4, $v1
mflo $v0
move $s1, $v0
lw  $s3, 4($t0)
lw  $t9, 0($s3)
li $t5, 1
slt $v0, $s1, $t9
move $s6, $v0
subu $v0, $t5, $s6
move $t8, $v0
beqz $t8, QS_SortL24
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_SortL24: 
nop
li $s7, 4
move $t6, $s7
addu $v0, $s1, $t6
move $s2, $v0
move $s5, $s2
addu $v0, $s3, $s5
move $s0, $v0
lw  $t7, 0($s0)
sw $t7, 0($s4)
li $t9, 1
li $v1, 4
mult $t9, $v1
mflo $v0
move $t5, $v0
addu $v0, $t0, $t5
move $s6, $v0
lw  $t6, 0($s6)
li $v1, 4
mult $t4, $v1
mflo $v0
move $t8, $v0
li $s7, 1
li $v1, 4
mult $s7, $v1
mflo $v0
move $t5, $v0
addu $v0, $t0, $t5
move $s1, $v0
lw  $t6, 0($s1)
lw  $s2, 0($t6)
li $s3, 1
slt $v0, $t8, $s2
move $s5, $v0
subu $v0, $s3, $s5
move $s0, $v0
beqz $s0, QS_SortL25
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_SortL25: 
nop
li $s4, 4
move $t7, $s4
addu $v0, $t8, $t7
move $t9, $v0
move $s6, $t9
addu $v0, $t6, $s6
move $s7, $v0
lw  $s1, 4($t0)
li $v1, 4
mult $t2, $v1
mflo $v0
move $t5, $v0
lw  $s1, 4($t0)
lw  $s2, 0($s1)
li $s3, 1
slt $v0, $t5, $s2
move $s5, $v0
subu $v0, $s3, $s5
move $s0, $v0
beqz $s0, QS_SortL26
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_SortL26: 
nop
li $s4, 4
move $t8, $s4
addu $v0, $t5, $t8
move $t7, $v0
move $t9, $t7
addu $v0, $s1, $t9
move $t6, $v0
lw  $s6, 0($t6)
sw $s6, 0($s7)
li $s2, 1
li $v1, 4
mult $s2, $v1
mflo $v0
move $s3, $v0
addu $v0, $t0, $s3
move $s5, $v0
lw  $t8, 0($s5)
li $v1, 4
mult $t2, $v1
mflo $v0
move $s0, $v0
li $s4, 1
li $v1, 4
mult $s4, $v1
mflo $v0
move $s3, $v0
addu $v0, $t0, $s3
move $t5, $v0
lw  $t8, 0($t5)
lw  $t7, 0($t8)
li $s1, 1
slt $v0, $s0, $t7
move $t9, $v0
subu $v0, $s1, $t9
move $t6, $v0
beqz $t6, QS_SortL27
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_SortL27: 
nop
li $s7, 4
move $s6, $s7
addu $v0, $s0, $s6
move $s2, $v0
move $s5, $s2
addu $v0, $t8, $s5
move $s4, $v0
sw $t3, 0($s4)
move $s3, $t0
lw  $t5, 0($s3)
lw  $t7, 4($t5)
li $s1, 1
subu $v0, $t4, $s1
move $t9, $v0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s3
move $a1, $t1
move $a2, $t9
jalr $t7
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t6, $v0
move $s7, $t0
lw  $s0, 0($s7)
lw  $s6, 4($s0)
li $s2, 1
addu $v0, $t4, $s2
move $t8, $v0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0, $s7
move $a1, $t8
move $a2, $t2
jalr $s6
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s5, $v0
b QS_SortL3
QS_SortL2: 
nop
QS_SortL3: 
nop
li $t3, 0
move $v0, $t3
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	QS_Print
QS_Print:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1, $a0
li $t0, 0
QS_PrintL28: 
nop
lw  $t2, 8($t1)
slt $v0, $t0, $t2
move $t3, $v0
beqz $t3, QS_PrintL29
lw  $t5, 4($t1)
li $v1, 4
mult $t0, $v1
mflo $v0
move $t4, $v0
lw  $t5, 4($t1)
lw  $t6, 0($t5)
li $t7, 1
slt $v0, $t4, $t6
move $t8, $v0
subu $v0, $t7, $t8
move $t9, $v0
beqz $t9, QS_PrintL30
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_PrintL30: 
nop
li $s0, 4
move $s1, $s0
addu $v0, $t4, $s1
move $s2, $v0
move $s3, $s2
addu $v0, $t5, $s3
move $s4, $v0
lw  $s5, 0($s4)
move $a0 ,$s5
jal _print
li $v1, 1
addu $v0, $t0, $v1
move $t0, $v0
b QS_PrintL28
QS_PrintL29: 
nop
li $s6, 0
move $v0, $s6
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	QS_Init
QS_Init:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0, $a0
move $t1, $a1
sw $t1, 8($t0)
li $v1, 1
addu $v0, $t1, $v1
move $t2, $v0
li $t3, 4
mult $t2, $t3
mflo $v0
move $t4, $v0
move $a0, $t4
jal _halloc
move $t5, $v0
li $t6, 4
QS_InitL31: 
nop
li $t7, 1
addu $v0, $t1, $t7
move $t8, $v0
li $t9, 4
move $s0, $t9
mult $t8, $s0
mflo $v0
move $s1, $v0
slt $v0, $t6, $s1
move $s2, $v0
beqz $s2, QS_InitL32
addu $v0, $t5, $t6
move $s3, $v0
li $s4, 0
sw $s4, 0($s3)
li $v1, 4
addu $v0, $t6, $v1
move $t6, $v0
b QS_InitL31
QS_InitL32: 
nop
li $s5, 4
mult $t1, $s5
mflo $v0
move $s6, $v0
sw $s6, 0($t5)
sw $t5, 4($t0)
li $s7, 1
li $v1, 4
mult $s7, $v1
mflo $v0
move $t2, $v0
addu $v0, $t0, $t2
move $t3, $v0
lw  $s0, 0($t3)
li $t4, 0
li $v1, 4
mult $t4, $v1
mflo $v0
move $t7, $v0
li $t9, 1
li $v1, 4
mult $t9, $v1
mflo $v0
move $t2, $v0
addu $v0, $t0, $t2
move $t8, $v0
lw  $s0, 0($t8)
lw  $s1, 0($s0)
li $s2, 1
slt $v0, $t7, $s1
move $s3, $v0
subu $v0, $s2, $s3
move $s4, $v0
beqz $s4, QS_InitL33
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_InitL33: 
nop
li $t6, 4
move $t1, $t6
addu $v0, $t7, $t1
move $s5, $v0
move $s6, $s5
addu $v0, $s0, $s6
move $t5, $v0
li $s7, 20
sw $s7, 0($t5)
li $t3, 1
li $v1, 4
mult $t3, $v1
mflo $v0
move $t4, $v0
addu $v0, $t0, $t4
move $t9, $v0
lw  $s3, 0($t9)
li $t2, 1
li $v1, 4
mult $t2, $v1
mflo $v0
move $t8, $v0
li $s1, 1
li $v1, 4
mult $s1, $v1
mflo $v0
move $t4, $v0
addu $v0, $t0, $t4
move $s2, $v0
lw  $s3, 0($s2)
lw  $s4, 0($s3)
li $t6, 1
slt $v0, $t8, $s4
move $t7, $v0
subu $v0, $t6, $t7
move $t1, $v0
beqz $t1, QS_InitL34
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_InitL34: 
nop
li $s5, 4
move $s0, $s5
addu $v0, $t8, $s0
move $s6, $v0
move $t5, $s6
addu $v0, $s3, $t5
move $s7, $v0
li $t3, 7
sw $t3, 0($s7)
li $t9, 1
li $v1, 4
mult $t9, $v1
mflo $v0
move $t2, $v0
addu $v0, $t0, $t2
move $s1, $v0
lw  $t7, 0($s1)
li $t4, 2
li $v1, 4
mult $t4, $v1
mflo $v0
move $s2, $v0
li $s4, 1
li $v1, 4
mult $s4, $v1
mflo $v0
move $t2, $v0
addu $v0, $t0, $t2
move $t6, $v0
lw  $t7, 0($t6)
lw  $t1, 0($t7)
li $s5, 1
slt $v0, $s2, $t1
move $t8, $v0
subu $v0, $s5, $t8
move $s0, $v0
beqz $s0, QS_InitL35
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_InitL35: 
nop
li $s6, 4
move $s3, $s6
addu $v0, $s2, $s3
move $t5, $v0
move $s7, $t5
addu $v0, $t7, $s7
move $t3, $v0
li $t9, 12
sw $t9, 0($t3)
li $s1, 1
li $v1, 4
mult $s1, $v1
mflo $v0
move $t4, $v0
addu $v0, $t0, $t4
move $s4, $v0
lw  $t8, 0($s4)
li $t2, 3
li $v1, 4
mult $t2, $v1
mflo $v0
move $t6, $v0
li $t1, 1
li $v1, 4
mult $t1, $v1
mflo $v0
move $t4, $v0
addu $v0, $t0, $t4
move $s5, $v0
lw  $t8, 0($s5)
lw  $s0, 0($t8)
li $s6, 1
slt $v0, $t6, $s0
move $s2, $v0
subu $v0, $s6, $s2
move $s3, $v0
beqz $s3, QS_InitL36
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_InitL36: 
nop
li $t5, 4
move $t7, $t5
addu $v0, $t6, $t7
move $s7, $v0
move $t3, $s7
addu $v0, $t8, $t3
move $t9, $v0
li $s1, 18
sw $s1, 0($t9)
li $s4, 1
li $v1, 4
mult $s4, $v1
mflo $v0
move $t2, $v0
addu $v0, $t0, $t2
move $t1, $v0
lw  $s2, 0($t1)
li $t4, 4
li $v1, 4
mult $t4, $v1
mflo $v0
move $s5, $v0
li $s0, 1
li $v1, 4
mult $s0, $v1
mflo $v0
move $t2, $v0
addu $v0, $t0, $t2
move $s6, $v0
lw  $s2, 0($s6)
lw  $s3, 0($s2)
li $t5, 1
slt $v0, $s5, $s3
move $t6, $v0
subu $v0, $t5, $t6
move $t7, $v0
beqz $t7, QS_InitL37
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_InitL37: 
nop
li $s7, 4
move $t8, $s7
addu $v0, $s5, $t8
move $t3, $v0
move $t9, $t3
addu $v0, $s2, $t9
move $s1, $v0
li $s4, 2
sw $s4, 0($s1)
li $t1, 1
li $v1, 4
mult $t1, $v1
mflo $v0
move $t4, $v0
addu $v0, $t0, $t4
move $s0, $v0
lw  $t6, 0($s0)
li $t2, 5
li $v1, 4
mult $t2, $v1
mflo $v0
move $s6, $v0
li $s3, 1
li $v1, 4
mult $s3, $v1
mflo $v0
move $t4, $v0
addu $v0, $t0, $t4
move $t5, $v0
lw  $t6, 0($t5)
lw  $t7, 0($t6)
li $s7, 1
slt $v0, $s6, $t7
move $s5, $v0
subu $v0, $s7, $s5
move $t8, $v0
beqz $t8, QS_InitL38
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_InitL38: 
nop
li $t3, 4
move $s2, $t3
addu $v0, $s6, $s2
move $t9, $v0
move $s1, $t9
addu $v0, $t6, $s1
move $s4, $v0
li $t1, 11
sw $t1, 0($s4)
li $s0, 1
li $v1, 4
mult $s0, $v1
mflo $v0
move $t2, $v0
addu $v0, $t0, $t2
move $s3, $v0
lw  $s5, 0($s3)
li $t4, 6
li $v1, 4
mult $t4, $v1
mflo $v0
move $t5, $v0
li $t7, 1
li $v1, 4
mult $t7, $v1
mflo $v0
move $t2, $v0
addu $v0, $t0, $t2
move $s7, $v0
lw  $s5, 0($s7)
lw  $t8, 0($s5)
li $t3, 1
slt $v0, $t5, $t8
move $s6, $v0
subu $v0, $t3, $s6
move $s2, $v0
beqz $s2, QS_InitL39
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_InitL39: 
nop
li $t9, 4
move $t6, $t9
addu $v0, $t5, $t6
move $s1, $v0
move $s4, $s1
addu $v0, $s5, $s4
move $t1, $v0
li $s0, 6
sw $s0, 0($t1)
li $s3, 1
li $v1, 4
mult $s3, $v1
mflo $v0
move $t4, $v0
addu $v0, $t0, $t4
move $t7, $v0
lw  $s6, 0($t7)
li $t2, 7
li $v1, 4
mult $t2, $v1
mflo $v0
move $s7, $v0
li $t8, 1
li $v1, 4
mult $t8, $v1
mflo $v0
move $t4, $v0
addu $v0, $t0, $t4
move $t3, $v0
lw  $s6, 0($t3)
lw  $s2, 0($s6)
li $t9, 1
slt $v0, $s7, $s2
move $t5, $v0
subu $v0, $t9, $t5
move $t6, $v0
beqz $t6, QS_InitL40
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_InitL40: 
nop
li $s1, 4
move $s5, $s1
addu $v0, $s7, $s5
move $s4, $v0
move $t1, $s4
addu $v0, $s6, $t1
move $s0, $v0
li $s3, 9
sw $s3, 0($s0)
li $t7, 1
li $v1, 4
mult $t7, $v1
mflo $v0
move $t2, $v0
addu $v0, $t0, $t2
move $t8, $v0
lw  $t5, 0($t8)
li $t4, 8
li $v1, 4
mult $t4, $v1
mflo $v0
move $t3, $v0
li $s2, 1
li $v1, 4
mult $s2, $v1
mflo $v0
move $t2, $v0
addu $v0, $t0, $t2
move $t9, $v0
lw  $t5, 0($t9)
lw  $t6, 0($t5)
li $s1, 1
slt $v0, $t3, $t6
move $s7, $v0
subu $v0, $s1, $s7
move $s5, $v0
beqz $s5, QS_InitL41
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_InitL41: 
nop
li $s4, 4
move $s6, $s4
addu $v0, $t3, $s6
move $t1, $v0
move $s0, $t1
addu $v0, $t5, $s0
move $s3, $v0
li $t7, 19
sw $t7, 0($s3)
li $t8, 1
li $v1, 4
mult $t8, $v1
mflo $v0
move $t4, $v0
addu $v0, $t0, $t4
move $s2, $v0
lw  $s7, 0($s2)
li $t2, 9
li $v1, 4
mult $t2, $v1
mflo $v0
move $t9, $v0
li $t6, 1
li $v1, 4
mult $t6, $v1
mflo $v0
move $t4, $v0
addu $v0, $t0, $t4
move $s1, $v0
lw  $s7, 0($s1)
lw  $s5, 0($s7)
li $s4, 1
slt $v0, $t9, $s5
move $t3, $v0
subu $v0, $s4, $t3
move $s6, $v0
beqz $s6, QS_InitL42
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
QS_InitL42: 
nop
li $t1, 4
move $t5, $t1
addu $v0, $t9, $t5
move $s0, $v0
move $s3, $s0
addu $v0, $s7, $s3
move $t7, $v0
li $t8, 5
sw $t8, 0($t7)
li $s2, 0
move $v0, $s2
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
