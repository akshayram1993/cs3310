import syntaxtree.*;
import visitor.*;

public class Main {
   public static void main(String [] args) {
      try {
         Node root = new MiniRAParser(System.in).Goal();
//       System.out.println("Program parsed successfully");
         FirstVisitor f = new FirstVisitor();
         root.accept(f);
         
         
         SecondVisitor s = new SecondVisitor();
         s.labels = f.labels;
         root.accept(s); // Your assignment part is invoked here.
      }
      catch (ParseException e) {
         System.out.println(e.toString());
      }
   }
} 



