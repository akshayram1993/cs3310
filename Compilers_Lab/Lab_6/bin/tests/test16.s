.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $t0 12
move $a0, $t0
jal _halloc
move $t1 $v0
move $t2 $t1
li $t3 8
move $a0, $t3
jal _halloc
move $t4 $v0
move $t5 $t4
move $t6 $t2
la $t7 __MYTRANSLATE__Test__second__
sw $t7, 8($t6)
move $t8 $t2
la $t9 __MYTRANSLATE__Test__start__
sw $t9, 0($t8)
move $s0 $t2
la $s1 __MYTRANSLATE__Test__first__
sw $s1, 4($s0)
li $s2 4
move $s3 $s2
MAINL0: 
move $s4 $s3
li $s5 8
slt $v0, $s4, $s5
move $s6 $v0
beqz $s6 MAINL1
move $s7 $t5
move $t0 $s3
addu $v0, $s7, $t0
move $t1 $v0
li $t3 0
sw $t3, 0($t1)
move $t4 $s3
li $t6 4
addu $v0, $t4, $t6
move $t7 $v0
move $s3 $t7
b MAINL0
MAINL1: 
move $t8 $t5
move $t9 $t2
sw $t9, 0($t8)
move $s0 $t5
move $s1 $s0
move $s2 $s1
move $s4 $s2
lw  $s5 0($s4)
move $s6 $s5
lw  $s7 0($s6)
move $t0 $s7
move $t1 $t0
move $t3 $s2
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t3
jalr $t1
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t4 $v0
move $a0 ,$t4
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	__MYTRANSLATE__Test__start__
__MYTRANSLATE__Test__start__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
li $t0 12
move $a0, $t0
jal _halloc
move $t2 $v0
move $t3 $t2
li $t4 8
move $a0, $t4
jal _halloc
move $t5 $v0
move $t6 $t5
move $t7 $t3
la $t8 __MYTRANSLATE__Test__second__
sw $t8, 8($t7)
move $t9 $t3
la $s0 __MYTRANSLATE__Test__start__
sw $s0, 0($t9)
move $s1 $t3
la $s2 __MYTRANSLATE__Test__first__
sw $s2, 4($s1)
li $s3 4
move $s4 $s3
__MYTRANSLATE__Test__start__L2: 
move $s5 $s4
li $s6 8
slt $v0, $s5, $s6
move $s7 $v0
beqz $s7 __MYTRANSLATE__Test__start__L3
move $t0 $t6
move $t2 $s4
addu $v0, $t0, $t2
move $t4 $v0
li $t5 0
sw $t5, 0($t4)
move $t7 $s4
li $t8 4
addu $v0, $t7, $t8
move $t9 $v0
move $s4 $t9
b __MYTRANSLATE__Test__start__L2
__MYTRANSLATE__Test__start__L3: 
move $s0 $t6
move $s1 $t3
sw $s1, 0($s0)
move $s2 $t6
move $s3 $s2
move $s5 $s3
move $s6 $t1
li $s7 10
sw $s7, 4($s6)
move $t0 $t1
move $t2 $t1
lw  $t4 4($t2)
move $t5 $t4
move $t7 $t5
move $t8 $s5
move $t9 $t8
move $s4 $t9
lw  $t3 0($s4)
move $s0 $t3
lw  $s1 4($s0)
move $t6 $s1
move $s2 $t6
move $s3 $t9
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s3
jalr $s2
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s6 $v0
move $s7 $s6
move $t2 $s7
lw  $t4 0($t2)
move $t5 $t4
lw  $s5 8($t5)
move $t8 $s5
move $s4 $t8
move $t3 $s7
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t3
jalr $s4
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s0 $v0
addu $v0, $t7, $s0
move $s1 $v0
sw $s1, 4($t0)
move $t6 $t1
lw  $t9 4($t6)
move $s2 $t9
move $s3 $s2
move $s6 $s3
move $v0 $s6
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__Test__first__
__MYTRANSLATE__Test__first__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
li $t0 12
move $a0, $t0
jal _halloc
move $t1 $v0
move $t2 $t1
li $t3 8
move $a0, $t3
jal _halloc
move $t4 $v0
move $t5 $t4
move $t6 $t2
la $t7 __MYTRANSLATE__Test__second__
sw $t7, 8($t6)
move $t8 $t2
la $t9 __MYTRANSLATE__Test__start__
sw $t9, 0($t8)
move $s0 $t2
la $s1 __MYTRANSLATE__Test__first__
sw $s1, 4($s0)
li $s2 4
move $s3 $s2
__MYTRANSLATE__Test__first__L4: 
move $s4 $s3
li $s5 8
slt $v0, $s4, $s5
move $s6 $v0
beqz $s6 __MYTRANSLATE__Test__first__L5
move $s7 $t5
move $t0 $s3
addu $v0, $s7, $t0
move $t1 $v0
li $t3 0
sw $t3, 0($t1)
move $t4 $s3
li $t6 4
addu $v0, $t4, $t6
move $t7 $v0
move $s3 $t7
b __MYTRANSLATE__Test__first__L4
__MYTRANSLATE__Test__first__L5: 
move $t8 $t5
move $t9 $t2
sw $t9, 0($t8)
move $s0 $t5
move $s1 $s0
move $s2 $s1
move $v0 $s2
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__Test__second__
__MYTRANSLATE__Test__second__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
move $t0 $t1
move $t2 $t1
lw  $t3 4($t2)
move $t4 $t3
move $t5 $t4
li $t6 10
addu $v0, $t5, $t6
move $t7 $v0
sw $t7, 4($t0)
move $t8 $t1
lw  $t9 4($t8)
move $s0 $t9
move $s1 $s0
move $s2 $s1
move $v0 $s2
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
