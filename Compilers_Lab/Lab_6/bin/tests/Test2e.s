.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $a0, 1
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	Finder_Find
Finder_Find:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
li $t0 10
li $v1, 1
addu $v0, $t0, $v1
move $t1 $v0
li $v1, 4
mult $t1 $v1
mflo $v0
move $t2 $v0
move $a0, $t2
jal _halloc
move $t3 $v0
move $t4 $t3
li $t5 4
Finder_FindL0: 
nop
li $v1, 1
addu $v0, $t0, $v1
move $t6 $v0
li $v1, 4
mult $t6 $v1
mflo $v0
move $t7 $v0
slt $v0, $t5, $t7
move $t8 $v0
beqz $t8 Finder_FindL1
addu $v0, $t4, $t5
move $t9 $v0
li $s0 0
sw $s0, 0($t9)
li $v1, 4
addu $v0, $t5, $v1
move $s1 $v0
move $t5 $s1
b Finder_FindL0
Finder_FindL1: 
nop
sw $t0, 0($t4)
move $s2 $t4
li $s3 0
move $s4 $s2
li $s5 0
li $v1, 0
slt $v0, $s5, $v1
move $s6 $v0
beqz $s6 Finder_FindL2
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
Finder_FindL2: 
nop
lw  $s7 0($s4)
slt $v0, $s5, $s7
move $t1 $v0
li $t2 1
subu $v0, $t2, $t1
move $t3 $v0
beqz $t3 Finder_FindL3
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
Finder_FindL3: 
nop
li $t6 4
mult $t6 $s5
mflo $v0
move $t7 $v0
move $s5 $t7
li $v1, 4
addu $v0, $s5, $v1
move $t8 $v0
addu $v0, $s4, $t8
move $t9 $v0
sw $s3, 0($t9)
li $s0 0
move $v0 $s0
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
