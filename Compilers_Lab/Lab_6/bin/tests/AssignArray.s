.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $a0, 4
jal _halloc
move $t0 $v0
li $a0, 8
jal _halloc
move $t1 $v0
la $t2 Ss_Compute
sw $t2, 0($t0)
sw $t0, 0($t1)
li $t3 0
sw $t3, 4($t1)
move $t4 $t1
lw  $t5 0($t4)
lw  $t6 0($t5)
li $t7 1
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t4
move $a1 $t7
jalr $t6
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t8 $v0
move $a0 ,$t8
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	Ss_Compute
Ss_Compute:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
li $t0 10
li $t2 15
li $v1, 1
addu $v0, $t2, $v1
move $t3 $v0
li $v1, 4
mult $t3 $v1
mflo $v0
move $t4 $v0
move $a0, $t4
jal _halloc
move $t5 $v0
move $t6 $t5
li $t7 4
Ss_ComputeL0: 
nop
li $v1, 1
addu $v0, $t2, $v1
move $t8 $v0
li $v1, 4
mult $t8 $v1
mflo $v0
move $t9 $v0
slt $v0, $t7, $t9
move $s0 $v0
beqz $s0 Ss_ComputeL1
addu $v0, $t6, $t7
move $s1 $v0
li $s2 0
sw $s2, 0($s1)
li $v1, 4
addu $v0, $t7, $v1
move $s3 $v0
move $t7 $s3
b Ss_ComputeL0
Ss_ComputeL1: 
nop
sw $t2, 0($t6)
sw $t6, 4($t1)
move $s4 $t0
li $v1, 1
addu $v0, $s4, $v1
move $s5 $v0
li $v1, 4
mult $s5 $v1
mflo $v0
move $s6 $v0
move $a0, $s6
jal _halloc
move $s7 $v0
move $t3 $s7
li $t4 4
Ss_ComputeL2: 
nop
li $v1, 1
addu $v0, $s4, $v1
move $t5 $v0
li $v1, 4
mult $t5 $v1
mflo $v0
move $t8 $v0
slt $v0, $t4, $t8
move $t9 $v0
beqz $t9 Ss_ComputeL3
addu $v0, $t3, $t4
move $s0 $v0
li $s1 0
sw $s1, 0($s0)
li $v1, 4
addu $v0, $t4, $v1
move $s2 $v0
move $t4 $s2
b Ss_ComputeL2
Ss_ComputeL3: 
nop
sw $s4, 0($t3)
move $s3 $t3
lw  $t7 4($t1)
move $t2 $t0
li $v1, 0
slt $v0, $t2, $v1
move $t6 $v0
beqz $t6 Ss_ComputeL4
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
Ss_ComputeL4: 
nop
lw  $s5 0($t7)
slt $v0, $t2, $s5
move $s6 $v0
li $s7 1
subu $v0, $s7, $s6
move $t5 $v0
beqz $t5 Ss_ComputeL5
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
Ss_ComputeL5: 
nop
li $t8 4
mult $t8 $t2
mflo $v0
move $t9 $v0
move $t2 $t9
li $v1, 4
addu $v0, $t2, $v1
move $s0 $v0
addu $v0, $t7, $s0
move $s1 $v0
move $s2 $s3
li $t4 5
li $v1, 0
slt $v0, $t4, $v1
move $s4 $v0
beqz $s4 Ss_ComputeL6
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
Ss_ComputeL6: 
nop
lw  $t3 0($s2)
slt $v0, $t4, $t3
move $t1 $v0
li $t0 1
subu $v0, $t0, $t1
move $t6 $v0
beqz $t6 Ss_ComputeL7
li $v0, 4
la $a0, str_er
syscall
li $v0, 10
syscall
Ss_ComputeL7: 
nop
li $s5 4
mult $s5 $t4
mflo $v0
move $s6 $v0
move $t4 $s6
li $v1, 4
addu $v0, $t4, $v1
move $s7 $v0
addu $v0, $s2, $s7
move $t5 $v0
lw  $t8 0($t5)
sw $t8, 0($s1)
li $t9 10
move $v0 $t9
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
