.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $t0 8
move $a0, $t0
jal _halloc
move $t1 $v0
move $t2 $t1
li $t3 20
move $a0, $t3
jal _halloc
move $t4 $v0
move $t5 $t4
move $t6 $t2
la $t7 __MYTRANSLATE__A24__m1__
sw $t7, 0($t6)
move $t8 $t2
la $t9 __MYTRANSLATE__A24__m2__
sw $t9, 4($t8)
li $s0 4
move $s1 $s0
MAINL0: 
move $s2 $s1
li $s3 20
slt $v0, $s2, $s3
move $s4 $v0
beqz $s4 MAINL1
move $s5 $t5
move $s6 $s1
addu $v0, $s5, $s6
move $s7 $v0
li $t0 0
sw $t0, 0($s7)
move $t1 $s1
li $t3 4
addu $v0, $t1, $t3
move $t4 $v0
move $s1 $t4
b MAINL0
MAINL1: 
move $t6 $t5
move $t7 $t2
sw $t7, 0($t6)
move $t8 $t5
move $t9 $t8
move $s0 $t9
move $s2 $s0
lw  $s3 0($s2)
move $s4 $s3
lw  $s5 0($s4)
move $s6 $s5
move $s7 $s6
move $t0 $s0
li $t1 0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t0
move $a1 $t1
jalr $s7
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t3 $v0
move $a0 ,$t3
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	__MYTRANSLATE__A24__m1__
__MYTRANSLATE__A24__m1__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0 $a0
move $t1 $a1
move $t2 $t1
beqz $t2 __MYTRANSLATE__A24__m1__L2
li $t3 5
move $t4 $t3
b __MYTRANSLATE__A24__m1__L3
__MYTRANSLATE__A24__m1__L2: 
nop
li $t5 10
move $t4 $t5
__MYTRANSLATE__A24__m1__L3: 
nop
move $t6 $t0
move $t7 $t4
li $t8 1
addu $v0, $t7, $t8
move $t9 $v0
li $s0 4
mult $t9 $s0
mflo $v0
move $s1 $v0
move $a0, $s1
jal _halloc
move $s2 $v0
move $s3 $s2
li $s4 4
move $s5 $s4
__MYTRANSLATE__A24__m1__L4: 
move $s6 $s5
move $s7 $t4
li $t1 1
addu $v0, $s7, $t1
move $t2 $v0
li $t3 4
mult $t2 $t3
mflo $v0
move $t5 $v0
slt $v0, $s6, $t5
move $t7 $v0
beqz $t7 __MYTRANSLATE__A24__m1__L5
move $t8 $s3
move $t9 $s5
addu $v0, $t8, $t9
move $s0 $v0
li $s1 0
sw $s1, 0($s0)
move $s2 $s5
li $s4 4
addu $v0, $s2, $s4
move $s7 $v0
move $s5 $s7
b __MYTRANSLATE__A24__m1__L4
__MYTRANSLATE__A24__m1__L5: 
move $t1 $s3
move $t2 $t4
sw $t2, 0($t1)
move $t3 $s3
move $s6 $t3
sw $s6, 8($t6)
li $t5 0
move $t4 $t5
move $t7 $t0
lw  $t8 8($t7)
move $t9 $t8
move $s0 $t9
lw  $s1 0($s0)
move $s2 $s1
move $s4 $s2
move $s7 $s4
__MYTRANSLATE__A24__m1__L6: 
move $s5 $t4
move $t1 $s7
slt $v0, $s5, $t1
move $t2 $v0
beqz $t2 __MYTRANSLATE__A24__m1__L7
move $s3 $t0
lw  $t3 8($s3)
move $t6 $t3
move $s6 $t6
move $t5 $t4
li $t7 4
mult $t5 $t7
mflo $v0
move $t8 $v0
move $t9 $t8
move $s0 $t9
move $s1 $s0
li $s2 4
addu $v0, $s1, $s2
move $s4 $v0
addu $v0, $s6, $s4
move $s5 $v0
move $t1 $t4
li $t2 1
addu $v0, $t1, $t2
move $s3 $v0
sw $s3, 0($s5)
move $t3 $t4
li $t6 1
addu $v0, $t3, $t6
move $t5 $v0
move $t4 $t5
b __MYTRANSLATE__A24__m1__L6
__MYTRANSLATE__A24__m1__L7: 
nop
move $t7 $t0
li $t8 12
move $a0, $t8
jal _halloc
move $t9 $v0
move $s0 $t9
li $s1 20
move $a0, $s1
jal _halloc
move $s2 $v0
move $s6 $s2
move $s4 $s0
la $t1 __MYTRANSLATE__B24__m2__
sw $t1, 8($s4)
move $t2 $s0
la $s5 __MYTRANSLATE__A24__m1__
sw $s5, 0($t2)
move $s3 $s0
la $t3 __MYTRANSLATE__B24__m2__
sw $t3, 4($s3)
li $t6 4
move $t5 $t6
__MYTRANSLATE__A24__m1__L8: 
move $t4 $t5
li $s7 20
slt $v0, $t4, $s7
move $t8 $v0
beqz $t8 __MYTRANSLATE__A24__m1__L9
move $t9 $s6
move $s1 $t5
addu $v0, $t9, $s1
move $s2 $v0
li $s4 0
sw $s4, 0($s2)
move $t1 $t5
li $t2 4
addu $v0, $t1, $t2
move $s5 $v0
move $t5 $s5
b __MYTRANSLATE__A24__m1__L8
__MYTRANSLATE__A24__m1__L9: 
move $s3 $s6
move $t3 $s0
sw $t3, 0($s3)
move $t6 $s6
move $t4 $t6
sw $t4, 16($t7)
move $s7 $t0
move $t8 $t0
lw  $t9 16($t8)
move $s1 $t9
move $s2 $s1
sw $s2, 12($s7)
move $s4 $t0
lw  $t1 12($s4)
move $t2 $t1
move $s5 $t2
move $t5 $s5
move $s0 $t5
lw  $s3 0($s0)
move $t3 $s3
lw  $s6 4($t3)
move $t6 $s6
move $t7 $t6
move $t4 $t5
move $t8 $t0
lw  $t9 8($t8)
move $s1 $t9
move $s7 $s1
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t4
move $a1 $s7
jalr $t7
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s2 $v0
move $s4 $s2
move $v0 $s4
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__A24__m2__
__MYTRANSLATE__A24__m2__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
li $t0 1
move $t1 $t0
move $v0 $t1
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__B24__m2__
__MYTRANSLATE__B24__m2__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0 $a1
li $t1 0
move $t2 $t1
li $t3 0
move $t4 $t3
move $t5 $t0
lw  $t6 0($t5)
move $t7 $t6
move $t8 $t7
move $t9 $t8
__MYTRANSLATE__B24__m2__L10: 
move $s0 $t2
move $s1 $t9
slt $v0, $s0, $s1
move $s2 $v0
beqz $s2 __MYTRANSLATE__B24__m2__L11
move $s3 $t0
move $s4 $t2
li $s5 4
mult $s4 $s5
mflo $v0
move $s6 $v0
move $s7 $s6
move $t1 $s7
move $t3 $t1
li $t5 4
addu $v0, $t3, $t5
move $t6 $v0
addu $v0, $s3, $t6
move $t7 $v0
lw  $t8 0($t7)
move $s0 $t8
move $s1 $s0
move $s2 $s1
move $s4 $t4
move $s5 $s2
addu $v0, $s4, $s5
move $s6 $v0
move $t4 $s6
move $s7 $t2
li $t1 1
addu $v0, $s7, $t1
move $t3 $v0
move $t2 $t3
b __MYTRANSLATE__B24__m2__L10
__MYTRANSLATE__B24__m2__L11: 
nop
move $t5 $t4
move $s3 $t5
move $v0 $s3
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
