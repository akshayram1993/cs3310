.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $a0, 4
jal _halloc
move $t0 $v0
li $a0, 4
jal _halloc
move $t1 $v0
la $t2 Coin_toss
sw $t2, 0($t0)
sw $t0, 0($t1)
move $t3 $t1
lw  $t4 0($t3)
lw  $t5 0($t4)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t3
jalr $t5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t6 $v0
move $a0 ,$t6
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	Coin_toss
Coin_toss:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
li $t0 1
beqz $t0 Coin_tossL0
li $t0 1
b Coin_tossL1
Coin_tossL0: 
nop
li $t0 1
Coin_tossL1: 
nop
li $t1 0
move $v0 $t1
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
