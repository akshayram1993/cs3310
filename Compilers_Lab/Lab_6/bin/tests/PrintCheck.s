.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $a0, 4
jal _halloc
move $t0 $v0
li $a0, 8
jal _halloc
move $t1 $v0
la $t2 Ss_Compute
sw $t2, 0($t0)
sw $t0, 0($t1)
li $t3 0
sw $t3, 4($t1)
move $t4 $t1
lw  $t5 0($t4)
lw  $t6 0($t5)
li $t7 10
li $t8 11
li $t9 1
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t4
move $a1 $t7
move $a2 $t8
move $a3 $t9
jalr $t6
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s0 $v0
move $a0 ,$s0
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	Ss_Compute
Ss_Compute:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
li $t0 15
li $v1, 1
addu $v0, $t0, $v1
move $t2 $v0
li $v1, 4
mult $t2 $v1
mflo $v0
move $t3 $v0
move $a0, $t3
jal _halloc
move $t4 $v0
move $t5 $t4
li $t6 4
Ss_ComputeL0: 
nop
li $v1, 1
addu $v0, $t0, $v1
move $t7 $v0
li $v1, 4
mult $t7 $v1
mflo $v0
move $t8 $v0
slt $v0, $t6, $t8
move $t9 $v0
beqz $t9 Ss_ComputeL1
addu $v0, $t5, $t6
move $s0 $v0
li $s1 0
sw $s1, 0($s0)
li $v1, 4
addu $v0, $t6, $v1
move $s2 $v0
move $t6 $s2
b Ss_ComputeL0
Ss_ComputeL1: 
nop
sw $t0, 0($t5)
sw $t5, 4($t1)
lw  $s3 4($t1)
move $s4 $s3
lw  $s5 0($s4)
move $s6 $s5
move $v0 $s6
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
