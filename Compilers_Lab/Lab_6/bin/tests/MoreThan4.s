.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 164
sw $ra, -4($fp)
li $a0, 8
jal _halloc
move $t0 $v0
li $a0, 4
jal _halloc
move $t1 $v0
la $t2 MT4_Change
sw $t2, 4($t0)
la $t3 MT4_Start
sw $t3, 0($t0)
sw $t0, 0($t1)
move $t4 $t1
lw  $t5 0($t4)
lw  $t6 0($t5)
li $t7 1
li $t8 2
li $t9 3
li $s0 4
li $s1 5
li $s2 6
sw $t0, 44($sp)
sw $t1, 48($sp)
sw $t2, 52($sp)
sw $t3, 56($sp)
sw $t4, 60($sp)
sw $t5, 64($sp)
sw $t6, 68($sp)
sw $t7, 72($sp)
sw $t8, 76($sp)
sw $t9, 80($sp)
move $a0 $t4
move $a1 $t7
move $a2 $t8
move $a3 $t9
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
jalr $t6
lw $t0, 44($sp)
lw $t1, 48($sp)
lw $t2, 52($sp)
lw $t3, 56($sp)
lw $t4, 60($sp)
lw $t5, 64($sp)
lw $t6, 68($sp)
lw $t7, 72($sp)
lw $t8, 76($sp)
lw $t9, 80($sp)
move $s3 $v0
move $a0 ,$s3
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 164
jr $ra
.text
.globl	MT4_Start
MT4_Start:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 180
sw $ra, -4($fp)
sw $s0, 24($sp)
sw $s1, 28($sp)
sw $s2, 32($sp)
sw $s3, 36($sp)
sw $s4, 40($sp)
sw $s5, 44($sp)
sw $s6, 48($sp)
sw $s7, 52($sp)
move $t3 $a0
move $t4 $a1
move $t5 $a2
move $t6 $a3
lw $v1, 0($fp)
move $t0 $v1
lw $v1, 4($fp)
move $t1 $v1
lw $v1, 8($fp)
move $t2 $v1
move $a0 ,$t4
jal _print
move $a0 ,$t5
jal _print
move $a0 ,$t6
jal _print
move $a0 ,$t0
jal _print
move $a0 ,$t1
jal _print
move $a0 ,$t2
jal _print
move $t7 $t3
lw  $t8 0($t7)
lw  $t9 4($t8)
sw $t0, 56($sp)
sw $t1, 60($sp)
sw $t2, 64($sp)
sw $t3, 68($sp)
sw $t4, 72($sp)
sw $t5, 76($sp)
sw $t6, 80($sp)
sw $t7, 84($sp)
sw $t8, 88($sp)
sw $t9, 92($sp)
move $a0 $t7
move $a1 $t2
move $a2 $t1
move $a3 $t0
sw $t6, 0($sp)
sw $t5, 4($sp)
sw $t4, 8($sp)
jalr $t9
lw $t0, 56($sp)
lw $t1, 60($sp)
lw $t2, 64($sp)
lw $t3, 68($sp)
lw $t4, 72($sp)
lw $t5, 76($sp)
lw $t6, 80($sp)
lw $t7, 84($sp)
lw $t8, 88($sp)
lw $t9, 92($sp)
move $s0 $v0
move $s1 $s0
move $v0 $s1
lw $s0, 24($sp)
lw $s1, 28($sp)
lw $s2, 32($sp)
lw $s3, 36($sp)
lw $s4, 40($sp)
lw $s5, 44($sp)
lw $s6, 48($sp)
lw $s7, 52($sp)
lw $ra, -4($fp)
lw $fp, 172($sp)
addu $sp, $sp, 180
jr $ra
.text
.globl	MT4_Change
MT4_Change:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 168
sw $ra, -4($fp)
sw $s0, 12($sp)
sw $s1, 16($sp)
sw $s2, 20($sp)
sw $s3, 24($sp)
sw $s4, 28($sp)
sw $s5, 32($sp)
sw $s6, 36($sp)
sw $s7, 40($sp)
move $t3 $a1
move $t4 $a2
move $t5 $a3
lw $v1, 0($fp)
move $t0 $v1
lw $v1, 4($fp)
move $t1 $v1
lw $v1, 8($fp)
move $t2 $v1
move $a0 ,$t3
jal _print
move $a0 ,$t4
jal _print
move $a0 ,$t5
jal _print
move $a0 ,$t0
jal _print
move $a0 ,$t1
jal _print
move $a0 ,$t2
jal _print
li $t6 0
move $v0 $t6
lw $s0, 12($sp)
lw $s1, 16($sp)
lw $s2, 20($sp)
lw $s3, 24($sp)
lw $s4, 28($sp)
lw $s5, 32($sp)
lw $s6, 36($sp)
lw $s7, 40($sp)
lw $ra, -4($fp)
lw $fp, 160($sp)
addu $sp, $sp, 168
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
