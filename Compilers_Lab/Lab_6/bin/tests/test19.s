.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $t0 12
move $a0, $t0
jal _halloc
move $t1 $v0
move $t2 $t1
li $t3 12
move $a0, $t3
jal _halloc
move $t4 $v0
move $t5 $t4
move $t6 $t2
la $t7 __MYTRANSLATE__B22__go__
sw $t7, 4($t6)
move $t8 $t2
la $t9 __MYTRANSLATE__B22__getI1__
sw $t9, 8($t8)
move $s0 $t2
la $s1 __MYTRANSLATE__B22__getI1__
sw $s1, 0($s0)
li $s2 4
move $s3 $s2
MAINL0: 
move $s4 $s3
li $s5 12
slt $v0, $s4, $s5
move $s6 $v0
beqz $s6 MAINL1
move $s7 $t5
move $t0 $s3
addu $v0, $s7, $t0
move $t1 $v0
li $t3 0
sw $t3, 0($t1)
move $t4 $s3
li $t6 4
addu $v0, $t4, $t6
move $t7 $v0
move $s3 $t7
b MAINL0
MAINL1: 
move $t8 $t5
move $t9 $t2
sw $t9, 0($t8)
move $s0 $t5
move $s1 $s0
move $s2 $s1
move $s4 $s2
lw  $s5 0($s4)
move $s6 $s5
lw  $s7 4($s6)
move $t0 $s7
move $t1 $t0
move $t3 $s2
li $t4 12
move $a0, $t4
jal _halloc
move $t6 $v0
move $t7 $t6
li $s3 12
move $a0, $s3
jal _halloc
move $t2 $v0
move $t8 $t2
move $t9 $t7
la $t5 __MYTRANSLATE__B22__go__
sw $t5, 4($t9)
move $s0 $t7
la $s1 __MYTRANSLATE__B22__getI1__
sw $s1, 8($s0)
move $s4 $t7
la $s5 __MYTRANSLATE__B22__getI1__
sw $s5, 0($s4)
li $s6 4
move $s7 $s6
MAINL2: 
move $t0 $s7
li $s2 12
slt $v0, $t0, $s2
move $t4 $v0
beqz $t4 MAINL3
move $t6 $t8
move $s3 $s7
addu $v0, $t6, $s3
move $t2 $v0
li $t9 0
sw $t9, 0($t2)
move $t5 $s7
li $s0 4
addu $v0, $t5, $s0
move $s1 $v0
move $s7 $s1
b MAINL2
MAINL3: 
move $s4 $t8
move $s5 $t7
sw $s5, 0($s4)
move $s6 $t8
move $t0 $s6
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t3
move $a1 $t0
jalr $t1
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s2 $v0
move $a0 ,$s2
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	__MYTRANSLATE__A22__getI1__
__MYTRANSLATE__A22__getI1__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
move $t0 $t1
li $t2 123
sw $t2, 8($t0)
move $t3 $t1
lw  $t4 8($t3)
move $t5 $t4
move $t6 $t5
move $t7 $t6
move $v0 $t7
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__B22__go__
__MYTRANSLATE__B22__go__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a1
move $t0 $t1
move $t2 $t0
move $t3 $t2
lw  $t4 0($t3)
move $t5 $t4
lw  $t6 0($t5)
move $t7 $t6
move $t8 $t7
move $t9 $t2
li $s0 1
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t9
move $a1 $s0
jalr $t8
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s1 $v0
move $s2 $s1
move $v0 $s2
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	__MYTRANSLATE__B22__getI1__
__MYTRANSLATE__B22__getI1__:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
move $t2 $a1
move $t0 $t2
beqz $t0 __MYTRANSLATE__B22__getI1__L4
move $t3 $t1
li $t4 456
sw $t4, 8($t3)
b __MYTRANSLATE__B22__getI1__L5
__MYTRANSLATE__B22__getI1__L4: 
nop
move $t5 $t1
li $t6 789
sw $t6, 8($t5)
__MYTRANSLATE__B22__getI1__L5: 
nop
move $t7 $t1
lw  $t8 8($t7)
move $t9 $t8
move $s0 $t9
move $s1 $s0
move $v0 $s1
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
