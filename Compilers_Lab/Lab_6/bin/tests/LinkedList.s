.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $a0, 4
jal _halloc
move $t0 $v0
li $a0, 4
jal _halloc
move $t1 $v0
la $t2 LL_Start
sw $t2, 0($t0)
sw $t0, 0($t1)
move $t3 $t1
lw  $t4 0($t3)
lw  $t5 0($t4)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t3
jalr $t5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t6 $v0
move $a0 ,$t6
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	Element_Init
Element_Init:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0 $a0
move $t1 $a1
move $t2 $a2
move $t3 $a3
sw $t1, 4($t0)
sw $t2, 8($t0)
sw $t3, 12($t0)
li $t4 1
move $v0 $t4
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	Element_GetAge
Element_GetAge:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
lw  $t0 4($t1)
move $v0 $t0
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	Element_GetSalary
Element_GetSalary:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
lw  $t0 8($t1)
move $v0 $t0
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	Element_GetMarried
Element_GetMarried:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
lw  $t0 12($t1)
move $v0 $t0
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	Element_Equal
Element_Equal:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
move $t2 $a1
li $t0 1
move $t3 $t2
lw  $t4 0($t3)
lw  $t5 4($t4)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t3
jalr $t5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t6 $v0
move $t7 $t6
li $t8 1
move $t9 $t1
lw  $s0 0($t9)
lw  $s1 20($s0)
lw  $s2 4($t1)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t9
move $a1 $t7
move $a2 $s2
jalr $s1
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s3 $v0
subu $v0, $t8, $s3
move $s4 $v0
beqz $s4 Element_EqualL2
li $t0 0
b Element_EqualL3
Element_EqualL2: 
nop
move $s5 $t2
lw  $s6 0($s5)
lw  $s7 8($s6)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s5
jalr $s7
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t4 $v0
move $t3 $t4
li $t5 1
move $t6 $t1
lw  $s0 0($t6)
lw  $t9 20($s0)
lw  $t7 8($t1)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t6
move $a1 $t3
move $a2 $t7
jalr $t9
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s1 $v0
subu $v0, $t5, $s1
move $s2 $v0
beqz $s2 Element_EqualL4
li $t0 0
b Element_EqualL5
Element_EqualL4: 
nop
lw  $t8 12($t1)
beqz $t8 Element_EqualL6
li $s3 1
move $s4 $t2
lw  $s6 0($s4)
lw  $s5 12($s6)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s4
jalr $s5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s7 $v0
subu $v0, $s3, $s7
move $t4 $v0
beqz $t4 Element_EqualL8
li $t0 0
b Element_EqualL9
Element_EqualL8: 
nop
Element_EqualL9: 
nop
b Element_EqualL7
Element_EqualL6: 
nop
move $s0 $t2
lw  $t6 0($s0)
lw  $t3 12($t6)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s0
jalr $t3
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t9 $v0
beqz $t9 Element_EqualL10
li $t0 0
b Element_EqualL11
Element_EqualL10: 
nop
Element_EqualL11: 
nop
Element_EqualL7: 
nop
Element_EqualL5: 
nop
Element_EqualL3: 
nop
move $v0 $t0
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	Element_Compare
Element_Compare:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0 $a1
move $t1 $a2
li $t4 0
li $v1, 1
addu $v0, $t1, $v1
move $t2 $v0
slt $v0, $t0, $t1
move $t3 $v0
beqz $t3 Element_CompareL12
li $t4 0
b Element_CompareL13
Element_CompareL12: 
nop
li $t5 1
slt $v0, $t0, $t2
move $t6 $v0
subu $v0, $t5, $t6
move $t7 $v0
beqz $t7 Element_CompareL14
li $t4 0
b Element_CompareL15
Element_CompareL14: 
nop
li $t4 1
Element_CompareL15: 
nop
Element_CompareL13: 
nop
move $v0 $t4
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	List_Init
List_Init:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
li $t0 1
sw $t0, 12($t1)
li $t2 1
move $v0 $t2
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	List_InitNew
List_InitNew:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0 $a0
move $t1 $a1
move $t2 $a2
move $t3 $a3
sw $t3, 12($t0)
sw $t1, 4($t0)
sw $t2, 8($t0)
li $t4 1
move $v0 $t4
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	List_Insert
List_Insert:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
move $t2 $a1
move $t0 $t1
li $a0, 40
jal _halloc
move $t3 $v0
li $a0, 16
jal _halloc
move $t4 $v0
la $t5 List_Print
sw $t5, 36($t3)
la $t6 List_GetNext
sw $t6, 32($t3)
la $t7 List_GetElem
sw $t7, 28($t3)
la $t8 List_GetEnd
sw $t8, 24($t3)
la $t9 List_Search
sw $t9, 20($t3)
la $s0 List_Delete
sw $s0, 16($t3)
la $s1 List_SetNext
sw $s1, 12($t3)
la $s2 List_Insert
sw $s2, 8($t3)
la $s3 List_InitNew
sw $s3, 4($t3)
la $s4 List_Init
sw $s4, 0($t3)
li $s5 4
List_InsertL16: 
nop
li $s6 16
slt $v0, $s5, $s6
move $s7 $v0
beqz $s7 List_InsertL17
addu $v0, $t4, $s5
move $t1 $v0
li $t5 0
sw $t5, 0($t1)
li $v1, 4
addu $v0, $s5, $v1
move $s5 $v0
b List_InsertL16
List_InsertL17: 
nop
sw $t3, 0($t4)
move $t6 $t4
move $t7 $t6
lw  $t8 0($t7)
lw  $t9 4($t8)
li $s0 0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t7
move $a1 $t2
move $a2 $t0
move $a3 $s0
jalr $t9
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s1 $v0
move $v0 $t6
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	List_SetNext
List_SetNext:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0 $a0
move $t1 $a1
sw $t1, 8($t0)
li $t2 1
move $v0 $t2
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	List_Delete
List_Delete:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
move $t2 $a1
move $t0 $t1
li $t3 0
li $t4 0
li $v1, 1
subu $v0, $t4, $v1
move $t5 $v0
move $t6 $t1
move $t7 $t1
lw  $t8 12($t1)
move $t9 $t8
lw  $s0 4($t1)
move $s1 $s0
List_DeleteL18: 
nop
li $s2 0
li $s3 1
subu $v0, $s3, $t9
move $s4 $v0
beqz $s4 List_DeleteL20
li $s5 1
subu $v0, $s5, $t3
move $s6 $v0
beqz $s6 List_DeleteL20
li $s2 1
List_DeleteL20: 
nop
beqz $s2 List_DeleteL19
move $s7 $t2
lw  $t4 0($s7)
lw  $t8 16($t4)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s7
move $a1 $s1
jalr $t8
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t1 $v0
beqz $t1 List_DeleteL21
li $t3 1
li $s0 0
slt $v0, $t5, $s0
move $s3 $v0
beqz $s3 List_DeleteL23
move $s4 $t6
lw  $s5 0($s4)
lw  $s6 32($s5)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s4
jalr $s6
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s2 $v0
move $t0 $s2
b List_DeleteL24
List_DeleteL23: 
nop
li $t4 0
li $s7 555
subu $v0, $t4, $s7
move $t8 $v0
move $a0 ,$t8
jal _print
move $t1 $t7
lw  $s0 0($t1)
lw  $s3 12($s0)
move $s5 $t6
lw  $s4 0($s5)
lw  $s6 32($s4)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s5
jalr $s6
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s2 $v0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t1
move $a1 $s2
jalr $s3
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t4 $v0
li $s7 0
li $t8 555
subu $v0, $s7, $t8
move $s0 $v0
move $a0 ,$s0
jal _print
List_DeleteL24: 
nop
b List_DeleteL22
List_DeleteL21: 
nop
List_DeleteL22: 
nop
li $s4 1
subu $v0, $s4, $t3
move $s5 $v0
beqz $s5 List_DeleteL25
move $t7 $t6
move $s6 $t6
lw  $s3 0($s6)
lw  $t1 32($s3)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s6
jalr $t1
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s2 $v0
move $t6 $s2
move $t4 $t6
lw  $s7 0($t4)
lw  $t8 24($s7)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t4
jalr $t8
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s0 $v0
move $t9 $s0
move $s4 $t6
lw  $s5 0($s4)
lw  $s3 28($s5)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s4
jalr $s3
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s6 $v0
move $s1 $s6
li $t5 1
b List_DeleteL26
List_DeleteL25: 
nop
List_DeleteL26: 
nop
b List_DeleteL18
List_DeleteL19: 
nop
move $v0 $t0
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	List_Search
List_Search:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0 $a0
move $t1 $a1
li $t2 0
move $t3 $t0
lw  $t4 12($t0)
move $t5 $t4
lw  $t6 4($t0)
move $t7 $t6
List_SearchL27: 
nop
li $t8 1
subu $v0, $t8, $t5
move $t9 $v0
beqz $t9 List_SearchL28
move $s0 $t1
lw  $s1 0($s0)
lw  $s2 16($s1)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s0
move $a1 $t7
jalr $s2
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s3 $v0
beqz $s3 List_SearchL29
li $t2 1
b List_SearchL30
List_SearchL29: 
nop
List_SearchL30: 
nop
move $s4 $t3
lw  $s5 0($s4)
lw  $s6 32($s5)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s4
jalr $s6
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s7 $v0
move $t3 $s7
move $t4 $t3
lw  $t0 0($t4)
lw  $t6 24($t0)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t4
jalr $t6
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t8 $v0
move $t5 $t8
move $t9 $t3
lw  $s1 0($t9)
lw  $s0 28($s1)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t9
jalr $s0
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s2 $v0
move $t7 $s2
b List_SearchL27
List_SearchL28: 
nop
move $v0 $t2
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	List_GetEnd
List_GetEnd:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
lw  $t0 12($t1)
move $v0 $t0
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	List_GetElem
List_GetElem:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
lw  $t0 4($t1)
move $v0 $t0
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	List_GetNext
List_GetNext:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
lw  $t0 8($t1)
move $v0 $t0
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	List_Print
List_Print:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
move $t0 $t1
lw  $t2 12($t1)
move $t3 $t2
lw  $t4 4($t1)
move $t5 $t4
List_PrintL31: 
nop
li $t6 1
subu $v0, $t6, $t3
move $t7 $v0
beqz $t7 List_PrintL32
move $t8 $t5
lw  $t9 0($t8)
lw  $s0 4($t9)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t8
jalr $s0
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s1 $v0
move $a0 ,$s1
jal _print
move $s2 $t0
lw  $s3 0($s2)
lw  $s4 32($s3)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s2
jalr $s4
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s5 $v0
move $t0 $s5
move $s6 $t0
lw  $s7 0($s6)
lw  $t2 24($s7)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s6
jalr $t2
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t1 $v0
move $t3 $t1
move $t4 $t0
lw  $t6 0($t4)
lw  $t7 28($t6)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t4
jalr $t7
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t9 $v0
move $t5 $t9
b List_PrintL31
List_PrintL32: 
nop
li $t8 1
move $v0 $t8
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	LL_Start
LL_Start:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
li $a0, 40
jal _halloc
move $t0 $v0
li $a0, 16
jal _halloc
move $t1 $v0
la $t2 List_Print
sw $t2, 36($t0)
la $t3 List_GetNext
sw $t3, 32($t0)
la $t4 List_GetElem
sw $t4, 28($t0)
la $t5 List_GetEnd
sw $t5, 24($t0)
la $t6 List_Search
sw $t6, 20($t0)
la $t7 List_Delete
sw $t7, 16($t0)
la $t8 List_SetNext
sw $t8, 12($t0)
la $t9 List_Insert
sw $t9, 8($t0)
la $s0 List_InitNew
sw $s0, 4($t0)
la $s1 List_Init
sw $s1, 0($t0)
li $s2 4
LL_StartL33: 
nop
li $s3 16
slt $v0, $s2, $s3
move $s4 $v0
beqz $s4 LL_StartL34
addu $v0, $t1, $s2
move $s5 $v0
li $s6 0
sw $s6, 0($s5)
li $v1, 4
addu $v0, $s2, $v1
move $s2 $v0
b LL_StartL33
LL_StartL34: 
nop
sw $t0, 0($t1)
move $s7 $t1
move $t2 $s7
lw  $t3 0($t2)
lw  $t4 0($t3)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t2
jalr $t4
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t5 $v0
move $t6 $s7
move $t7 $t6
lw  $t8 0($t7)
lw  $t9 0($t8)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t7
jalr $t9
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s0 $v0
move $s1 $t6
lw  $s3 0($s1)
lw  $s4 36($s3)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s1
jalr $s4
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s5 $v0
li $a0, 24
jal _halloc
move $s6 $v0
li $a0, 16
jal _halloc
move $s2 $v0
la $t0 Element_Compare
sw $t0, 20($s6)
la $t1 Element_Equal
sw $t1, 16($s6)
la $t3 Element_GetMarried
sw $t3, 12($s6)
la $t2 Element_GetSalary
sw $t2, 8($s6)
la $t4 Element_GetAge
sw $t4, 4($s6)
la $t5 Element_Init
sw $t5, 0($s6)
li $s7 4
LL_StartL35: 
nop
li $t8 16
slt $v0, $s7, $t8
move $t7 $v0
beqz $t7 LL_StartL36
addu $v0, $s2, $s7
move $t9 $v0
li $s0 0
sw $s0, 0($t9)
li $v1, 4
addu $v0, $s7, $v1
move $s7 $v0
b LL_StartL35
LL_StartL36: 
nop
sw $s6, 0($s2)
move $s3 $s2
move $s1 $s3
lw  $s4 0($s1)
lw  $s5 0($s4)
li $t0 25
li $t1 37000
li $t3 0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s1
move $a1 $t0
move $a2 $t1
move $a3 $t3
jalr $s5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t2 $v0
move $t4 $t6
lw  $t5 0($t4)
lw  $t8 8($t5)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t4
move $a1 $s3
jalr $t8
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t7 $v0
move $t6 $t7
move $t9 $t6
lw  $s0 0($t9)
lw  $s7 36($s0)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t9
jalr $s7
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s6 $v0
li $s2 10000000
move $a0 ,$s2
jal _print
li $a0, 24
jal _halloc
move $s4 $v0
li $a0, 16
jal _halloc
move $s1 $v0
la $s5 Element_Compare
sw $s5, 20($s4)
la $t0 Element_Equal
sw $t0, 16($s4)
la $t1 Element_GetMarried
sw $t1, 12($s4)
la $t3 Element_GetSalary
sw $t3, 8($s4)
la $t2 Element_GetAge
sw $t2, 4($s4)
la $t5 Element_Init
sw $t5, 0($s4)
li $t4 4
LL_StartL37: 
nop
li $t8 16
slt $v0, $t4, $t8
move $t7 $v0
beqz $t7 LL_StartL38
addu $v0, $s1, $t4
move $s0 $v0
li $t9 0
sw $t9, 0($s0)
li $v1, 4
addu $v0, $t4, $v1
move $t4 $v0
b LL_StartL37
LL_StartL38: 
nop
sw $s4, 0($s1)
move $s3 $s1
move $s7 $s3
lw  $s6 0($s7)
lw  $s2 0($s6)
li $s5 39
li $t0 42000
li $t1 1
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s7
move $a1 $s5
move $a2 $t0
move $a3 $t1
jalr $s2
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t3 $v0
move $t2 $s3
move $t5 $t6
lw  $t8 0($t5)
lw  $t7 8($t8)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t5
move $a1 $s3
jalr $t7
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s0 $v0
move $t6 $s0
move $t9 $t6
lw  $t4 0($t9)
lw  $s4 36($t4)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t9
jalr $s4
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s1 $v0
li $s6 10000000
move $a0 ,$s6
jal _print
li $a0, 24
jal _halloc
move $s7 $v0
li $a0, 16
jal _halloc
move $s2 $v0
la $s5 Element_Compare
sw $s5, 20($s7)
la $t0 Element_Equal
sw $t0, 16($s7)
la $t1 Element_GetMarried
sw $t1, 12($s7)
la $t3 Element_GetSalary
sw $t3, 8($s7)
la $t8 Element_GetAge
sw $t8, 4($s7)
la $t5 Element_Init
sw $t5, 0($s7)
li $t7 4
LL_StartL39: 
nop
li $s0 16
slt $v0, $t7, $s0
move $t4 $v0
beqz $t4 LL_StartL40
addu $v0, $s2, $t7
move $t9 $v0
li $s4 0
sw $s4, 0($t9)
li $v1, 4
addu $v0, $t7, $v1
move $t7 $v0
b LL_StartL39
LL_StartL40: 
nop
sw $s7, 0($s2)
move $s3 $s2
move $s1 $s3
lw  $s6 0($s1)
lw  $s5 0($s6)
li $t0 22
li $t1 34000
li $t3 0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s1
move $a1 $t0
move $a2 $t1
move $a3 $t3
jalr $s5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t8 $v0
move $t5 $t6
lw  $s0 0($t5)
lw  $t4 8($s0)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t5
move $a1 $s3
jalr $t4
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t9 $v0
move $t6 $t9
move $s4 $t6
lw  $t7 0($s4)
lw  $s7 36($t7)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s4
jalr $s7
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s2 $v0
li $a0, 24
jal _halloc
move $s6 $v0
li $a0, 16
jal _halloc
move $s1 $v0
la $s5 Element_Compare
sw $s5, 20($s6)
la $t0 Element_Equal
sw $t0, 16($s6)
la $t1 Element_GetMarried
sw $t1, 12($s6)
la $t3 Element_GetSalary
sw $t3, 8($s6)
la $t8 Element_GetAge
sw $t8, 4($s6)
la $s0 Element_Init
sw $s0, 0($s6)
li $t5 4
LL_StartL41: 
nop
li $t4 16
slt $v0, $t5, $t4
move $t9 $v0
beqz $t9 LL_StartL42
addu $v0, $s1, $t5
move $t7 $v0
li $s4 0
sw $s4, 0($t7)
li $v1, 4
addu $v0, $t5, $v1
move $t5 $v0
b LL_StartL41
LL_StartL42: 
nop
sw $s6, 0($s1)
move $s7 $s1
move $s2 $s7
lw  $s5 0($s2)
lw  $t0 0($s5)
li $t1 27
li $t3 34000
li $t8 0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s2
move $a1 $t1
move $a2 $t3
move $a3 $t8
jalr $t0
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s0 $v0
move $t4 $t6
lw  $t9 0($t4)
lw  $t7 20($t9)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t4
move $a1 $t2
jalr $t7
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s4 $v0
move $a0 ,$s4
jal _print
move $t5 $t6
lw  $s6 0($t5)
lw  $s1 20($s6)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t5
move $a1 $s7
jalr $s1
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s5 $v0
move $a0 ,$s5
jal _print
li $s2 10000000
move $a0 ,$s2
jal _print
li $a0, 24
jal _halloc
move $t0 $v0
li $a0, 16
jal _halloc
move $t1 $v0
la $t3 Element_Compare
sw $t3, 20($t0)
la $t8 Element_Equal
sw $t8, 16($t0)
la $s0 Element_GetMarried
sw $s0, 12($t0)
la $t9 Element_GetSalary
sw $t9, 8($t0)
la $t4 Element_GetAge
sw $t4, 4($t0)
la $t7 Element_Init
sw $t7, 0($t0)
li $s4 4
LL_StartL43: 
nop
li $s6 16
slt $v0, $s4, $s6
move $t5 $v0
beqz $t5 LL_StartL44
addu $v0, $t1, $s4
move $s7 $v0
li $s1 0
sw $s1, 0($s7)
li $v1, 4
addu $v0, $s4, $v1
move $s4 $v0
b LL_StartL43
LL_StartL44: 
nop
sw $t0, 0($t1)
move $s3 $t1
move $s5 $s3
lw  $s2 0($s5)
lw  $t3 0($s2)
li $t8 28
li $s0 35000
li $t9 0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s5
move $a1 $t8
move $a2 $s0
move $a3 $t9
jalr $t3
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t4 $v0
move $t7 $t6
lw  $s6 0($t7)
lw  $t5 8($s6)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t7
move $a1 $s3
jalr $t5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s7 $v0
move $t6 $s7
move $s1 $t6
lw  $s4 0($s1)
lw  $t0 36($s4)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s1
jalr $t0
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t1 $v0
li $s2 2220000
move $a0 ,$s2
jal _print
move $s5 $t6
lw  $t3 0($s5)
lw  $t8 16($t3)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s5
move $a1 $t2
jalr $t8
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s0 $v0
move $t6 $s0
move $t9 $t6
lw  $t4 0($t9)
lw  $s6 36($t4)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t9
jalr $s6
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t7 $v0
li $t5 33300000
move $a0 ,$t5
jal _print
move $s7 $t6
lw  $s4 0($s7)
lw  $s1 16($s4)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s7
move $a1 $s3
jalr $s1
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t0 $v0
move $t6 $t0
move $t1 $t6
lw  $s2 0($t1)
lw  $t3 36($s2)
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t1
jalr $t3
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s5 $v0
li $t2 44440000
move $a0 ,$t2
jal _print
li $t8 0
move $v0 $t8
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
