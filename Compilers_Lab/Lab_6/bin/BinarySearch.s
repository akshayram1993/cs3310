.text
.globl	main
main:
move $fp, $sp
subu $sp, $sp, 152
sw $ra, -4($fp)
li $t0, 24
move $a0, $t0
jal _halloc
move $t1 $v0
move $t2 $t1
li $t3 12
move $a0, $t3
jal _halloc
move $t4 $v0
move $t0 $t4
move $t5 $t2
la $t6 BS_Search
sw $t6, 4($t5)
move $t7 $t2
la $t8 BS_Start
sw $t8, 0($t7)
move $t9 $t2
la $s0, BS_Init
sw $s0, 20($t9)
move $s1 $t2
la $s2 BS_Compare
sw $s2, 12($s1)
move $s3 $t2
la $s4 BS_Print
sw $s4, 16($s3)
move $s5 $t2
la $s6 BS_Div
sw $s6, 8($s5)
li $s7 4
move $t1 $s7
MAINL0: 
move $t3 $t1
li $t4 12
slt $v0, $t3, $t4
move $t5 $v0
beqz $t5 MAINL1
move $t6 $t0
move $t7 $t1
addu $v0, $t6, $t7
move $t8 $v0
li $t9 0
sw $t9, 0($t8)
move $s0 $t1
li $s1 4
addu $v0, $s0, $s1
move $s2 $v0
move $t1 $s2
b MAINL0
MAINL1: 
move $s3 $t0
move $s4 $t2
sw $s4, 0($s3)
move $s5 $t0
move $s6 $s5
move $s7 $s6
move $t3 $s7
lw  $t4 0($t3)
move $t5 $t4
lw  $t6 0($t5)
move $t7 $t6
move $t8 $t7
move $t9 $s7
li $s0 20
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t9
move $a1 $s0
jalr $t8
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s1 $v0
move $a0 ,$s1
jal _print
lw $ra, -4($fp)
addu $sp, $sp, 152
jr $ra
.text
.globl	BS_Start
BS_Start:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
move $t2 $a1
move $t0 $t1
move $t3 $t0
move $t4 $t3
lw  $t5 0($t4)
move $t6 $t5
lw  $t7 20($t6)
move $t8 $t7
move $t9 $t8
move $s0 $t3
move $s1 $t2
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s0
move $a1 $s1
jalr $t9
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s2 $v0
move $s3 $t1
move $s4 $s3
move $s5 $s4
lw  $s6 0($s5)
move $s7 $s6
lw  $t0 16($s7)
move $t4 $t0
move $t5 $t4
move $t6 $s4
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t6
jalr $t5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t7 $v0
move $t8 $t1
move $t3 $t8
move $t2 $t3
lw  $t9 0($t2)
move $s0 $t9
lw  $s1 4($s0)
move $s2 $s1
move $s3 $s2
move $s5 $t3
li $s6 8
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s5
move $a1 $s6
jalr $s3
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s7 $v0
beqz $s7 BS_StartL1
li $t0 1
move $a0 ,$t0
jal _print
b BS_StartL2
BS_StartL1: 
li $t4 0
move $a0 ,$t4
jal _print
BS_StartL2: 
nop
move $s4 $t1
move $t5 $s4
move $t6 $t5
lw  $t7 0($t6)
move $t8 $t7
lw  $t2 4($t8)
move $t9 $t2
move $s0 $t9
move $s1 $t5
li $s2 19
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s1
move $a1 $s2
jalr $s0
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t3 $v0
beqz $t3 BS_StartL3
li $s3 1
move $a0 ,$s3
jal _print
b BS_StartL4
BS_StartL3: 
li $s5 0
move $a0 ,$s5
jal _print
BS_StartL4: 
nop
move $s6 $t1
move $s7 $s6
move $t0 $s7
lw  $t4 0($t0)
move $s4 $t4
lw  $t6 4($s4)
move $t7 $t6
move $t8 $t7
move $t2 $s7
li $t9 20
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t2
move $a1 $t9
jalr $t8
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t5 $v0
beqz $t5 BS_StartL5
li $s0 1
move $a0 ,$s0
jal _print
b BS_StartL6
BS_StartL5: 
li $s1 0
move $a0 ,$s1
jal _print
BS_StartL6: 
nop
move $s2 $t1
move $t3 $s2
move $s3 $t3
lw  $s5 0($s3)
move $s6 $s5
lw  $t0 4($s6)
move $t4 $t0
move $s4 $t4
move $t6 $t3
li $t7 21
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t6
move $a1 $t7
jalr $s4
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s7 $v0
beqz $s7 BS_StartL7
li $t8 1
move $a0 ,$t8
jal _print
b BS_StartL8
BS_StartL7: 
li $t2 0
move $a0 ,$t2
jal _print
BS_StartL8: 
nop
move $t9 $t1
move $t5 $t9
move $s0 $t5
lw  $s1 0($s0)
move $s2 $s1
lw  $s3 4($s2)
move $s5 $s3
move $s6 $s5
move $t0 $t5
li $t4 37
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t0
move $a1 $t4
jalr $s6
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t3 $v0
beqz $t3 BS_StartL9
li $s4 1
move $a0 ,$s4
jal _print
b BS_StartL10
BS_StartL9: 
li $t6 0
move $a0 ,$t6
jal _print
BS_StartL10: 
nop
move $t7 $t1
move $s7 $t7
move $t8 $s7
lw  $t2 0($t8)
move $t9 $t2
lw  $s0 4($t9)
move $s1 $s0
move $s2 $s1
move $s3 $s7
li $s5 38
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s3
move $a1 $s5
jalr $s2
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t5 $v0
beqz $t5 BS_StartL11
li $s6 1
move $a0 ,$s6
jal _print
b BS_StartL12
BS_StartL11: 
li $t0 0
move $a0 ,$t0
jal _print
BS_StartL12: 
nop
move $t4 $t1
move $t3 $t4
move $s4 $t3
lw  $t6 0($s4)
move $t7 $t6
lw  $t8 4($t7)
move $t2 $t8
move $t9 $t2
move $s0 $t3
li $s1 39
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s0
move $a1 $s1
jalr $t9
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s7 $v0
beqz $s7 BS_StartL13
li $s2 1
move $a0 ,$s2
jal _print
b BS_StartL14
BS_StartL13: 
li $s3 0
move $a0 ,$s3
jal _print
BS_StartL14: 
nop
move $s5 $t1
move $t5 $s5
move $s6 $t5
lw  $t0 0($s6)
move $t4 $t0
lw  $s4 4($t4)
move $t6 $s4
move $t7 $t6
move $t8 $t5
li $t2 50
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t8
move $a1 $t2
jalr $t7
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t3 $v0
beqz $t3 BS_StartL15
li $t9 1
move $a0 ,$t9
jal _print
b BS_StartL16
BS_StartL15: 
li $s0 0
move $a0 ,$s0
jal _print
BS_StartL16: 
nop
li $s1 999
move $s7 $s1
move $v0 $s7
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	BS_Search
BS_Search:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
move $t2 $a1
li $t0 0
move $t3 $t0
li $t4 0
move $s4 $t4
move $t5 $t1
lw  $t6 4($t5)
move $t7 $t6
move $t8 $t7
move $t9 $t8
move $s0 $t9
lw  $s1 0($s0)
move $s2 $s1
move $s3 $s2
move $s4 $s3
move $s5 $s4
li $s6 1
subu $v0, $s5, $s6
move $s7 $v0
move $t0 $s7
move $t4 $t0
move $t5 $t4
move $s4 $t5
li $t6 0
move $t7 $t6
li $t8 1
move $t9 $t8
BS_SearchL18: 
move $s0 $t9
beqz $s0 BS_SearchL17
move $s1 $t7
move $s2 $s4
addu $v0, $s1, $s2
move $s3 $v0
move $s5 $s3
move $s6 $s5
move $s7 $s6
move $t0 $s7
move $t4 $t1
move $t5 $t4
move $t6 $t5
lw  $t8 0($t6)
move $s0 $t8
lw  $s1 8($s0)
move $s2 $s1
move $s3 $s2
move $s5 $t5
move $s6 $t0
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s5
move $a1 $s6
jalr $s3
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s7 $v0
move $t0 $s7
move $t4 $t1
lw  $t6 4($t4)
move $t8 $t6
move $s0 $t8
move $s1 $t0
li $s2 4
mult $s1 $s2
mflo $v0
move $t5 $v0
move $s3 $t5
move $s5 $s3
move $s6 $s5
li $s7 4
addu $v0, $s6, $s7
move $t4 $v0
addu $v0, $s0, $t4
move $t6 $v0
lw  $t8 0($t6)
move $s1 $t8
move $s2 $s1
move $t3 $s2
move $t5 $t2
move $s3 $t3
slt $v0, $t5, $s3
move $s5 $v0
move $s6 $s5
move $s7 $s6
move $s0 $s7
beqz $s0 BS_SearchL19
move $t4 $t0
li $t6 1
subu $v0, $t4, $t6
move $t8 $v0
move $s1 $t8
move $s2 $s1
move $t5 $s2
move $s4 $t5
b BS_SearchL20
BS_SearchL19: 
move $s3 $t0
li $s5 1
addu $v0, $s3, $s5
move $s6 $v0
move $s7 $s6
move $s0 $s7
move $t4 $s0
move $t7 $t4
BS_SearchL20: 
nop
move $t6 $t1
move $t8 $t6
move $s1 $t8
lw  $s2 0($s1)
move $t5 $s2
lw  $t0 12($t5)
move $s3 $t0
move $s5 $s3
move $s6 $t8
move $s7 $t3
move $s0 $t2
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $s6
move $a1 $s7
move $a2 $s0
jalr $s5
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $t4 $v0
beqz $t4 BS_SearchL21
li $t6 0
move $t9 $t6
b BS_SearchL22
BS_SearchL21: 
li $s1 1
move $t9 $s1
BS_SearchL22: 
nop
move $s2 $s4
move $t5 $t7
slt $v0, $s2, $t5
move $t0 $v0
move $s3 $t0
move $t8 $s3
move $s5 $t8
beqz $s5 BS_SearchL23
li $s6 0
move $t9 $s6
b BS_SearchL24
BS_SearchL23: 
li $s7 0
BS_SearchL24: 
nop
b BS_SearchL18
BS_SearchL17: 
nop
move $s0 $t1
move $t4 $s0
move $t6 $t4
lw  $s1 0($t6)
move $s2 $s1
lw  $t5 12($s2)
move $t0 $t5
move $s3 $t0
move $t8 $t4
move $s5 $t3
move $s6 $t2
sw $t0, 32($sp)
sw $t1, 36($sp)
sw $t2, 40($sp)
sw $t3, 44($sp)
sw $t4, 48($sp)
sw $t5, 52($sp)
sw $t6, 56($sp)
sw $t7, 60($sp)
sw $t8, 64($sp)
sw $t9, 68($sp)
move $a0 $t8
move $a1 $s5
move $a2 $s6
jalr $s3
lw $t0, 32($sp)
lw $t1, 36($sp)
lw $t2, 40($sp)
lw $t3, 44($sp)
lw $t4, 48($sp)
lw $t5, 52($sp)
lw $t6, 56($sp)
lw $t7, 60($sp)
lw $t8, 64($sp)
lw $t9, 68($sp)
move $s7 $v0
beqz $s7 BS_SearchL25
li $t7 1
move $s4 $t7
b BS_SearchL26
BS_SearchL25: 
li $t9 0
move $s4 $t9
BS_SearchL26: 
nop
move $t1 $s4
move $s0 $t1
move $v0 $s0
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	BS_Div
BS_Div:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a1
li $t0 0
move $t2 $t0
li $t3 0
move $t4 $t3
move $t5 $t1
li $t6 1
subu $v0, $t5, $t6
move $t7 $v0
move $t8 $t7
move $t9 $t8
move $s0 $t9
move $s1 $s0
BS_DivL28: 
move $s2 $t4
move $s3 $s1
slt $v0, $s2, $s3
move $s4 $v0
move $s5 $s4
move $s6 $s5
move $s7 $s6
beqz $s7 BS_DivL27
move $t0 $t2
li $t3 1
addu $v0, $t0, $t3
move $t1 $v0
move $t5 $t1
move $t6 $t5
move $t7 $t6
move $t2 $t7
move $t8 $t4
li $t9 2
addu $v0, $t8, $t9
move $s0 $v0
move $s2 $s0
move $s3 $s2
move $s4 $s3
move $t4 $s4
b BS_DivL28
BS_DivL27: 
nop
move $s5 $t2
move $s6 $s5
move $v0 $s6
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	BS_Compare
BS_Compare:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t0 $a1
move $t1 $a2
li $t2 0
move $s7 $t2
move $t3 $t1
li $t4 1
addu $v0, $t3, $t4
move $t5 $v0
move $t6 $t5
move $t7 $t6
move $t8 $t7
move $t9 $t8
move $s0 $t0
move $s1 $t1
slt $v0, $s0, $s1
move $s2 $v0
move $s3 $s2
move $s4 $s3
move $s5 $s4
beqz $s5 BS_CompareL29
li $s6 0
move $s7 $s6
b BS_CompareL30
BS_CompareL29: 
li $t2 1
move $t3 $t0
move $t4 $t9
slt $v0, $t3, $t4
move $t5 $v0
move $t6 $t5
move $t7 $t6
move $t8 $t7
subu $v0, $t2, $t8
move $t1 $v0
move $s0 $t1
move $s1 $s0
move $s2 $s1
beqz $s2 BS_CompareL31
li $s3 0
move $s7 $s3
b BS_CompareL32
BS_CompareL31: 
li $s4 1
move $s7 $s4
BS_CompareL32: 
nop
BS_CompareL30: 
nop
move $s5 $s7
move $s6 $s5
move $v0 $s6
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	BS_Print
BS_Print:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
li $t0 1
move $t2 $t0
BS_PrintL34: 
move $t3 $t2
move $t4 $t1
lw  $t5 8($t4)
move $t6 $t5
move $t7 $t6
slt $v0, $t3, $t7
move $t8 $v0
move $t9 $t8
move $s0 $t9
move $s1 $s0
beqz $s1 BS_PrintL33
move $s2 $t1
lw  $s3 4($s2)
move $s4 $s3
move $s5 $s4
move $s6 $t2
li $s7 4
mult $s6 $s7
mflo $v0
move $t0 $v0
move $t4 $t0
move $t5 $t4
move $t6 $t5
li $t3 4
addu $v0, $t6, $t3
move $t7 $v0
addu $v0, $s5, $t7
move $t8 $v0
lw  $t9 0($t8)
move $s0 $t9
move $s1 $s0
move $a0 ,$s1
jal _print
move $s2 $t2
li $s3 1
addu $v0, $s2, $s3
move $s4 $v0
move $s6 $s4
move $s7 $s6
move $t0 $s7
move $t2 $t0
b BS_PrintL34
BS_PrintL33: 
nop
li $t4 99999
move $a0 ,$t4
jal _print
li $t5 0
move $t6 $t5
move $v0 $t6
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
.globl	BS_Init
BS_Init:
sw $fp, -8($sp)
move $fp, $sp
subu $sp, $sp, 156
sw $ra, -4($fp)
sw $s0, 0($sp)
sw $s1, 4($sp)
sw $s2, 8($sp)
sw $s3, 12($sp)
sw $s4, 16($sp)
sw $s5, 20($sp)
sw $s6, 24($sp)
sw $s7, 28($sp)
move $t1 $a0
move $t2 $a1
move $t0 $t1
move $t3 $t2
sw $t3, 8($t0)
move $t4 $t1
move $t5 $t2
li $t6 1
addu $v0, $t5, $t6
move $t7 $v0
li $t8 4
mult $t7 $t8
mflo $v0
move $t9 $v0
move $a0, $t9
jal _halloc
move $s0 $v0
move $s1 $s0
li $s2 4
move $s3 $s2
BS_InitL35: 
move $s4 $s3
move $s5 $t2
li $s6 1
addu $v0, $s5, $s6
move $s7 $v0
li $t0 4
mult $s7 $t0
mflo $v0
move $t3 $v0
slt $v0, $s4, $t3
move $t5 $v0
beqz $t5 BS_InitL36
move $t6 $s1
move $t7 $s3
addu $v0, $t6, $t7
move $t8 $v0
li $t9 0
sw $t9, 0($t8)
move $s0 $s3
li $s2 4
addu $v0, $s0, $s2
move $s5 $v0
move $s3 $s5
b BS_InitL35
BS_InitL36: 
move $s6 $s1
move $s7 $t2
sw $s7, 0($s6)
move $t0 $s1
move $s4 $t0
sw $s4, 4($t4)
li $t3 1
move $t5 $t3
move $t6 $t1
lw  $t7 8($t6)
move $t8 $t7
move $t9 $t8
li $s0 1
addu $v0, $t9, $s0
move $s2 $v0
move $s5 $s2
move $s3 $s5
move $t2 $s3
move $s6 $t2
BS_InitL38: 
move $s7 $t5
move $s1 $t1
lw  $t0 8($s1)
move $t4 $t0
move $s4 $t4
slt $v0, $s7, $s4
move $t3 $v0
move $t6 $t3
move $t7 $t6
move $t8 $t7
beqz $t8 BS_InitL37
li $t9 2
move $s0 $t5
mult $t9 $s0
mflo $v0
move $s2 $v0
move $s5 $s2
move $s3 $s5
move $t2 $s3
move $s1 $t2
move $t0 $s6
li $t4 3
subu $v0, $t0, $t4
move $s7 $v0
move $s4 $s7
move $t3 $s4
move $t6 $t3
move $t7 $t6
move $t8 $t1
lw  $t9 4($t8)
move $s0 $t9
move $s2 $s0
move $s5 $s2
move $s3 $s5
li $t2 4
move $t0 $t5
mult $t2 $t0
mflo $v0
move $t4 $v0
move $s7 $t4
move $s4 $s7
move $t3 $s4
addu $v0, $s3, $t3
move $t6 $v0
move $t8 $s1
move $t9 $t7
addu $v0, $t8, $t9
move $s0 $v0
move $s2 $s0
move $s5 $s2
move $t2 $s5
sw $t2, 4($t6)
move $t0 $t5
li $t4 1
addu $v0, $t0, $t4
move $s7 $v0
move $s4 $s7
move $s3 $s4
move $t3 $s3
move $t5 $t3
move $s1 $s6
li $t7 1
subu $v0, $s1, $t7
move $t8 $v0
move $t9 $t8
move $s0 $t9
move $s2 $s0
move $s6 $s2
b BS_InitL38
BS_InitL37: 
nop
li $s5 0
move $t6 $s5
move $v0 $t6
lw $s0, 0($sp)
lw $s1, 4($sp)
lw $s2, 8($sp)
lw $s3, 12($sp)
lw $s4, 16($sp)
lw $s5, 20($sp)
lw $s6, 24($sp)
lw $s7, 28($sp)
lw $ra, -4($fp)
lw $fp, 148($sp)
addu $sp, $sp, 156
jr $ra
.text
	.globl _halloc
_halloc:
	li $v0, 9
	syscall
	jr $ra
	.text
	.globl _print
_print:
	li $v0, 1
	syscall
	la $a0, newl
	li $v0, 4
	syscall
	jr $ra
	.data
	.align   0
newl:	.asciiz "\n"
	 .data
	.align   0
str_er:	  .asciiz " ERROR: abnormal termination\n" 
