declare -f FILES
declare -f i

ant build

FILES=tests/*.miniRA
echo hello
for i in $FILES
do
	java P6 <$i >tests/$(basename ${i::-7}).s
	spim -file tests/$(basename ${i::-7}).s | awk 'NR > 5'>out.text
	echo $(basename ${i::-7})
	DIFF= `diff -B -w -i out.text tests/$(basename ${i::-7}).out`
	if [ "$DIFF" == "" ]
	then
		echo Pass
	else
		echo Fail
	fi
done

ant clean