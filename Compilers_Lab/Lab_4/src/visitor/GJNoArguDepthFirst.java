//
// Generated by JTB 1.3.2
//

package visitor;
import syntaxtree.*;
import java.util.*;

/**
 * Provides default methods which visit each node in the tree in depth-first
 * order.  Your visitors may extend this class.
 */
public class GJNoArguDepthFirst<R> implements GJNoArguVisitor<R> {
   
   public int globalTempCount = 1000;
   public boolean toPrintLabel = false;
   public boolean toPrintInteger = false;
   public boolean toPrintTemp = false;
   public boolean isProcedure = false;
   public R visit(NodeList n) {
      R _ret=null;
      int _count=0;
      for ( Enumeration<Node> e = n.elements(); e.hasMoreElements(); ) {
         e.nextElement().accept(this);
         _count++;
      }
      return _ret;
   }

   public R visit(NodeListOptional n) {
      if ( n.present() ) {
         R _ret=null;
         int _count=0;
         for ( Enumeration<Node> e = n.elements(); e.hasMoreElements(); ) {
            e.nextElement().accept(this);
            _count++;
         }
         return _ret;
      }
      else
         return null;
   }

   public R visit(NodeOptional n) {
      if ( n.present() ) {
    	   toPrintLabel = true;
    	   String id = (String)n.node.accept(this);
    	   toPrintLabel = false;
    	   System.out.println(id);
    	   return null;
      }
         
      else
         return null;
   }

   public R visit(NodeSequence n) {
      R _ret=null;
      int _count=0;
      for ( Enumeration<Node> e = n.elements(); e.hasMoreElements(); ) {
         e.nextElement().accept(this);
         _count++;
      }
      return _ret;
   }

   public R visit(NodeToken n) { return null; }

   //
   // User-generated visitor methods below
   //

   /**
    * f0 -> "MAIN"
    * f1 -> StmtList()
    * f2 -> "END"
    * f3 -> ( Procedure() )*
    * f4 -> <EOF>
    */
   public R visit(Goal n) {
      R _ret=null;
      System.out.println("MAIN");
      n.f0.accept(this);
      n.f1.accept(this);
      System.out.println("END");
      n.f2.accept(this);
      n.f3.accept(this);
      n.f4.accept(this);
      return _ret;
   }

   /**
    * f0 -> ( ( Label() )? Stmt() )*
    */
   public R visit(StmtList n) {
      R _ret=null;
      n.f0.accept(this);
      return _ret;
   }

   /**
    * f0 -> Label()
    * f1 -> "["
    * f2 -> IntegerLiteral()
    * f3 -> "]"
    * f4 -> StmtExp()
    */
   public R visit(Procedure n) {
      R _ret=null;
      toPrintLabel = true;
      String label = (String)n.f0.accept(this);
      toPrintLabel = false;
      n.f1.accept(this);
      toPrintInteger = true;
      String integer = (String)n.f2.accept(this);
      toPrintInteger = false;
      n.f3.accept(this);
      System.out.println(label+" [ "+integer+" ]");
      isProcedure = true;
      n.f4.accept(this);
      isProcedure = false;
      return _ret;
   }

   /**
    * f0 -> NoOpStmt()
    *       | ErrorStmt()
    *       | CJumpStmt()
    *       | JumpStmt()
    *       | HStoreStmt()
    *       | HLoadStmt()
    *       | MoveStmt()
    *       | PrintStmt()
    */
   public R visit(Stmt n) {
      R _ret=null;
      
      return n.f0.accept(this);
   }

   /**
    * f0 -> "NOOP"
    */
   public R visit(NoOpStmt n) {
      R _ret=null;
      n.f0.accept(this);
      System.out.println("NOOP");
      return _ret;
   }

   /**
    * f0 -> "ERROR"
    */
   public R visit(ErrorStmt n) {
      R _ret=null;
      n.f0.accept(this);
      System.out.println("ERROR");
      return _ret;
   }

   /**
    * f0 -> "CJUMP"
    * f1 -> Exp()
    * f2 -> Label()
    */
   public R visit(CJumpStmt n) {
      R _ret=null;
      n.f0.accept(this);
      String Temp = (String)n.f1.accept(this);
      toPrintLabel = true;
      String label = (String)n.f2.accept(this);
      toPrintLabel = false;
      System.out.println("CJUMP TEMP "+Temp+" "+label);
      return _ret;
   }

   /**
    * f0 -> "JUMP"
    * f1 -> Label()
    */
   public R visit(JumpStmt n) {
      R _ret=null;
      n.f0.accept(this);
      toPrintLabel = true;
      String label = (String)n.f1.accept(this);
      toPrintLabel = false;
      System.out.println("JUMP "+label);
      return _ret;
   }

   /**
    * f0 -> "HSTORE"
    * f1 -> Exp()
    * f2 -> IntegerLiteral()
    * f3 -> Exp()
    */
   public R visit(HStoreStmt n) {
      R _ret=null;
      n.f0.accept(this);
      String Temp1 = (String)n.f1.accept(this);
      toPrintInteger = true;
      String intLit = (String)n.f2.accept(this);
      toPrintInteger = false;
      String Temp2 = (String)n.f3.accept(this);
      System.out.println("HSTORE TEMP "+Temp1+" "+intLit+" TEMP "+Temp2);
      return _ret;
   }

   /**
    * f0 -> "HLOAD"
    * f1 -> Temp()
    * f2 -> Exp()
    * f3 -> IntegerLiteral()
    */
   public R visit(HLoadStmt n) {
      R _ret=null;
      n.f0.accept(this);
      toPrintTemp = true;
      String Temp1 = (String)n.f1.accept(this);
      toPrintTemp = false;
      String Temp2 = (String)n.f2.accept(this);
      toPrintInteger = true;
      String integerLit = (String)n.f3.accept(this);
      toPrintInteger = false;
      System.out.println("HLOAD TEMP "+Temp1+" TEMP "+Temp2+" "+integerLit);
      return _ret;
   }

   /**
    * f0 -> "MOVE"
    * f1 -> Temp()
    * f2 -> Exp()
    */
   public R visit(MoveStmt n) {
      R _ret=null;
      n.f0.accept(this);
      toPrintTemp = true;
      String Temp1 = (String)n.f1.accept(this);
      toPrintTemp = false;
      String Temp2 = (String)n.f2.accept(this);
      System.out.println("MOVE TEMP "+Temp1+" TEMP "+Temp2);
      return _ret;
   }

   /**
    * f0 -> "PRINT"
    * f1 -> Exp()
    */
   public R visit(PrintStmt n) {
      R _ret=null;
      //n.f0.accept(this);
      String Temp = (String)n.f1.accept(this);
      System.out.println("PRINT TEMP "+Temp);
      return _ret;
   }

   /**
    * f0 -> StmtExp()
    *       | Call()
    *       | HAllocate()
    *       | BinOp()
    *       | Temp()
    *       | IntegerLiteral()
    *       | Label()
    */
   public R visit(Exp n) {
      R _ret=null;
      return n.f0.accept(this);
   }

   /**
    * f0 -> "BEGIN"
    * f1 -> StmtList()
    * f2 -> "RETURN"
    * f3 -> Exp()
    * f4 -> "END"
    */
   public R visit(StmtExp n) {
      R _ret=null;
      n.f0.accept(this);
      if(isProcedure) {
    	  isProcedure = false;
    	  System.out.println("BEGIN");
    	  n.f1.accept(this);
          n.f2.accept(this);
          String Temp = (String)n.f3.accept(this);
          String result = Integer.toString(globalTempCount++);
          System.out.println("MOVE TEMP "+result+" TEMP "+Temp);
          n.f4.accept(this);
          System.out.println("RETURN TEMP "+result);
          System.out.println("END");
          return (R)result;
      }
      else {
    	  n.f1.accept(this);
          n.f2.accept(this);
          String Temp = (String)n.f3.accept(this);
          String result = Integer.toString(globalTempCount++);
          System.out.println("MOVE TEMP "+result+" TEMP "+Temp);
          n.f4.accept(this);
          return (R)result;
      }
      
   }

   /**
    * f0 -> "CALL"
    * f1 -> Exp()
    * f2 -> "("
    * f3 -> ( Exp() )*
    * f4 -> ")"
    */
   public R visit(Call n) {
      R _ret=null;
      n.f0.accept(this);
      String Temp1 = (String)n.f1.accept(this);
      n.f2.accept(this);
      int count = 0;
      String[] Temp = new String[n.f3.size()];
      for(count = 0;count < n.f3.size(); count++) {
    	  Temp[count] = (String)n.f3.elementAt(count).accept(this);
      }
      //n.f3.accept(this);
      String result = Integer.toString(globalTempCount++);
      System.out.println("MOVE TEMP "+result);
      System.out.println("CALL TEMP "+Temp1+" (");
      for(count = 0;count < n.f3.size(); count++) {
    	  System.out.println(" TEMP "+Temp[count]);
      }
      System.out.println(" )");
      n.f4.accept(this);
      return (R)result;
   }

   /**
    * f0 -> "HALLOCATE"
    * f1 -> Exp()
    */
   public R visit(HAllocate n) {
      R _ret=null;
      //n.f0.accept(this);
      String Temp = (String)n.f1.accept(this);
      String result = Integer.toString(globalTempCount++);
      System.out.println("MOVE TEMP "+result+" HALLOCATE TEMP "+Temp);
      return (R)result;
   }

   /**
    * f0 -> Operator()
    * f1 -> Exp()
    * f2 -> Exp()
    */
   public R visit(BinOp n) {
      R _ret=null;
      
      String Temp1 = (String)n.f1.accept(this);
      
      String Temp2 = (String)n.f2.accept(this);
      
      String result = Integer.toString(globalTempCount++);
      System.out.println("MOVE TEMP "+result+" ");
      n.f0.accept(this);
      System.out.println(" TEMP "+Temp1+" TEMP "+Temp2);
      return (R)result;
   }

   /**
    * f0 -> "LT"
    *       | "PLUS"
    *       | "MINUS"
    *       | "TIMES"
    */
   public R visit(Operator n) {
      R _ret=null;
      int opCode = n.f0.which;
      if(opCode == 0) {
    	  System.out.println("LT ");  
      }
      else if(opCode == 1) {
    	  System.out.println("PLUS ");
      }
      else if(opCode == 2) {
    	  System.out.println("MINUS ");
      }
      else if(opCode == 3) {
    	  System.out.println("TIMES ");
      }
      
      return _ret;
   }

   /**
    * f0 -> "TEMP"
    * f1 -> IntegerLiteral()
    */
   public R visit(Temp n) {
      R _ret=null;
      int temp = globalTempCount++;
      String Temp = Integer.toString(temp);
      //n.f0.accept(this);
      toPrintInteger = true;
      String result = (String)n.f1.accept(this);
      toPrintInteger = false;
      if(!toPrintTemp) {
    	  System.out.println("MOVE TEMP "+Temp+" TEMP "+result);
          return (R)Temp;  
      }
      else {
    	  return (R)result;
      }
   }

   /**
    * f0 -> <INTEGER_LITERAL>
    */
   public R visit(IntegerLiteral n) {
      R _ret=null;
      int temp = globalTempCount++;
      String Temp = Integer.toString(temp);
      String integerVal = n.f0.toString();
      if(!toPrintInteger) {
    	  System.out.println("MOVE TEMP "+Temp+" "+integerVal);  
      }
      else {
    	  return (R)integerVal;
      }
      return (R)Temp;
   }

   /**
    * f0 -> <IDENTIFIER>
    */
   public R visit(Label n) {
      R _ret=null;
      int temp = globalTempCount++;
      String Temp = Integer.toString(temp);
	  String id = n.f0.toString();
	  
      if(!toPrintLabel) {
          System.out.println("MOVE TEMP "+Temp+" "+id);  
      }
      else {
    	  return (R)id;
      }
      return (R)Temp;
   }

}
