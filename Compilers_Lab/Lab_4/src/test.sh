javac Main.java

java Main < BubbleSort.miniIR > BubbleSort.microIR
cd debug
echo BubbleSort
java Main < ../BubbleSort.microIR
cd ..
java -jar pgi.jar < BubbleSort.microIR > BubbleSort.microIR.text
java -jar pgi.jar < BubbleSort.miniIR > BubbleSort.miniIR.text
DIFF= `diff -B -w -i BubbleSort.microIR.text BubbleSort.miniIR.text`
if [ "$DIFF" == "" ]
then
	echo Pass
	rm BubbleSort.microIR.text BubbleSort.miniIR.text
fi

java Main < BinarySearch.miniIR > BinarySearch.microIR
cd debug
echo BinarySearch
java Main < ../BinarySearch.microIR
cd ..
java -jar pgi.jar < BinarySearch.microIR > BinarySearch.microIR.text
java -jar pgi.jar < BinarySearch.miniIR > BinarySearch.miniIR.text
DIFF= `diff -B -w -i BinarySearch.microIR.text BinarySearch.miniIR.text`
if [ "$DIFF" == "" ]
then
	echo Pass
	rm BinarySearch.microIR.text BinarySearch.miniIR.text
fi


java Main < BinaryTree.miniIR > BinaryTree.microIR
cd debug
echo BinaryTree
java Main < ../BinaryTree.microIR
cd ..
java -jar pgi.jar < BinaryTree.microIR > BinaryTree.microIR.text
java -jar pgi.jar < BinaryTree.miniIR > BinaryTree.miniIR.text
DIFF= `diff -B -w -i BinaryTree.microIR.text BinaryTree.miniIR.text`
if [ "$DIFF" == "" ]
then
	echo Pass
	rm BinaryTree.microIR.text BinaryTree.miniIR.text
fi

java Main < Factorial.miniIR > Factorial.microIR
cd debug
echo Factorial
java Main < ../Factorial.microIR
cd ..
java -jar pgi.jar < Factorial.microIR > Factorial.microIR.text
java -jar pgi.jar < Factorial.miniIR > Factorial.miniIR.text
DIFF= `diff -B -w -i Factorial.microIR.text Factorial.miniIR.text`
if [ "$DIFF" == "" ]
then
	echo Pass
	rm Factorial.microIR.text Factorial.miniIR.text
fi

java Main < LinearSearch.miniIR > LinearSearch.microIR
cd debug
echo LinearSearch
java Main < ../LinearSearch.microIR
cd ..
java -jar pgi.jar < LinearSearch.microIR > LinearSearch.microIR.text
java -jar pgi.jar < LinearSearch.miniIR > LinearSearch.miniIR.text
DIFF= `diff -B -w -i LinearSearch.microIR.text LinearSearch.miniIR.text`
if [ "$DIFF" == "" ]
then
	echo Pass
	rm LinearSearch.microIR.text LinearSearch.miniIR.text
fi

java Main < LinkedList.miniIR > LinkedList.microIR
cd debug
echo LinkedList
java Main < ../LinkedList.microIR
cd ..
java -jar pgi.jar < LinkedList.microIR > LinkedList.microIR.text
java -jar pgi.jar < LinkedList.miniIR > LinkedList.miniIR.text
DIFF= `diff -B -w -i LinkedList.microIR.text LinkedList.miniIR.text`
if [ "$DIFF" == "" ]
then
	echo Pass
	rm LinkedList.microIR.text LinkedList.miniIR.text
fi

java Main < MoreThan4.miniIR > MoreThan4.microIR
cd debug
echo MoreThan4
java Main < ../MoreThan4.microIR
cd ..
java -jar pgi.jar < MoreThan4.microIR > MoreThan4.microIR.text
java -jar pgi.jar < MoreThan4.miniIR > MoreThan4.miniIR.text
DIFF= `diff -B -w -i MoreThan4.microIR.text MoreThan4.miniIR.text`
if [ "$DIFF" == "" ]
then
	echo Pass
	rm MoreThan4.microIR.text MoreThan4.miniIR.text
fi

java Main < QuickSort.miniIR > QuickSort.microIR
cd debug
echo QuickSort
java Main < ../QuickSort.microIR
cd ..
java -jar pgi.jar < QuickSort.microIR > QuickSort.microIR.text
java -jar pgi.jar < QuickSort.miniIR > QuickSort.miniIR.text
DIFF= `diff -B -w -i QuickSort.microIR.text QuickSort.miniIR.text`
if [ "$DIFF" == "" ]
then
	echo Pass
	rm QuickSort.microIR.text QuickSort.miniIR.text
fi

java Main < TreeVisitor.miniIR > TreeVisitor.microIR
cd debug
echo TreeVisitor
java Main < ../TreeVisitor.microIR
cd ..
java -jar pgi.jar < TreeVisitor.microIR > TreeVisitor.microIR.text
java -jar pgi.jar < TreeVisitor.miniIR > TreeVisitor.miniIR.text
DIFF= `diff -B -w -i TreeVisitor.microIR.text TreeVisitor.miniIR.text`
if [ "$DIFF" == "" ]
then
	echo Pass
	rm TreeVisitor.microIR.text TreeVisitor.miniIR.text
fi
