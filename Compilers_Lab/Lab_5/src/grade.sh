declare -f FILES
declare -f i

javac Main.java

FILES=tests/*.microIR
echo hello
for i in $FILES
do
	java Main <$i >out.miniRA
	java -jar kgi.jar <out.miniRA >out.text
	DIFF= `diff -B -w -i out.text $(basename ${i::-8}).out`
	if [ "$DIFF" == "" ]
	then
		echo Pass
	else
		echo Fail
	fi
done
