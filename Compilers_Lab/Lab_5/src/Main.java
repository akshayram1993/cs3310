import syntaxtree.*;
import visitor.*;

public class Main {
   public static void main(String [] args) {
      try {
         Node root = new microIRParser(System.in).Goal();
         //System.out.println("Program parsed successfully");
         FirstVisitor f = new FirstVisitor();
         root.accept(f); // Your assignment part is invoked here.
         
         SecondVisitor s = new SecondVisitor();
         s.procedures = f.procedures;
         
         root.accept(s);
         
         ThirdVisitor t = new ThirdVisitor();
         root.accept(t);
         
         FourthVisitor four = new FourthVisitor();
         four.procedures = s.procedures;
         four.auxProcedures = t.auxProcedures;
         root.accept(four);
      }
      catch (ParseException e) {
         System.out.println(e.toString());
      }
   }
} 


