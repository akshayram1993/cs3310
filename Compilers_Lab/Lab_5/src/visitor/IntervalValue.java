package visitor;

public class IntervalValue {
	public int startVal;
	public int endVal;
	public String temp;
	public int register;
	public int stackLocation;
	
	public IntervalValue() {
		this.startVal = 0;
		this.endVal = -1;
		temp = null;
		register = -1;
		stackLocation = -1;
	}
}
