package visitor;
import java.util.*;

public class StatementInfo {
	public int number;
	public Set<String> in ;
	public Set<String> out;
	public Set<String> def;
	public Set<String> use;
	public Set<String> succ;
	
	public StatementInfo() {
		this.number = 0;
		this.def = new HashSet<String>();
		this.in  = new HashSet<String>();
		this.out = new HashSet<String>();
		this.use = new HashSet<String>();
		this.succ = new HashSet<String>();
	}
}
