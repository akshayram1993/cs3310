package visitor;
import java.util.*;

public class ProcedureValue {
	public List statements;
	public HashMap labels;
	public int numOfArgs;
	public HashMap intervals;
	public List liveIntervals;
	public int currentStackLocation;
	
	public ProcedureValue() {
		this.statements = new ArrayList();
		this.labels = new HashMap();
		this.numOfArgs = 0;
		this.intervals = new HashMap();
		this.liveIntervals = new ArrayList();
		this.currentStackLocation = 0;
	}
}
