/* A Bison parser, made by GNU Bison 2.5.  */

/* Bison implementation for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2011 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.5"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Copy the first part of user declarations.  */

/* Line 268 of yacc.c  */
#line 1 "P1.y"

#include <stdio.h>	
#include <string.h>
#include <stdlib.h>
#include "LinkedList.h"

struct macro* head = NULL;


/* Line 268 of yacc.c  */
#line 81 "P1.tab.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     ID = 258,
     NUM = 259,
     CLASS = 260,
     PUBLIC = 261,
     STATIC = 262,
     VOID = 263,
     MAIN = 264,
     STRING = 265,
     SYSTEM = 266,
     OUT = 267,
     PRINTLN = 268,
     EXTENDS = 269,
     RETURN = 270,
     INT = 271,
     BOOLEAN = 272,
     IF = 273,
     ELSE = 274,
     WHILE = 275,
     LENGTH = 276,
     TRUE = 277,
     FALSE = 278,
     NEW = 279,
     MACRO = 280,
     THIS = 281,
     DOT = 282,
     SC = 283,
     LP = 284,
     RP = 285,
     LB = 286,
     RB = 287,
     LSB = 288,
     RSB = 289,
     COMMA = 290,
     EQUAL = 291,
     AND = 292,
     PLUS = 293,
     MINUS = 294,
     STAR = 295,
     DIV = 296,
     LESSTHAN = 297,
     NOT = 298
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 293 of yacc.c  */
#line 10 "P1.y"

	char* string;
	struct node* node;



/* Line 293 of yacc.c  */
#line 167 "P1.tab.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */


/* Line 343 of yacc.c  */
#line 179 "P1.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  3
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   178

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  44
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  23
/* YYNRULES -- Number of rules.  */
#define YYNRULES  64
/* YYNRULES -- Number of states.  */
#define YYNSTATES  184

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   298

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     7,    33,    34,    37,    44,    53,    54,
      57,    71,    72,    76,    77,    82,    83,    88,    92,    94,
      96,    98,    99,   102,   112,   117,   125,   131,   135,   143,
     149,   155,   159,   163,   167,   171,   175,   179,   184,   188,
     190,   197,   202,   203,   206,   207,   210,   211,   215,   216,
     220,   222,   224,   226,   228,   230,   236,   241,   244,   248,
     249,   252,   254,   256,   265
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      45,     0,    -1,    63,    46,    47,    -1,     5,     3,    31,
       6,     7,     8,     9,    29,    10,    33,    34,     3,    30,
      31,    11,    27,    12,    27,    13,    29,    57,    30,    28,
      32,    32,    -1,    -1,    48,    47,    -1,     5,     3,    31,
      53,    49,    32,    -1,     5,     3,    14,     3,    31,    53,
      49,    32,    -1,    -1,    50,    49,    -1,     6,    54,     3,
      29,    51,    30,    31,    53,    55,    15,    57,    28,    32,
      -1,    -1,    54,     3,    52,    -1,    -1,    35,    54,     3,
      52,    -1,    -1,    53,    54,     3,    28,    -1,    16,    33,
      34,    -1,    17,    -1,    16,    -1,     3,    -1,    -1,    56,
      55,    -1,    11,    27,    12,    27,    13,    29,    57,    30,
      28,    -1,     3,    36,    57,    28,    -1,     3,    33,    57,
      34,    36,    57,    28,    -1,    18,    29,    57,    30,    56,
      -1,    31,    55,    32,    -1,    18,    29,    57,    30,    56,
      19,    56,    -1,    20,    29,    57,    30,    56,    -1,     3,
      29,    58,    30,    28,    -1,    62,    37,    62,    -1,    62,
      42,    62,    -1,    62,    38,    62,    -1,    62,    39,    62,
      -1,    62,    40,    62,    -1,    62,    41,    62,    -1,    62,
      33,    62,    34,    -1,    62,    27,    21,    -1,    62,    -1,
      62,    27,     3,    29,    58,    30,    -1,     3,    29,    58,
      30,    -1,    -1,    57,    61,    -1,    -1,     3,    60,    -1,
      -1,    35,     3,    60,    -1,    -1,    35,    57,    61,    -1,
       4,    -1,    22,    -1,    23,    -1,     3,    -1,    26,    -1,
      24,    16,    33,    57,    34,    -1,    24,     3,    29,    30,
      -1,    43,    57,    -1,    29,    57,    30,    -1,    -1,    63,
      64,    -1,    65,    -1,    66,    -1,    25,     3,    29,    59,
      30,    29,    57,    30,    -1,    25,     3,    29,    59,    30,
      31,    55,    32,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    94,    94,   103,   212,   216,   223,   249,   286,   289,
     296,   341,   344,   359,   362,   383,   386,   403,   418,   427,
     436,   447,   450,   458,   502,   521,   550,   572,   592,   619,
     641,   706,   716,   727,   737,   747,   757,   767,   781,   794,
     799,   826,   864,   867,   875,   879,   887,   891,   900,   903,
     915,   923,   931,   939,   947,   955,   977,   997,  1006,  1023,
    1024,  1028,  1029,  1032,  1044
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "ID", "NUM", "CLASS", "PUBLIC", "STATIC",
  "VOID", "MAIN", "STRING", "SYSTEM", "OUT", "PRINTLN", "EXTENDS",
  "RETURN", "INT", "BOOLEAN", "IF", "ELSE", "WHILE", "LENGTH", "TRUE",
  "FALSE", "NEW", "MACRO", "THIS", "DOT", "SC", "LP", "RP", "LB", "RB",
  "LSB", "RSB", "COMMA", "EQUAL", "AND", "PLUS", "MINUS", "STAR", "DIV",
  "LESSTHAN", "NOT", "$accept", "goal", "mainClass",
  "typeDeclaration_star", "typeDeclaration", "methodDeclaration_star",
  "methodDeclaration", "arg_TI", "commaTypeIdentifier_star",
  "typeIdentifierSC_star", "type", "statement_star", "statement",
  "expression", "arg_E", "arg_I", "commaID_star", "commaExpression_star",
  "primaryExpression", "macroDefinition_star", "macroDefinition",
  "macroDefExpression", "macroDefStatement", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    44,    45,    46,    47,    47,    48,    48,    49,    49,
      50,    51,    51,    52,    52,    53,    53,    54,    54,    54,
      54,    55,    55,    56,    56,    56,    56,    56,    56,    56,
      56,    57,    57,    57,    57,    57,    57,    57,    57,    57,
      57,    57,    58,    58,    59,    59,    60,    60,    61,    61,
      62,    62,    62,    62,    62,    62,    62,    62,    62,    63,
      63,    64,    64,    65,    66
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     3,    25,     0,     2,     6,     8,     0,     2,
      13,     0,     3,     0,     4,     0,     4,     3,     1,     1,
       1,     0,     2,     9,     4,     7,     5,     3,     7,     5,
       5,     3,     3,     3,     3,     3,     3,     4,     3,     1,
       6,     4,     0,     2,     0,     2,     0,     3,     0,     3,
       1,     1,     1,     1,     1,     5,     4,     2,     3,     0,
       2,     1,     1,     8,     8
};

/* YYDEFACT[STATE-NAME] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
      59,     0,     0,     1,     0,     0,     4,    60,    61,    62,
       0,     0,     0,     2,     4,     0,    44,     0,     5,     0,
      46,     0,     0,    15,     0,     0,    45,     0,     0,     8,
       0,    46,     0,    21,    15,    20,     0,    19,    18,     0,
       8,     0,     0,    47,    53,    50,    51,    52,     0,    54,
       0,     0,     0,    39,     0,     0,     0,     0,    21,     0,
      21,     8,     0,     0,     6,     9,     0,     0,    42,     0,
       0,     0,    57,    63,     0,     0,     0,     0,     0,     0,
       0,     0,    42,     0,     0,     0,     0,     0,     0,    64,
      22,     0,     0,    17,    16,     0,    48,     0,     0,     0,
      58,     0,    38,    53,     0,    31,    33,    34,    35,    36,
      32,     0,     0,     0,     0,     0,     0,    27,     7,    11,
       0,     0,    43,    41,    56,     0,    42,    37,     0,     0,
      24,     0,     0,     0,     0,     0,     0,    48,    55,     0,
      30,     0,     0,    26,    29,     0,    13,     0,    49,    40,
       0,     0,     0,    15,     0,    12,     0,    25,     0,    28,
      21,     0,     0,     0,    20,     0,    13,     0,    23,     0,
      14,     0,     0,     0,     0,     0,    10,     0,     0,     0,
       0,     0,     0,     3
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     6,    13,    14,    39,    40,   134,   155,    29,
      41,    59,    60,    96,    97,    21,    26,   122,    53,     2,
       7,     8,     9
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -119
static const yytype_int16 yypact[] =
{
    -119,    38,    15,  -119,     5,    41,    43,  -119,  -119,  -119,
      18,    28,    60,  -119,    43,    74,    63,    19,  -119,    64,
      49,    56,    84,  -119,    82,    95,  -119,    54,    68,     6,
      91,    49,     3,    10,  -119,  -119,    65,    69,  -119,    71,
      99,    98,    77,  -119,    78,  -119,  -119,  -119,     8,  -119,
       3,     3,    80,    37,    36,    81,    83,    85,    10,    79,
      10,     6,   110,    87,  -119,  -119,    88,   105,     3,    89,
      92,    94,  -119,  -119,    40,    13,    13,    13,    13,    13,
      13,    13,     3,     3,     3,   114,     3,     3,    90,  -119,
    -119,    96,   100,  -119,  -119,    97,   101,   102,   103,     3,
    -119,   106,  -119,  -119,    93,  -119,  -119,  -119,  -119,  -119,
    -119,   104,   107,   111,   113,   108,   112,  -119,  -119,    65,
     109,     3,  -119,  -119,  -119,   115,     3,  -119,   116,   117,
    -119,   118,    10,    10,   120,   142,   144,   101,  -119,   121,
    -119,     3,   119,   133,  -119,    86,   122,   124,  -119,  -119,
     127,     3,    10,  -119,    65,  -119,   125,  -119,   128,  -119,
      42,   156,   149,   134,    36,   146,   122,   136,  -119,     3,
    -119,   152,   137,   139,   135,   155,  -119,   140,     3,   141,
     145,   138,   143,  -119
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -119,  -119,  -119,   158,  -119,   -30,  -119,  -119,    11,   -33,
     -31,   -56,  -118,   -32,   -79,  -119,   147,    39,    16,  -119,
    -119,  -119,  -119
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint8 yytable[] =
{
      52,    61,    88,   111,    90,    62,    44,    45,    10,    35,
      65,    69,    36,    54,   143,   144,   103,    45,    71,    72,
       4,    55,    37,    38,    70,    46,    47,    48,    56,    49,
      57,    91,    50,    22,   159,    46,    47,    48,     3,    49,
       5,    58,    50,   101,    11,   164,    51,   139,    12,    15,
      23,   112,   113,    55,   115,   116,    51,    16,    37,    38,
      56,   102,    57,    17,    74,    82,    20,   125,    35,    83,
      75,    24,    84,    58,    76,    77,    78,    79,    80,    81,
      19,    37,    38,    32,    25,    33,    27,    28,   135,   137,
      30,   104,   105,   106,   107,   108,   109,   110,    31,    34,
      42,    66,    63,    64,   165,    36,    67,    68,    85,   150,
      73,    89,    86,    92,    87,    95,    94,   153,    98,   158,
     160,    93,   117,   161,   100,    99,   114,   127,   118,   119,
     120,   142,   123,   124,   128,   126,   121,   172,   132,   130,
     131,   129,   133,   136,   140,   146,   179,   147,   151,   138,
     145,   149,   152,   141,   156,   157,   162,   154,   163,   166,
     167,   169,   168,   171,   173,   174,   175,   176,   177,   178,
     182,   180,    18,   181,     0,   183,   148,   170,    43
};

#define yypact_value_is_default(yystate) \
  ((yystate) == (-119))

#define yytable_value_is_error(yytable_value) \
  YYID (0)

static const yytype_int16 yycheck[] =
{
      32,    34,    58,    82,    60,    36,     3,     4,     3,     3,
      40,     3,     6,     3,   132,   133,     3,     4,    50,    51,
       5,    11,    16,    17,    16,    22,    23,    24,    18,    26,
      20,    61,    29,    14,   152,    22,    23,    24,     0,    26,
      25,    31,    29,     3,     3,     3,    43,   126,     5,    31,
      31,    83,    84,    11,    86,    87,    43,    29,    16,    17,
      18,    21,    20,     3,    27,    29,     3,    99,     3,    33,
      33,     7,    36,    31,    37,    38,    39,    40,    41,    42,
       6,    16,    17,    29,    35,    31,    30,     3,   119,   121,
       8,    75,    76,    77,    78,    79,    80,    81,     3,    31,
       9,     3,    33,    32,   160,     6,    29,    29,    27,   141,
      30,    32,    29,     3,    29,    10,    28,    31,    29,   151,
     153,    34,    32,   154,    30,    33,    12,    34,    32,    29,
      33,    13,    30,    30,    30,    29,    35,   169,    30,    28,
      27,    34,    30,    34,    28,     3,   178,     3,    29,    34,
      30,    30,    19,    36,    30,    28,    31,    35,    30,     3,
      11,    15,    28,    27,    12,    28,    27,    32,    13,    29,
      32,    30,    14,    28,    -1,    32,   137,   166,    31
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    45,    63,     0,     5,    25,    46,    64,    65,    66,
       3,     3,     5,    47,    48,    31,    29,     3,    47,     6,
       3,    59,    14,    31,     7,    35,    60,    30,     3,    53,
       8,     3,    29,    31,    31,     3,     6,    16,    17,    49,
      50,    54,     9,    60,     3,     4,    22,    23,    24,    26,
      29,    43,    57,    62,     3,    11,    18,    20,    31,    55,
      56,    53,    54,    33,    32,    49,     3,    29,    29,     3,
      16,    57,    57,    30,    27,    33,    37,    38,    39,    40,
      41,    42,    29,    33,    36,    27,    29,    29,    55,    32,
      55,    49,     3,    34,    28,    10,    57,    58,    29,    33,
      30,     3,    21,     3,    62,    62,    62,    62,    62,    62,
      62,    58,    57,    57,    12,    57,    57,    32,    32,    29,
      33,    35,    61,    30,    30,    57,    29,    34,    30,    34,
      28,    27,    30,    30,    51,    54,    34,    57,    34,    58,
      28,    36,    13,    56,    56,    30,     3,     3,    61,    30,
      57,    29,    19,    31,    35,    52,    30,    28,    57,    56,
      53,    54,    31,    30,     3,    55,     3,    11,    28,    15,
      52,    27,    57,    12,    28,    27,    32,    13,    29,    57,
      30,    28,    32,    32
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  However,
   YYFAIL appears to be in use.  Nevertheless, it is formally deprecated
   in Bison 2.4.2's NEWS entry, where a plan to phase it out is
   discussed.  */

#define YYFAIL		goto yyerrlab
#if defined YYFAIL
  /* This is here to suppress warnings from the GCC cpp's
     -Wunused-macros.  Normally we don't worry about that warning, but
     some users do, and we want to make it easy for users to remove
     YYFAIL uses, which will produce warnings from Bison 2.5.  */
#endif

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* This macro is provided for backward compatibility. */

#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (0, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  YYSIZE_T yysize1;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = 0;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - Assume YYFAIL is not used.  It's too flawed to consider.  See
       <http://lists.gnu.org/archive/html/bison-patches/2009-12/msg00024.html>
       for details.  YYERROR is fine as it does not invoke this
       function.
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                yysize1 = yysize + yytnamerr (0, yytname[yyx]);
                if (! (yysize <= yysize1
                       && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                  return 2;
                yysize = yysize1;
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  yysize1 = yysize + yystrlen (yyformat);
  if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
    return 2;
  yysize = yysize1;

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:

/* Line 1806 of yacc.c  */
#line 94 "P1.y"
    {
																		(yyval.node) = NULL;
																		(yyval.node) = LinkingLists((yyval.node),(yyvsp[(2) - (3)].node));
																		(yyval.node) = LinkingLists((yyval.node),(yyvsp[(3) - (3)].node));
																		print((yyval.node));
																		
																	}
    break;

  case 3:

/* Line 1806 of yacc.c  */
#line 103 "P1.y"
    {
																				
																																							struct node* temp1 = malloc(sizeof(struct node));
																																							temp1->string = malloc(strlen((yyvsp[(1) - (25)].string))+1);
																																							strcpy(temp1->string,(yyvsp[(1) - (25)].string));
																																							struct node* temp2 = malloc(sizeof(struct node));
																																							temp2->string = malloc(strlen((yyvsp[(2) - (25)].string))+1);
																																							strcpy(temp2->string,(yyvsp[(2) - (25)].string));
																																							struct node* temp3 = malloc(sizeof(struct node));
																																							temp3->string = malloc(strlen((yyvsp[(3) - (25)].string))+1);
																																							strcpy(temp3->string,(yyvsp[(3) - (25)].string));
																																							struct node* temp4 = malloc(sizeof(struct node));
																																							temp4->string = malloc(strlen((yyvsp[(4) - (25)].string))+1);
																																							strcpy(temp4->string,(yyvsp[(4) - (25)].string));
																																							struct node* temp5 = malloc(sizeof(struct node));
																																							temp5->string = malloc(strlen((yyvsp[(5) - (25)].string))+1);
																																							strcpy(temp5->string,(yyvsp[(5) - (25)].string));
																																							struct node* temp6 = malloc(sizeof(struct node));
																																							temp6->string = malloc(strlen((yyvsp[(6) - (25)].string))+1);
																																							strcpy(temp6->string,(yyvsp[(6) - (25)].string));
																																							struct node* temp7 = malloc(sizeof(struct node));
																																							temp7->string = malloc(strlen((yyvsp[(7) - (25)].string))+1);
																																							strcpy(temp7->string,(yyvsp[(7) - (25)].string));
																																							struct node* temp8 = malloc(sizeof(struct node));
																																							temp8->string = malloc(strlen((yyvsp[(8) - (25)].string))+1);
																																							strcpy(temp8->string,(yyvsp[(8) - (25)].string));
																																							struct node* temp9 = malloc(sizeof(struct node));
																																							temp9->string = malloc(strlen((yyvsp[(9) - (25)].string))+1);
																																							strcpy(temp9->string,(yyvsp[(9) - (25)].string));
																																							struct node* temp10 = malloc(sizeof(struct node));
																																							temp10->string = malloc(strlen((yyvsp[(10) - (25)].string))+1);
																																							strcpy(temp10->string,(yyvsp[(10) - (25)].string));
																																							struct node* temp11 = malloc(sizeof(struct node));
																																							temp11->string = malloc(strlen((yyvsp[(11) - (25)].string))+1);
																																							strcpy(temp11->string,(yyvsp[(11) - (25)].string));
																																							struct node* temp12 = malloc(sizeof(struct node));
																																							temp12->string = malloc(strlen((yyvsp[(12) - (25)].string))+1);
																																							strcpy(temp12->string,(yyvsp[(12) - (25)].string));
																																							struct node* temp13 = malloc(sizeof(struct node));
																																							temp13->string = malloc(strlen((yyvsp[(13) - (25)].string))+1);
																																							strcpy(temp13->string,(yyvsp[(13) - (25)].string));
																																							struct node* temp14 = malloc(sizeof(struct node));
																																							temp14->string = malloc(strlen((yyvsp[(14) - (25)].string))+1);
																																							strcpy(temp14->string,(yyvsp[(14) - (25)].string));
																																							struct node* temp15 = malloc(sizeof(struct node));
																																							temp15->string = malloc(strlen((yyvsp[(15) - (25)].string))+1);
																																							strcpy(temp15->string,(yyvsp[(15) - (25)].string));
																																							struct node* temp16 = malloc(sizeof(struct node));
																																							temp16->string = malloc(strlen((yyvsp[(16) - (25)].string))+1);
																																							strcpy(temp16->string,(yyvsp[(16) - (25)].string));
																																							struct node* temp17 = malloc(sizeof(struct node));
																																							temp17->string = malloc(strlen((yyvsp[(17) - (25)].string))+1);
																																							strcpy(temp17->string,(yyvsp[(17) - (25)].string));
																																							struct node* temp18 = malloc(sizeof(struct node));
																																							temp18->string = malloc(strlen((yyvsp[(18) - (25)].string))+1);
																																							strcpy(temp18->string,(yyvsp[(18) - (25)].string));
																																							struct node* temp19 = malloc(sizeof(struct node));
																																							temp19->string = malloc(strlen((yyvsp[(19) - (25)].string))+1);
																																							strcpy(temp19->string,(yyvsp[(19) - (25)].string));
																																							struct node* temp20 = malloc(sizeof(struct node));
																																							temp20->string = malloc(strlen((yyvsp[(20) - (25)].string))+1);
																																							strcpy(temp20->string,(yyvsp[(20) - (25)].string));
																																							struct node* temp22 = malloc(sizeof(struct node));
																																							temp22->string = malloc(strlen((yyvsp[(22) - (25)].string))+1);
																																							strcpy(temp22->string,(yyvsp[(22) - (25)].string));
																																							struct node* temp23 = malloc(sizeof(struct node));
																																							temp23->string = malloc(strlen((yyvsp[(23) - (25)].string))+1);
																																							strcpy(temp23->string,(yyvsp[(23) - (25)].string));
																																							struct node* temp24 = malloc(sizeof(struct node));
																																							temp24->string = malloc(strlen((yyvsp[(24) - (25)].string))+1);
																																							strcpy(temp24->string,(yyvsp[(24) - (25)].string));
																																							struct node* temp25 = malloc(sizeof(struct node));
																																							temp25->string = malloc(strlen((yyvsp[(25) - (25)].string))+1);
																																							strcpy(temp25->string,(yyvsp[(25) - (25)].string));
																																							(yyval.node) = NULL;
																																							(yyval.node) = ListInsert((yyval.node),temp1);
																																							(yyval.node) = ListInsert((yyval.node),temp2);
																																							(yyval.node) = ListInsert((yyval.node),temp3);
																																							(yyval.node) = ListInsert((yyval.node),temp4);
																																							(yyval.node) = ListInsert((yyval.node),temp5);
																																							(yyval.node) = ListInsert((yyval.node),temp6);
																																							(yyval.node) = ListInsert((yyval.node),temp7);
																																							(yyval.node) = ListInsert((yyval.node),temp8);
																																							(yyval.node) = ListInsert((yyval.node),temp9);
																																							(yyval.node) = ListInsert((yyval.node),temp10);
																																							(yyval.node) = ListInsert((yyval.node),temp11);
																																							(yyval.node) = ListInsert((yyval.node),temp12);
																																							(yyval.node) = ListInsert((yyval.node),temp13);
																																							(yyval.node) = ListInsert((yyval.node),temp14);
																																							(yyval.node) = ListInsert((yyval.node),temp15);
																																							(yyval.node) = ListInsert((yyval.node),temp16);
																																							(yyval.node) = ListInsert((yyval.node),temp17);
																																							(yyval.node) = ListInsert((yyval.node),temp18);
																																							(yyval.node) = ListInsert((yyval.node),temp19);
																																							(yyval.node) = ListInsert((yyval.node),temp20);
																																							(yyval.node) = LinkingLists((yyval.node),(yyvsp[(21) - (25)].node));
																																							(yyval.node) = ListInsert((yyval.node),temp22);
																																							(yyval.node) = ListInsert((yyval.node),temp23);
																																							(yyval.node) = ListInsert((yyval.node),temp24);
																																							(yyval.node) = ListInsert((yyval.node),temp25);
																																							
																																							
																																							

																																						}
    break;

  case 4:

/* Line 1806 of yacc.c  */
#line 212 "P1.y"
    {
																														(yyval.node) = NULL;
																													}
    break;

  case 5:

/* Line 1806 of yacc.c  */
#line 216 "P1.y"
    {
																														(yyval.node) = NULL;
																														(yyval.node) = LinkingLists((yyval.node),(yyvsp[(1) - (2)].node));
																														(yyval.node) = LinkingLists((yyval.node),(yyvsp[(2) - (2)].node));
																													}
    break;

  case 6:

/* Line 1806 of yacc.c  */
#line 223 "P1.y"
    {
																				
																															struct node* temp1 = malloc(sizeof(struct node));
																															temp1->string = malloc(strlen((yyvsp[(1) - (6)].string))+1);
																															strcpy(temp1->string,(yyvsp[(1) - (6)].string));
																															struct node* temp2 = malloc(sizeof(struct node));
																															temp2->string = malloc(strlen((yyvsp[(2) - (6)].string))+1);
																															strcpy(temp2->string,(yyvsp[(2) - (6)].string));
																															struct node* temp3 = malloc(sizeof(struct node));
																															temp3->string = malloc(strlen((yyvsp[(3) - (6)].string))+1);
																															strcpy(temp3->string,(yyvsp[(3) - (6)].string));
																															struct node* temp4 = malloc(sizeof(struct node));
																															temp4->string = malloc(strlen((yyvsp[(6) - (6)].string))+1);
																															strcpy(temp4->string,(yyvsp[(6) - (6)].string));
																															
																															(yyval.node) = NULL;
																															(yyval.node) = ListInsert((yyval.node),temp1);
																															(yyval.node) = ListInsert((yyval.node),temp2);
																															(yyval.node) = ListInsert((yyval.node),temp3);
																															(yyval.node) = LinkingLists((yyval.node),(yyvsp[(4) - (6)].node));
																															(yyval.node) = LinkingLists((yyval.node),(yyvsp[(5) - (6)].node));
																															(yyval.node) = ListInsert((yyval.node),temp4);
																															
																															

																														}
    break;

  case 7:

/* Line 1806 of yacc.c  */
#line 249 "P1.y"
    {
																				
																															struct node* temp1 = malloc(sizeof(struct node));
																															temp1->string = malloc(strlen((yyvsp[(1) - (8)].string))+1);
																															strcpy(temp1->string,(yyvsp[(1) - (8)].string));
																															struct node* temp2 = malloc(sizeof(struct node));
																															temp2->string = malloc(strlen((yyvsp[(2) - (8)].string))+1);
																															strcpy(temp2->string,(yyvsp[(2) - (8)].string));
																															struct node* temp3 = malloc(sizeof(struct node));
																															temp3->string = malloc(strlen((yyvsp[(3) - (8)].string))+1);
																															strcpy(temp3->string,(yyvsp[(3) - (8)].string));
																															struct node* temp4 = malloc(sizeof(struct node));
																															temp4->string = malloc(strlen((yyvsp[(4) - (8)].string))+1);
																															strcpy(temp4->string,(yyvsp[(4) - (8)].string));
																															struct node* temp5 = malloc(sizeof(struct node));
																															temp5->string = malloc(strlen((yyvsp[(5) - (8)].string))+1);
																															strcpy(temp5->string,(yyvsp[(5) - (8)].string));
																															struct node* temp6 = malloc(sizeof(struct node));
																															temp6->string = malloc(strlen((yyvsp[(8) - (8)].string))+1);
																															strcpy(temp6->string,(yyvsp[(8) - (8)].string));
																															
																															(yyval.node) = NULL;
																															(yyval.node) = ListInsert((yyval.node),temp1);
																															(yyval.node) = ListInsert((yyval.node),temp2);
																															(yyval.node) = ListInsert((yyval.node),temp3);
																															(yyval.node) = ListInsert((yyval.node),temp4);
																															(yyval.node) = ListInsert((yyval.node),temp5);
																															(yyval.node) = LinkingLists((yyval.node),(yyvsp[(6) - (8)].node));
																															(yyval.node) = LinkingLists((yyval.node),(yyvsp[(7) - (8)].node));
																															(yyval.node) = ListInsert((yyval.node),temp6);
																															
																															

																														}
    break;

  case 8:

/* Line 1806 of yacc.c  */
#line 286 "P1.y"
    {
																															(yyval.node) = NULL;
																														}
    break;

  case 9:

/* Line 1806 of yacc.c  */
#line 289 "P1.y"
    {
																															(yyval.node) = NULL;
																															(yyval.node) = LinkingLists((yyval.node),(yyvsp[(1) - (2)].node));
																															(yyval.node) = LinkingLists((yyval.node),(yyvsp[(2) - (2)].node));
																														}
    break;

  case 10:

/* Line 1806 of yacc.c  */
#line 296 "P1.y"
    {
																				
																															struct node* temp1 = malloc(sizeof(struct node));
																															temp1->string = malloc(strlen((yyvsp[(1) - (13)].string))+1);
																															strcpy(temp1->string,(yyvsp[(1) - (13)].string));
																															struct node* temp2 = malloc(sizeof(struct node));
																															temp2->string = malloc(strlen((yyvsp[(3) - (13)].string))+1);
																															strcpy(temp2->string,(yyvsp[(3) - (13)].string));
																															struct node* temp3 = malloc(sizeof(struct node));
																															temp3->string = malloc(strlen((yyvsp[(4) - (13)].string))+1);
																															strcpy(temp3->string,(yyvsp[(4) - (13)].string));
																															struct node* temp4 = malloc(sizeof(struct node));
																															temp4->string = malloc(strlen((yyvsp[(6) - (13)].string))+1);
																															strcpy(temp4->string,(yyvsp[(6) - (13)].string));
																															struct node* temp5 = malloc(sizeof(struct node));
																															temp5->string = malloc(strlen((yyvsp[(7) - (13)].string))+1);
																															strcpy(temp5->string,(yyvsp[(7) - (13)].string));
																															struct node* temp6 = malloc(sizeof(struct node));
																															temp6->string = malloc(strlen((yyvsp[(10) - (13)].string))+1);
																															strcpy(temp6->string,(yyvsp[(10) - (13)].string));
																															struct node* temp7 = malloc(sizeof(struct node));
																															temp7->string = malloc(strlen((yyvsp[(12) - (13)].string))+1);
																															strcpy(temp7->string,(yyvsp[(12) - (13)].string));
																															struct node* temp8 = malloc(sizeof(struct node));
																															temp8->string = malloc(strlen((yyvsp[(13) - (13)].string))+1);
																															strcpy(temp8->string,(yyvsp[(13) - (13)].string));
																															(yyval.node) = NULL;
																															(yyval.node) = ListInsert((yyval.node),temp1);
																															(yyval.node) = LinkingLists((yyval.node),(yyvsp[(2) - (13)].node));
																															(yyval.node) = ListInsert((yyval.node),temp2);
																															(yyval.node) = ListInsert((yyval.node),temp3);
																															(yyval.node) = LinkingLists((yyval.node),(yyvsp[(5) - (13)].node));
																															(yyval.node) = ListInsert((yyval.node),temp4);
																															(yyval.node) = ListInsert((yyval.node),temp5);
																															(yyval.node) = LinkingLists((yyval.node),(yyvsp[(8) - (13)].node));
																															(yyval.node) = LinkingLists((yyval.node),(yyvsp[(9) - (13)].node));
																															(yyval.node) = ListInsert((yyval.node),temp6);
																															(yyval.node) = LinkingLists((yyval.node),(yyvsp[(11) - (13)].node));
																															(yyval.node) = ListInsert((yyval.node),temp7);
																															(yyval.node) = ListInsert((yyval.node),temp8);
																															

																														}
    break;

  case 11:

/* Line 1806 of yacc.c  */
#line 341 "P1.y"
    {
																				(yyval.node) = NULL;	
																			}
    break;

  case 12:

/* Line 1806 of yacc.c  */
#line 344 "P1.y"
    {
																				
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen((yyvsp[(2) - (3)].string))+1);
																				strcpy(temp2->string,(yyvsp[(2) - (3)].string));
																				
																				(yyval.node) = NULL;
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(1) - (3)].node));
																				
																				(yyval.node) = ListInsert((yyval.node),temp2);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(3) - (3)].node));
																				
																			}
    break;

  case 13:

/* Line 1806 of yacc.c  */
#line 359 "P1.y"
    {
																				(yyval.node) = NULL;	
																			}
    break;

  case 14:

/* Line 1806 of yacc.c  */
#line 362 "P1.y"
    {
																				
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen((yyvsp[(1) - (4)].string))+1);
																				strcpy(temp1->string,(yyvsp[(1) - (4)].string));
																				
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen((yyvsp[(3) - (4)].string))+1);
																				strcpy(temp2->string,(yyvsp[(3) - (4)].string));
																				
																				(yyval.node) = NULL;
																				(yyval.node) = ListInsert((yyval.node),temp1);

																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(2) - (4)].node));
																				(yyval.node) = ListInsert((yyval.node),temp2);
																				
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(4) - (4)].node));
																				
																			}
    break;

  case 15:

/* Line 1806 of yacc.c  */
#line 383 "P1.y"
    {
																				(yyval.node) = NULL;	
																			}
    break;

  case 16:

/* Line 1806 of yacc.c  */
#line 386 "P1.y"
    {
																				
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen((yyvsp[(3) - (4)].string))+1);
																				strcpy(temp2->string,(yyvsp[(3) - (4)].string));
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen((yyvsp[(4) - (4)].string))+1);
																				strcpy(temp3->string,(yyvsp[(4) - (4)].string));
																				(yyval.node) = NULL;
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(1) - (4)].node));
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(2) - (4)].node));
																				(yyval.node) = ListInsert((yyval.node),temp2);
																				(yyval.node) = ListInsert((yyval.node),temp3);
																				
																			}
    break;

  case 17:

/* Line 1806 of yacc.c  */
#line 403 "P1.y"
    {
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen((yyvsp[(1) - (3)].string))+1);
																				strcpy(temp1->string,(yyvsp[(1) - (3)].string));
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen((yyvsp[(2) - (3)].string))+1);
																				strcpy(temp2->string,(yyvsp[(2) - (3)].string));
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen((yyvsp[(3) - (3)].string))+1);
																				strcpy(temp3->string,(yyvsp[(3) - (3)].string));
																				(yyval.node) = NULL;
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				(yyval.node) = ListInsert((yyval.node),temp2);
																				(yyval.node) = ListInsert((yyval.node),temp3);
																			}
    break;

  case 18:

/* Line 1806 of yacc.c  */
#line 418 "P1.y"
    {
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen((yyvsp[(1) - (1)].string))+1);
																				strcpy(temp1->string,(yyvsp[(1) - (1)].string));
																				
																				(yyval.node) = NULL;
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				
																			}
    break;

  case 19:

/* Line 1806 of yacc.c  */
#line 427 "P1.y"
    {
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen((yyvsp[(1) - (1)].string))+1);
																				strcpy(temp1->string,(yyvsp[(1) - (1)].string));
																				
																				(yyval.node) = NULL;
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				
																			}
    break;

  case 20:

/* Line 1806 of yacc.c  */
#line 436 "P1.y"
    {
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen((yyvsp[(1) - (1)].string))+1);
																				strcpy(temp1->string,(yyvsp[(1) - (1)].string));
																				
																				(yyval.node) = NULL;
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				
																			}
    break;

  case 21:

/* Line 1806 of yacc.c  */
#line 447 "P1.y"
    {
																					(yyval.node) = NULL;	
																				}
    break;

  case 22:

/* Line 1806 of yacc.c  */
#line 450 "P1.y"
    {
																					(yyval.node) = NULL;
																					(yyval.node) = LinkingLists((yyval.node),(yyvsp[(1) - (2)].node));
																					(yyval.node) = LinkingLists((yyval.node),(yyvsp[(2) - (2)].node));
																					
																				}
    break;

  case 23:

/* Line 1806 of yacc.c  */
#line 458 "P1.y"
    {
																				
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen((yyvsp[(1) - (9)].string))+1);
																				strcpy(temp1->string,(yyvsp[(1) - (9)].string));
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen((yyvsp[(2) - (9)].string))+1);
																				strcpy(temp2->string,(yyvsp[(2) - (9)].string));
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen((yyvsp[(3) - (9)].string))+1);
																				strcpy(temp3->string,(yyvsp[(3) - (9)].string));
																				struct node* temp4 = malloc(sizeof(struct node));
																				temp4->string = malloc(strlen((yyvsp[(4) - (9)].string))+1);
																				strcpy(temp4->string,(yyvsp[(4) - (9)].string));
																				struct node* temp5 = malloc(sizeof(struct node));
																				temp5->string = malloc(strlen((yyvsp[(5) - (9)].string))+1);
																				strcpy(temp5->string,(yyvsp[(5) - (9)].string));
																				struct node* temp6 = malloc(sizeof(struct node));
																				temp6->string = malloc(strlen((yyvsp[(6) - (9)].string))+1);
																				strcpy(temp6->string,(yyvsp[(6) - (9)].string));
																				struct node* temp7 = malloc(sizeof(struct node));
																				temp7->string = malloc(strlen((yyvsp[(8) - (9)].string))+1);
																				strcpy(temp7->string,(yyvsp[(8) - (9)].string));
																				struct node* temp8 = malloc(sizeof(struct node));
																				temp8->string = malloc(strlen((yyvsp[(9) - (9)].string))+1);
																				strcpy(temp8->string,(yyvsp[(9) - (9)].string));
																				(yyval.node) = NULL;
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				(yyval.node) = ListInsert((yyval.node),temp2);
																				(yyval.node) = ListInsert((yyval.node),temp3);
																				(yyval.node) = ListInsert((yyval.node),temp4);
																				(yyval.node) = ListInsert((yyval.node),temp5);
																				(yyval.node) = ListInsert((yyval.node),temp6);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(7) - (9)].node));
																				(yyval.node) = ListInsert((yyval.node),temp7);
																				(yyval.node) = ListInsert((yyval.node),temp8);
																				
																				 





																			}
    break;

  case 24:

/* Line 1806 of yacc.c  */
#line 502 "P1.y"
    {
																				
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen((yyvsp[(1) - (4)].string))+1);
																				strcpy(temp1->string,(yyvsp[(1) - (4)].string));
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen((yyvsp[(2) - (4)].string))+1);
																				strcpy(temp2->string,(yyvsp[(2) - (4)].string));
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen((yyvsp[(4) - (4)].string))+1);
																				strcpy(temp3->string,(yyvsp[(4) - (4)].string));
																				(yyval.node) = NULL;
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				(yyval.node) = ListInsert((yyval.node),temp2);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(3) - (4)].node));
																				(yyval.node) = ListInsert((yyval.node),temp3);
																				
																			
																			}
    break;

  case 25:

/* Line 1806 of yacc.c  */
#line 521 "P1.y"
    {
																				
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen((yyvsp[(1) - (7)].string))+1);
																				strcpy(temp1->string,(yyvsp[(1) - (7)].string));
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen((yyvsp[(2) - (7)].string))+1);
																				strcpy(temp2->string,(yyvsp[(2) - (7)].string));
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen((yyvsp[(4) - (7)].string))+1);
																				strcpy(temp3->string,(yyvsp[(4) - (7)].string));
																				struct node* temp4 = malloc(sizeof(struct node));
																				temp4->string = malloc(strlen((yyvsp[(5) - (7)].string))+1);
																				strcpy(temp4->string,(yyvsp[(5) - (7)].string));
																				struct node* temp5 = malloc(sizeof(struct node));
																				temp5->string = malloc(strlen((yyvsp[(7) - (7)].string)));
																				strcpy(temp5->string,(yyvsp[(7) - (7)].string));
																				
																				(yyval.node) = NULL;
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				(yyval.node) = ListInsert((yyval.node),temp2);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(3) - (7)].node));
																				(yyval.node) = ListInsert((yyval.node),temp3);
																				(yyval.node) = ListInsert((yyval.node),temp4);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(6) - (7)].node)); 
																				(yyval.node) = ListInsert((yyval.node),temp5);
																				
																				
																			}
    break;

  case 26:

/* Line 1806 of yacc.c  */
#line 550 "P1.y"
    {
																				
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen((yyvsp[(1) - (5)].string))+1);
																				strcpy(temp1->string,(yyvsp[(1) - (5)].string));
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen((yyvsp[(2) - (5)].string))+1);
																				strcpy(temp2->string,(yyvsp[(2) - (5)].string));
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen((yyvsp[(4) - (5)].string))+1);
																				strcpy(temp3->string,(yyvsp[(4) - (5)].string));
																				
																				
																				(yyval.node) = NULL;
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				(yyval.node) = ListInsert((yyval.node),temp2);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(3) - (5)].node));
																				(yyval.node) = ListInsert((yyval.node),temp3);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(5) - (5)].node));
																				
																				
																			}
    break;

  case 27:

/* Line 1806 of yacc.c  */
#line 572 "P1.y"
    {
																				
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen((yyvsp[(1) - (3)].string))+1);
																				strcpy(temp1->string,(yyvsp[(1) - (3)].string));
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen((yyvsp[(3) - (3)].string))+1);
																				strcpy(temp2->string,(yyvsp[(3) - (3)].string));
																				
																				
																				
																				(yyval.node) = NULL;
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(2) - (3)].node));
																				(yyval.node) = ListInsert((yyval.node),temp2);
																				
																			
																				
																				
																			}
    break;

  case 28:

/* Line 1806 of yacc.c  */
#line 592 "P1.y"
    {
																				
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen((yyvsp[(1) - (7)].string))+1);
																				strcpy(temp1->string,(yyvsp[(1) - (7)].string));
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen((yyvsp[(2) - (7)].string))+1);
																				strcpy(temp2->string,(yyvsp[(2) - (7)].string));
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen((yyvsp[(4) - (7)].string))+1);
																				strcpy(temp3->string,(yyvsp[(4) - (7)].string));
																				struct node* temp4 = malloc(sizeof(struct node));
																				temp4->string = malloc(strlen((yyvsp[(6) - (7)].string))+1);
																				strcpy(temp4->string,(yyvsp[(6) - (7)].string));
																				
																				
																				(yyval.node) = NULL;
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				(yyval.node) = ListInsert((yyval.node),temp2);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(3) - (7)].node));
																				(yyval.node) = ListInsert((yyval.node),temp3);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(5) - (7)].node));
																				(yyval.node) = ListInsert((yyval.node),temp4);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(7) - (7)].node)); 
																				
																				
																			}
    break;

  case 29:

/* Line 1806 of yacc.c  */
#line 619 "P1.y"
    {
																				
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen((yyvsp[(1) - (5)].string))+1);
																				strcpy(temp1->string,(yyvsp[(1) - (5)].string));
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen((yyvsp[(2) - (5)].string))+1);
																				strcpy(temp2->string,(yyvsp[(2) - (5)].string));
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen((yyvsp[(4) - (5)].string))+1);
																				strcpy(temp3->string,(yyvsp[(4) - (5)].string));
																				
																				
																				(yyval.node) = NULL;
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				(yyval.node) = ListInsert((yyval.node),temp2);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(3) - (5)].node));
																				(yyval.node) = ListInsert((yyval.node),temp3);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(5) - (5)].node));
																				
																				
																			}
    break;

  case 30:

/* Line 1806 of yacc.c  */
#line 641 "P1.y"
    {
																				
																				/*struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1));
																				strcpy(temp1->string,$1);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($2));
																				strcpy(temp2->string,$2);
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen($4));
																				strcpy(temp3->string,$4);
																				struct node* temp4 = malloc(sizeof(struct node));
																				temp4->string = malloc(strlen($5));
																				strcpy(temp4->string,$5);
																				
																				
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);
																				$$ = ListInsert($$,temp2);
																				$$ = LinkingLists($$,$3);
																				$$ = ListInsert($$,temp3);
																				$$ = LinkingLists($$,temp4);*/
																				/*struct macro* temp = search(head,$1);
																				char* statements = malloc(strlen(temp->statements));
																				strcpy(statements,temp->statements);
																				
																				char* arguments = convertToString($3);
																				
																				strcpy(statements,temp->statements);
																				
																				char* string = replace(statements,temp->args,arguments);
																				int n = strlen(string);
																				char* temp_string = malloc(n+1);
																				strcpy(temp_string,string);
																				temp_string[n] = '\0';
																				struct node* temp1 = (struct node*)calloc(1, sizeof(struct node)) ;
																				
																				temp1->string = temp_string;*/
																				
																				struct macro* temp = search(head,(yyvsp[(1) - (5)].string));
																				
																				char* arguments = convertToString((yyvsp[(3) - (5)].node));
																				
																				struct node* string = replace(temp->statements,temp->args,arguments);
																																								(yyval.node) = NULL;
																				(yyval.node) = NULL;
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(2);
																				strcpy(temp1->string,"{");
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				(yyval.node) = LinkingLists((yyval.node),string);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(2);
																				strcpy(temp2->string,"}");
																				(yyval.node) = ListInsert((yyval.node),temp2);
																				//print($$);
																				//$$ = LinkingLists($$,temp1);
																				

																				
																			}
    break;

  case 31:

/* Line 1806 of yacc.c  */
#line 706 "P1.y"
    {
																				struct node* temp = malloc(sizeof(struct node));
																				temp->string = malloc(strlen((yyvsp[(2) - (3)].string))+1);
																				strcpy(temp->string,(yyvsp[(2) - (3)].string));
																				(yyval.node) = NULL;
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(1) - (3)].node));
																				(yyval.node) = ListInsert((yyval.node),temp);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(3) - (3)].node));

																			}
    break;

  case 32:

/* Line 1806 of yacc.c  */
#line 716 "P1.y"
    {
																				struct node* temp = malloc(sizeof(struct node));
																				temp->string = malloc(strlen((yyvsp[(2) - (3)].string))+1);
																				strcpy(temp->string,(yyvsp[(2) - (3)].string));
																				(yyval.node) = NULL;
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(1) - (3)].node));
																				(yyval.node) = ListInsert((yyval.node),temp);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(3) - (3)].node));


																			}
    break;

  case 33:

/* Line 1806 of yacc.c  */
#line 727 "P1.y"
    {
																				struct node* temp = malloc(sizeof(struct node));
																				temp->string = malloc(strlen((yyvsp[(2) - (3)].string))+1);
																				strcpy(temp->string,(yyvsp[(2) - (3)].string));
																				(yyval.node) = NULL;
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(1) - (3)].node));
																				(yyval.node) = ListInsert((yyval.node),temp);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(3) - (3)].node));

																			}
    break;

  case 34:

/* Line 1806 of yacc.c  */
#line 737 "P1.y"
    {
																				struct node* temp = malloc(sizeof(struct node));
																				temp->string = malloc(strlen((yyvsp[(2) - (3)].string))+1);
																				strcpy(temp->string,(yyvsp[(2) - (3)].string));
																				(yyval.node) = NULL;
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(1) - (3)].node));
																				(yyval.node) = ListInsert((yyval.node),temp);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(3) - (3)].node));

																			}
    break;

  case 35:

/* Line 1806 of yacc.c  */
#line 747 "P1.y"
    {
																				struct node* temp = malloc(sizeof(struct node));
																				temp->string = malloc(strlen((yyvsp[(2) - (3)].string))+1);
																				strcpy(temp->string,(yyvsp[(2) - (3)].string));
																				(yyval.node) = NULL;
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(1) - (3)].node));
																				(yyval.node) = ListInsert((yyval.node),temp);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(3) - (3)].node));

																			}
    break;

  case 36:

/* Line 1806 of yacc.c  */
#line 757 "P1.y"
    {
																				struct node* temp = malloc(sizeof(struct node));
																				temp->string = malloc(strlen((yyvsp[(2) - (3)].string))+1);
																				strcpy(temp->string,(yyvsp[(2) - (3)].string));
																				(yyval.node) = NULL;
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(1) - (3)].node));
																				(yyval.node) = ListInsert((yyval.node),temp);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(3) - (3)].node));

																			}
    break;

  case 37:

/* Line 1806 of yacc.c  */
#line 767 "P1.y"
    {
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen((yyvsp[(2) - (4)].string))+1);
																				strcpy(temp1->string,(yyvsp[(2) - (4)].string));
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen((yyvsp[(4) - (4)].string))+1);
																				strcpy(temp2->string,(yyvsp[(4) - (4)].string));
																				(yyval.node) = NULL;
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(1) - (4)].node));
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(3) - (4)].node));
																				(yyval.node) = ListInsert((yyval.node),temp2);

																			}
    break;

  case 38:

/* Line 1806 of yacc.c  */
#line 781 "P1.y"
    {
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen((yyvsp[(2) - (3)].string))+1);
																				strcpy(temp1->string,(yyvsp[(2) - (3)].string));
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen((yyvsp[(3) - (3)].string))+1);
																				strcpy(temp2->string,(yyvsp[(3) - (3)].string));
																				(yyval.node) = NULL;
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(1) - (3)].node));
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				(yyval.node) = ListInsert((yyval.node),temp2);

																			}
    break;

  case 39:

/* Line 1806 of yacc.c  */
#line 794 "P1.y"
    {
																				(yyval.node) = NULL;
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(1) - (1)].node));

																			}
    break;

  case 40:

/* Line 1806 of yacc.c  */
#line 799 "P1.y"
    {
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen((yyvsp[(2) - (6)].string))+1);
																				strcpy(temp1->string,(yyvsp[(2) - (6)].string));
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen((yyvsp[(3) - (6)].string))+1);
																				strcpy(temp2->string,(yyvsp[(3) - (6)].string));
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen((yyvsp[(4) - (6)].string))+1);
																				strcpy(temp3->string,(yyvsp[(4) - (6)].string));
																				struct node* temp4 = malloc(sizeof(struct node));
																				temp4->string = malloc(strlen((yyvsp[(6) - (6)].string))+1);
																				strcpy(temp4->string,(yyvsp[(6) - (6)].string));
																				(yyval.node) = NULL;
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(1) - (6)].node));
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				(yyval.node) = ListInsert((yyval.node),temp2);
																				(yyval.node) = ListInsert((yyval.node),temp3);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(5) - (6)].node));
																				(yyval.node) = ListInsert((yyval.node),temp4);
																				 





																			}
    break;

  case 41:

/* Line 1806 of yacc.c  */
#line 826 "P1.y"
    {
																				/*struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1));
																				strcpy(temp1->string,$1);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($2));
																				strcpy(temp2->string,$2);
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen($4));
																				strcpy(temp3->string,$4);
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);
																				$$ = ListInsert($$,temp2);
																				$$ = LinkingLists($$,$3);
																				$$ = ListInsert($$,temp3);*/
																				struct macro* temp = search(head,(yyvsp[(1) - (4)].string));
																				
																				char* arguments = convertToString((yyvsp[(3) - (4)].node));
																				
																				struct node* string = replace(temp->statements,temp->args,arguments);
																																								(yyval.node) = NULL;
																				(yyval.node) = NULL;

																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(2);
																				strcpy(temp1->string,"(");
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				(yyval.node) = LinkingLists((yyval.node),string);			
																				
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(2);
																				strcpy(temp2->string,")");
																				(yyval.node) = ListInsert((yyval.node),temp2);

																				
																			}
    break;

  case 42:

/* Line 1806 of yacc.c  */
#line 864 "P1.y"
    {
																				(yyval.node) = NULL;	
																			}
    break;

  case 43:

/* Line 1806 of yacc.c  */
#line 867 "P1.y"
    {

																				(yyval.node) = NULL;
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(1) - (2)].node));
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(2) - (2)].node));
																				
																			}
    break;

  case 44:

/* Line 1806 of yacc.c  */
#line 875 "P1.y"
    {
																				(yyval.string) = malloc(1);
																				(yyval.string)[0] = '\0';			
																			}
    break;

  case 45:

/* Line 1806 of yacc.c  */
#line 879 "P1.y"
    {
																				(yyval.string) = malloc(strlen((yyvsp[(1) - (2)].string))+strlen((yyvsp[(2) - (2)].string))+1);
																				strcat((yyval.string),(yyvsp[(1) - (2)].string));
																				strcat((yyval.string),(yyvsp[(2) - (2)].string));
																				free((yyvsp[(2) - (2)].string));
																			}
    break;

  case 46:

/* Line 1806 of yacc.c  */
#line 887 "P1.y"
    {
																				(yyval.string) = malloc(1);
																				(yyval.string)[0] = '\0';			
																			}
    break;

  case 47:

/* Line 1806 of yacc.c  */
#line 891 "P1.y"
    {
																				(yyval.string) = malloc(strlen((yyvsp[(1) - (3)].string))+strlen((yyvsp[(2) - (3)].string))+strlen((yyvsp[(3) - (3)].string))+1);
																				strcat((yyval.string),(yyvsp[(1) - (3)].string));
																				strcat((yyval.string),(yyvsp[(2) - (3)].string));
																				strcat((yyval.string),(yyvsp[(3) - (3)].string));
																				free((yyvsp[(3) - (3)].string));
																			}
    break;

  case 48:

/* Line 1806 of yacc.c  */
#line 900 "P1.y"
    {
																				(yyval.node) = NULL;	
																			}
    break;

  case 49:

/* Line 1806 of yacc.c  */
#line 903 "P1.y"
    {
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen((yyvsp[(1) - (3)].string))+1);
																				strcpy(temp1->string,(yyvsp[(1) - (3)].string));
																				(yyval.node) = NULL;
																				(yyval.node) = ListInsert((yyval.node),temp1);
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(2) - (3)].node));
																				(yyval.node) = LinkingLists((yyval.node),(yyvsp[(3) - (3)].node));

																			}
    break;

  case 50:

/* Line 1806 of yacc.c  */
#line 915 "P1.y"
    {
																		struct node* temp = malloc(sizeof(struct node));
																		temp->string = malloc(strlen((yyvsp[(1) - (1)].string))+1);
																		strcpy(temp->string,(yyvsp[(1) - (1)].string));
																		(yyval.node) = NULL;
																		(yyval.node) = ListInsert((yyval.node),temp);

																	}
    break;

  case 51:

/* Line 1806 of yacc.c  */
#line 923 "P1.y"
    {
																		struct node* temp = malloc(sizeof(struct node));
																		temp->string = malloc(strlen((yyvsp[(1) - (1)].string))+1);
																		strcpy(temp->string,(yyvsp[(1) - (1)].string));
																		(yyval.node) = NULL;
																		(yyval.node) = ListInsert((yyval.node),temp);

																	}
    break;

  case 52:

/* Line 1806 of yacc.c  */
#line 931 "P1.y"
    {
																		struct node* temp = malloc(sizeof(struct node));
																		temp->string = malloc(strlen((yyvsp[(1) - (1)].string))+1);
																		strcpy(temp->string,(yyvsp[(1) - (1)].string));
																		(yyval.node) = NULL;
																		(yyval.node) = ListInsert((yyval.node),temp);

																	}
    break;

  case 53:

/* Line 1806 of yacc.c  */
#line 939 "P1.y"
    {
																		struct node* temp = malloc(sizeof(struct node));
																		temp->string = malloc(strlen((yyvsp[(1) - (1)].string))+1);
																		strcpy(temp->string,(yyvsp[(1) - (1)].string));
																		(yyval.node) = NULL;
																		(yyval.node) = ListInsert((yyval.node),temp);

																	}
    break;

  case 54:

/* Line 1806 of yacc.c  */
#line 947 "P1.y"
    {
																		struct node* temp = malloc(sizeof(struct node));
																		temp->string = malloc(strlen((yyvsp[(1) - (1)].string))+1);
																		strcpy(temp->string,(yyvsp[(1) - (1)].string));
																		(yyval.node) = NULL;
																		(yyval.node) = ListInsert((yyval.node),temp);

																	}
    break;

  case 55:

/* Line 1806 of yacc.c  */
#line 955 "P1.y"
    {
																		struct node* temp1 = malloc(sizeof(struct node));
																		temp1->string = malloc(strlen((yyvsp[(1) - (5)].string))+1);
																		strcpy(temp1->string,(yyvsp[(1) - (5)].string));
																		struct node* temp2 = malloc(sizeof(struct node));
																		temp2->string = malloc(strlen((yyvsp[(2) - (5)].string))+1);
																		strcpy(temp2->string,(yyvsp[(2) - (5)].string));
																		struct node* temp3 = malloc(sizeof(struct node));
																		temp3->string = malloc(strlen((yyvsp[(3) - (5)].string))+1);
																		strcpy(temp3->string,(yyvsp[(3) - (5)].string));
																		struct node* temp5 = malloc(sizeof(struct node));
																		temp5->string = malloc(strlen((yyvsp[(5) - (5)].string))+1);
																		strcpy(temp5->string,(yyvsp[(5) - (5)].string));
																		(yyval.node) = NULL;
																		(yyval.node) = ListInsert((yyval.node),temp1);
																		(yyval.node) = ListInsert((yyval.node),temp2);
																		(yyval.node) = ListInsert((yyval.node),temp3);
																		(yyval.node) = LinkingLists((yyval.node),(yyvsp[(4) - (5)].node));
																		(yyval.node) = ListInsert((yyval.node),temp5);
																		

																	}
    break;

  case 56:

/* Line 1806 of yacc.c  */
#line 977 "P1.y"
    {
																		struct node* temp1 = malloc(sizeof(struct node));
																		temp1->string = malloc(strlen((yyvsp[(1) - (4)].string))+1);
																		strcpy(temp1->string,(yyvsp[(1) - (4)].string));
																		struct node* temp2 = malloc(sizeof(struct node));
																		temp2->string = malloc(strlen((yyvsp[(2) - (4)].string))+1);
																		strcpy(temp2->string,(yyvsp[(2) - (4)].string));
																		struct node* temp3 = malloc(sizeof(struct node));
																		temp3->string = malloc(strlen((yyvsp[(3) - (4)].string))+1);
																		strcpy(temp3->string,(yyvsp[(3) - (4)].string));
																		struct node* temp4 = malloc(sizeof(struct node));
																		temp4->string = malloc(strlen((yyvsp[(4) - (4)].string))+1);
																		strcpy(temp4->string,(yyvsp[(4) - (4)].string));
																		(yyval.node) = NULL;
																		(yyval.node) = ListInsert((yyval.node),temp1);
																		(yyval.node) = ListInsert((yyval.node),temp2);
																		(yyval.node) = ListInsert((yyval.node),temp3);
																		(yyval.node) = ListInsert((yyval.node),temp4);

																	}
    break;

  case 57:

/* Line 1806 of yacc.c  */
#line 997 "P1.y"
    {
																		struct node* temp1 = malloc(sizeof(struct node));
																		temp1->string = malloc(strlen((yyvsp[(1) - (2)].string))+1);
																		strcpy(temp1->string,(yyvsp[(1) - (2)].string));
																		(yyval.node) = NULL;
																		(yyval.node) = ListInsert((yyval.node),temp1);
																		(yyval.node) = LinkingLists((yyval.node),(yyvsp[(2) - (2)].node));

																	}
    break;

  case 58:

/* Line 1806 of yacc.c  */
#line 1006 "P1.y"
    {
																		struct node* temp1 = malloc(sizeof(struct node));
																		temp1->string = malloc(strlen((yyvsp[(1) - (3)].string))+1);
																		strcpy(temp1->string,(yyvsp[(1) - (3)].string));
																		struct node* temp3 = malloc(sizeof(struct node));
																		temp3->string = malloc(strlen((yyvsp[(3) - (3)].string))+1);
																		strcpy(temp3->string,(yyvsp[(3) - (3)].string));
																		
																		(yyval.node) = NULL;
																		(yyval.node) = ListInsert((yyval.node),temp1);
																		(yyval.node) = LinkingLists((yyval.node),(yyvsp[(2) - (3)].node));
																		(yyval.node) = ListInsert((yyval.node),temp3);
																		

																	}
    break;

  case 63:

/* Line 1806 of yacc.c  */
#line 1032 "P1.y"
    {
																			struct macro* x;
																			x = malloc(sizeof(struct macro));
																			x->name = malloc(strlen((yyvsp[(2) - (8)].string))+1);
																			strcpy(x->name,(yyvsp[(2) - (8)].string));
																			x->args = malloc(strlen((yyvsp[(4) - (8)].string))+1);
																			strcpy(x->args,(yyvsp[(4) - (8)].string));
																			x->statements = (yyvsp[(7) - (8)].node);
																			head = insert(head,x);	
																		}
    break;

  case 64:

/* Line 1806 of yacc.c  */
#line 1044 "P1.y"
    {
																			struct macro* x;
																			x = malloc(sizeof(struct macro));
																			x->name = malloc(strlen((yyvsp[(2) - (8)].string))+1);
																			strcpy(x->name,(yyvsp[(2) - (8)].string));
																			x->args = malloc(strlen((yyvsp[(4) - (8)].string))+1);
																			strcpy(x->args,(yyvsp[(4) - (8)].string));
																			x->statements = (yyvsp[(7) - (8)].node);
																			head = insert(head,x);	
																		}
    break;



/* Line 1806 of yacc.c  */
#line 2811 "P1.tab.c"
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 2067 of yacc.c  */
#line 1055 "P1.y"


main (int argc, char **argv) {
  yyparse();
  
}

yyerror (char *s) {
  fprintf(stderr, "error: %s\n", s);
}

