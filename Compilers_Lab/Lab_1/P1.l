%{
#include "P1.tab.h"
#include <string.h>

%}



%%

"//"[^\n]*					{;/* Pattern for eating up one-line comments*/}	
"/*"[^*/]*					{;}

[ \t\n]						{;} 

"class "						{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return CLASS;}
"public "					{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return PUBLIC;}
"static "					{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return STATIC;}
"void "						{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return VOID;}
"main"						{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return MAIN;}
"String"					{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return STRING;}
"System"					{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return SYSTEM;}
"out"						{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return OUT;}
"println"					{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return PRINTLN;}
"extends"					{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return EXTENDS;}
"return"					{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return RETURN;}
"int"						{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return INT;}
"boolean"					{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return BOOLEAN;}
"if"						{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return IF;}
"else"						{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return ELSE;}
"while"						{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return WHILE;}
"length"					{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return LENGTH;}
"true"						{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return TRUE;}
"false"						{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return FALSE;}
"new "						{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return NEW;}
"#"[ \t\n]*"define"			{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return MACRO;}
"this"						{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return THIS;}

"."							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return DOT;}
";"							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return SC;}
","							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return COMMA;}
"{"							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return LB;}
"}"							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return RB;}
"("							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return LP;}
")"							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return RP;}
"["							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return LSB;}
"]"							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return RSB;}

"="							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return EQUAL;}
"&"							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return AND;}
"<"							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return LESSTHAN;}
"+"							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return PLUS;}
"-"							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return MINUS;}
"*"							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return STAR;}
"/"							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return DIV;}
"!"							{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return NOT;}

[_a-zA-Z$][a-zA-Z_0-9$]*		{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return ID;}
[0-9]+						{yylval.string = malloc(strlen(yytext)+1);
							 strncpy(yylval.string,yytext,strlen(yytext));
							 return NUM;}	




%%