#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "LinkedList.h"

struct macro
{
	char* name;
	char* args;
	struct node* statements;
	struct macro* next;
};

struct macro* insert(struct macro* head,struct macro* x) {
	struct macro* temp = head;
	if(temp==NULL) {
		head = x;
		head->next = NULL;
		return head;
	}
	while(temp->next != NULL) {
		temp = temp->next;
	}
	temp->next = x;
	x->next = NULL;
	return head;
}

struct macro* search(struct macro* head,char* idName) {
	struct macro* temp = head;
	while(temp != NULL) {
		if(strcmp(temp->name,idName)==0) {
			return temp;
		}
		temp = temp->next;
	}
	return temp;
}