#include <stdio.h>
#include <string.h>
#include <stdlib.h>
struct node
{
	char* string;
	struct node* next;
};

struct LinkedList {
	struct node* head;
};
struct macro
{
	char* name;
	char* args;
	struct node* statements;
	struct macro* next;
};

struct node* ListInsert(struct node* head,struct node* x) {
	struct node* temp = head;
	if(temp==NULL) {
		head = x;
		head->next = NULL;
		return head;
	}
	while(temp->next != NULL) {
		temp = temp->next;
	}
	temp->next = x;
	x->next = NULL;
	return head;
}

struct node* LinkingLists(struct node* head1,struct node* head2) {
	struct node* temp = head1;
	if(temp==NULL) {
		head1 = head2;
		return head1;
	}
	while(temp->next != NULL) {
		temp = temp->next;
	}
	temp->next = head2;
	return head1;	
}

void print(struct node* head) {
	struct node* temp = head;
	while(temp != NULL) {
		printf("%s ",temp->string);
		temp = temp->next;
	}
}
struct macro* insert(struct macro* head,struct macro* x) {
	struct macro* temp = head;
	if(temp==NULL) {
		head = x;
		return head;
	}
	while(temp->next != NULL) {
		temp = temp->next;
	}
	temp->next = x;
	x->next = NULL;
	return head;
}

struct macro* search(struct macro* head,char* idName) {
	struct macro* temp = head;
	while(temp != NULL) {
		if(strcmp(temp->name,idName)==0) {
			return temp;
		}
		temp = temp->next;
	}
	return temp;
}

char* convertToString(struct node* head) {
	struct node* temp = head;
	int length = 0;
	
	while(temp!=NULL) {
		length = length + strlen(temp->string);
		temp = temp->next;
	}
	temp = head;
	char* ret = malloc(length);
	while(temp!=NULL) {
		strcat(ret,temp->string);
		temp = temp->next;
	}
	return ret;
}
/*char* foo(char* statements,struct node* f) {
	statements = convertToString(f);
	return statements;
}*/
int compare(char* s1, char* s2) {
	int i = 0;
	if(strlen(s1)!=strlen(s2)) {
		return 0;
	}
	else {
		for(i=0;i<strlen(s1);i++) {
			if(s1[i] != s2[i]) {
				return 0;
			}
		}
		return 1;
	}
}
struct node* copy(struct node* destination,struct node* source) {
	destination = NULL;
	struct node* temp = source;
	while(temp!= NULL) {
		struct node* temp1 = malloc(sizeof(struct node));
		temp1->string = malloc(strlen(temp->string)+1);
		strcpy(temp1->string,temp->string);
		destination = ListInsert(destination,temp1);
		temp = temp->next;
	}
	return destination;
}

struct node* parseAndReplace(struct node* statements,char* toBeReplaced,char* toReplace) {
	int i = 0;
	struct node* temp = statements;
	while(temp!=NULL) {
		if(strcmp(temp->string,toBeReplaced)==0) {
			temp->string = malloc(strlen(toReplace) + 1);
			strcpy(temp->string,toReplace);
		}
		temp = temp->next;
	}
	
	return statements;
}

struct node* replace(struct node* statements,char* args,char* arguments) {
	int i =0;
	int j = 0;
	int count = 0;
	struct node* string;
	string = copy(string,statements);
	char toReplace[100];
	char toBeReplaced[100];
	while(arguments[i] != '\0') {
		toReplace[0] = '\0';
		toBeReplaced[0] = '\0';
		count = 0; 
		while(arguments[i] != '\0'&& arguments[i] != ',') {
			count++;
			i++;
		}
		i = i - count;
		count = 0;
		while(arguments[i] != '\0'&& arguments[i] != ',') {
			toReplace[count] = arguments[i];
			count++;
			i++;
		}
		toReplace[count] = '\0';
		i++;
		count = 0;
		while(args[j] != '\0' && args[j] != ',') {
				count++;
				j++;	
		}
		j = j - count;
		count = 0;
		while(args[j] != '\0' && args[j] != ',') {
				toBeReplaced[count] = args[j];
				count++;
				j++;	
		}
		toBeReplaced[count] = '\0';
		j++;
		string = parseAndReplace(string,toBeReplaced,toReplace);
	}
	return string;
}


