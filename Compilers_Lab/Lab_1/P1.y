%{
#include <stdio.h>	
#include <string.h>
#include <stdlib.h>
#include "LinkedList.h"

struct macro* head = NULL;
%}

%union{
	char* string;
	struct node* node;
}


%token <string> ID 
%token <string> NUM
%token <string> CLASS
%token <string> PUBLIC
%token <string> STATIC
%token <string> VOID
%token <string> MAIN 
%token <string> STRING
%token <string> SYSTEM
%token <string> OUT
%token <string> PRINTLN
%token <string> EXTENDS
%token <string> RETURN
%token <string> INT
%token <string> BOOLEAN
%token <string> IF
%token <string> ELSE
%token <string> WHILE
%token <string> LENGTH
%token <string> TRUE
%token <string> FALSE
%token <string> NEW
%token <string> MACRO
%token <string> THIS
%token <string> DOT
%token <string> SC
%token <string> LP
%token <string> RP
%token <string> LB
%token <string> RB
%token <string> LSB
%token <string> RSB
%token <string> COMMA
%token <string> EQUAL
%token <string> AND
%token <string> PLUS
%token <string> MINUS
%token <string> STAR
%token <string> DIV
%token <string> LESSTHAN
%token <string> NOT




%type <node> goal
%type <node> mainClass
%type <node> typeDeclaration_star
%type <node> typeDeclaration
%type <node> methodDeclaration_star
%type <node> methodDeclaration
%type <node> arg_TI
%type <node> commaTypeIdentifier_star
%type <node> typeIdentifierSC_star
%type <node> type
%type <node> statement_star
%type <node> statement
%type <node> expression
%type <node> arg_E
%type <string> arg_I
%type <string> commaID_star
%type <node> commaExpression_star
%type <node> primaryExpression
%type <string> macroDefinition
%type <string> macroDefinition_star
%type <string> macroDefExpression
%type <string> macroDefStatement





%left PLUS MINUS
%left STAR DIV
%left NOT

%%

goal :	macroDefinition_star mainClass typeDeclaration_star			{
																		$$ = NULL;
																		$$ = LinkingLists($$,$2);
																		$$ = LinkingLists($$,$3);
																		print($$);
																		
																	}			
	 ;

mainClass 				:	CLASS ID LB PUBLIC STATIC VOID MAIN LP STRING LSB RSB ID RP LB SYSTEM DOT OUT DOT PRINTLN LP expression RP  SC RB RB  		{
																				
																																							struct node* temp1 = malloc(sizeof(struct node));
																																							temp1->string = malloc(strlen($1)+1);
																																							strcpy(temp1->string,$1);
																																							struct node* temp2 = malloc(sizeof(struct node));
																																							temp2->string = malloc(strlen($2)+1);
																																							strcpy(temp2->string,$2);
																																							struct node* temp3 = malloc(sizeof(struct node));
																																							temp3->string = malloc(strlen($3)+1);
																																							strcpy(temp3->string,$3);
																																							struct node* temp4 = malloc(sizeof(struct node));
																																							temp4->string = malloc(strlen($4)+1);
																																							strcpy(temp4->string,$4);
																																							struct node* temp5 = malloc(sizeof(struct node));
																																							temp5->string = malloc(strlen($5)+1);
																																							strcpy(temp5->string,$5);
																																							struct node* temp6 = malloc(sizeof(struct node));
																																							temp6->string = malloc(strlen($6)+1);
																																							strcpy(temp6->string,$6);
																																							struct node* temp7 = malloc(sizeof(struct node));
																																							temp7->string = malloc(strlen($7)+1);
																																							strcpy(temp7->string,$7);
																																							struct node* temp8 = malloc(sizeof(struct node));
																																							temp8->string = malloc(strlen($8)+1);
																																							strcpy(temp8->string,$8);
																																							struct node* temp9 = malloc(sizeof(struct node));
																																							temp9->string = malloc(strlen($9)+1);
																																							strcpy(temp9->string,$9);
																																							struct node* temp10 = malloc(sizeof(struct node));
																																							temp10->string = malloc(strlen($10)+1);
																																							strcpy(temp10->string,$10);
																																							struct node* temp11 = malloc(sizeof(struct node));
																																							temp11->string = malloc(strlen($11)+1);
																																							strcpy(temp11->string,$11);
																																							struct node* temp12 = malloc(sizeof(struct node));
																																							temp12->string = malloc(strlen($12)+1);
																																							strcpy(temp12->string,$12);
																																							struct node* temp13 = malloc(sizeof(struct node));
																																							temp13->string = malloc(strlen($13)+1);
																																							strcpy(temp13->string,$13);
																																							struct node* temp14 = malloc(sizeof(struct node));
																																							temp14->string = malloc(strlen($14)+1);
																																							strcpy(temp14->string,$14);
																																							struct node* temp15 = malloc(sizeof(struct node));
																																							temp15->string = malloc(strlen($15)+1);
																																							strcpy(temp15->string,$15);
																																							struct node* temp16 = malloc(sizeof(struct node));
																																							temp16->string = malloc(strlen($16)+1);
																																							strcpy(temp16->string,$16);
																																							struct node* temp17 = malloc(sizeof(struct node));
																																							temp17->string = malloc(strlen($17)+1);
																																							strcpy(temp17->string,$17);
																																							struct node* temp18 = malloc(sizeof(struct node));
																																							temp18->string = malloc(strlen($18)+1);
																																							strcpy(temp18->string,$18);
																																							struct node* temp19 = malloc(sizeof(struct node));
																																							temp19->string = malloc(strlen($19)+1);
																																							strcpy(temp19->string,$19);
																																							struct node* temp20 = malloc(sizeof(struct node));
																																							temp20->string = malloc(strlen($20)+1);
																																							strcpy(temp20->string,$20);
																																							struct node* temp22 = malloc(sizeof(struct node));
																																							temp22->string = malloc(strlen($22)+1);
																																							strcpy(temp22->string,$22);
																																							struct node* temp23 = malloc(sizeof(struct node));
																																							temp23->string = malloc(strlen($23)+1);
																																							strcpy(temp23->string,$23);
																																							struct node* temp24 = malloc(sizeof(struct node));
																																							temp24->string = malloc(strlen($24)+1);
																																							strcpy(temp24->string,$24);
																																							struct node* temp25 = malloc(sizeof(struct node));
																																							temp25->string = malloc(strlen($25)+1);
																																							strcpy(temp25->string,$25);
																																							$$ = NULL;
																																							$$ = ListInsert($$,temp1);
																																							$$ = ListInsert($$,temp2);
																																							$$ = ListInsert($$,temp3);
																																							$$ = ListInsert($$,temp4);
																																							$$ = ListInsert($$,temp5);
																																							$$ = ListInsert($$,temp6);
																																							$$ = ListInsert($$,temp7);
																																							$$ = ListInsert($$,temp8);
																																							$$ = ListInsert($$,temp9);
																																							$$ = ListInsert($$,temp10);
																																							$$ = ListInsert($$,temp11);
																																							$$ = ListInsert($$,temp12);
																																							$$ = ListInsert($$,temp13);
																																							$$ = ListInsert($$,temp14);
																																							$$ = ListInsert($$,temp15);
																																							$$ = ListInsert($$,temp16);
																																							$$ = ListInsert($$,temp17);
																																							$$ = ListInsert($$,temp18);
																																							$$ = ListInsert($$,temp19);
																																							$$ = ListInsert($$,temp20);
																																							$$ = LinkingLists($$,$21);
																																							$$ = ListInsert($$,temp22);
																																							$$ = ListInsert($$,temp23);
																																							$$ = ListInsert($$,temp24);
																																							$$ = ListInsert($$,temp25);
																																							
																																							
																																							

																																						}
													
																																																								
						;

typeDeclaration_star	: 																							{
																														$$ = NULL;
																													}																	

						| typeDeclaration typeDeclaration_star    													{
																														$$ = NULL;
																														$$ = LinkingLists($$,$1);
																														$$ = LinkingLists($$,$2);
																													}	
						;

typeDeclaration 		: CLASS ID LB typeIdentifierSC_star methodDeclaration_star RB    							{
																				
																															struct node* temp1 = malloc(sizeof(struct node));
																															temp1->string = malloc(strlen($1)+1);
																															strcpy(temp1->string,$1);
																															struct node* temp2 = malloc(sizeof(struct node));
																															temp2->string = malloc(strlen($2)+1);
																															strcpy(temp2->string,$2);
																															struct node* temp3 = malloc(sizeof(struct node));
																															temp3->string = malloc(strlen($3)+1);
																															strcpy(temp3->string,$3);
																															struct node* temp4 = malloc(sizeof(struct node));
																															temp4->string = malloc(strlen($6)+1);
																															strcpy(temp4->string,$6);
																															
																															$$ = NULL;
																															$$ = ListInsert($$,temp1);
																															$$ = ListInsert($$,temp2);
																															$$ = ListInsert($$,temp3);
																															$$ = LinkingLists($$,$4);
																															$$ = LinkingLists($$,$5);
																															$$ = ListInsert($$,temp4);
																															
																															

																														}					
						| CLASS ID EXTENDS ID LB typeIdentifierSC_star methodDeclaration_star RB 						{
																				
																															struct node* temp1 = malloc(sizeof(struct node));
																															temp1->string = malloc(strlen($1)+1);
																															strcpy(temp1->string,$1);
																															struct node* temp2 = malloc(sizeof(struct node));
																															temp2->string = malloc(strlen($2)+1);
																															strcpy(temp2->string,$2);
																															struct node* temp3 = malloc(sizeof(struct node));
																															temp3->string = malloc(strlen($3)+1);
																															strcpy(temp3->string,$3);
																															struct node* temp4 = malloc(sizeof(struct node));
																															temp4->string = malloc(strlen($4)+1);
																															strcpy(temp4->string,$4);
																															struct node* temp5 = malloc(sizeof(struct node));
																															temp5->string = malloc(strlen($5)+1);
																															strcpy(temp5->string,$5);
																															struct node* temp6 = malloc(sizeof(struct node));
																															temp6->string = malloc(strlen($8)+1);
																															strcpy(temp6->string,$8);
																															
																															$$ = NULL;
																															$$ = ListInsert($$,temp1);
																															$$ = ListInsert($$,temp2);
																															$$ = ListInsert($$,temp3);
																															$$ = ListInsert($$,temp4);
																															$$ = ListInsert($$,temp5);
																															$$ = LinkingLists($$,$6);
																															$$ = LinkingLists($$,$7);
																															$$ = ListInsert($$,temp6);
																															
																															

																														}
						;


methodDeclaration_star  :																								{
																															$$ = NULL;
																														}
						| methodDeclaration methodDeclaration_star 														{
																															$$ = NULL;
																															$$ = LinkingLists($$,$1);
																															$$ = LinkingLists($$,$2);
																														}
						;

methodDeclaration 		: PUBLIC type ID LP arg_TI RP LB typeIdentifierSC_star statement_star RETURN expression SC RB   {
																				
																															struct node* temp1 = malloc(sizeof(struct node));
																															temp1->string = malloc(strlen($1)+1);
																															strcpy(temp1->string,$1);
																															struct node* temp2 = malloc(sizeof(struct node));
																															temp2->string = malloc(strlen($3)+1);
																															strcpy(temp2->string,$3);
																															struct node* temp3 = malloc(sizeof(struct node));
																															temp3->string = malloc(strlen($4)+1);
																															strcpy(temp3->string,$4);
																															struct node* temp4 = malloc(sizeof(struct node));
																															temp4->string = malloc(strlen($6)+1);
																															strcpy(temp4->string,$6);
																															struct node* temp5 = malloc(sizeof(struct node));
																															temp5->string = malloc(strlen($7)+1);
																															strcpy(temp5->string,$7);
																															struct node* temp6 = malloc(sizeof(struct node));
																															temp6->string = malloc(strlen($10)+1);
																															strcpy(temp6->string,$10);
																															struct node* temp7 = malloc(sizeof(struct node));
																															temp7->string = malloc(strlen($12)+1);
																															strcpy(temp7->string,$12);
																															struct node* temp8 = malloc(sizeof(struct node));
																															temp8->string = malloc(strlen($13)+1);
																															strcpy(temp8->string,$13);
																															$$ = NULL;
																															$$ = ListInsert($$,temp1);
																															$$ = LinkingLists($$,$2);
																															$$ = ListInsert($$,temp2);
																															$$ = ListInsert($$,temp3);
																															$$ = LinkingLists($$,$5);
																															$$ = ListInsert($$,temp4);
																															$$ = ListInsert($$,temp5);
																															$$ = LinkingLists($$,$8);
																															$$ = LinkingLists($$,$9);
																															$$ = ListInsert($$,temp6);
																															$$ = LinkingLists($$,$11);
																															$$ = ListInsert($$,temp7);
																															$$ = ListInsert($$,temp8);
																															

																														}	
						;

arg_TI 					: 													{
																				$$ = NULL;	
																			}														
						| type ID commaTypeIdentifier_star 					{
																				
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($2)+1);
																				strcpy(temp2->string,$2);
																				
																				$$ = NULL;
																				$$ = LinkingLists($$,$1);
																				
																				$$ = ListInsert($$,temp2);
																				$$ = LinkingLists($$,$3);
																				
																			}
						;

commaTypeIdentifier_star:													{
																				$$ = NULL;	
																			}	
						| COMMA type ID commaTypeIdentifier_star         	{
																				
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1)+1);
																				strcpy(temp1->string,$1);
																				
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($3)+1);
																				strcpy(temp2->string,$3);
																				
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);

																				$$ = LinkingLists($$,$2);
																				$$ = ListInsert($$,temp2);
																				
																				$$ = LinkingLists($$,$4);
																				
																			}	
						;

typeIdentifierSC_star	:													{
																				$$ = NULL;	
																			}	
						| typeIdentifierSC_star type ID SC   				{
																				
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($3)+1);
																				strcpy(temp2->string,$3);
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen($4)+1);
																				strcpy(temp3->string,$4);
																				$$ = NULL;
																				$$ = LinkingLists($$,$1);
																				$$ = LinkingLists($$,$2);
																				$$ = ListInsert($$,temp2);
																				$$ = ListInsert($$,temp3);
																				
																			}	
						;

type 					: INT LSB RSB										{
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1)+1);
																				strcpy(temp1->string,$1);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($2)+1);
																				strcpy(temp2->string,$2);
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen($3)+1);
																				strcpy(temp3->string,$3);
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);
																				$$ = ListInsert($$,temp2);
																				$$ = ListInsert($$,temp3);
																			}	
						| BOOLEAN											{
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1)+1);
																				strcpy(temp1->string,$1);
																				
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);
																				
																			}
						| INT 												{
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1)+1);
																				strcpy(temp1->string,$1);
																				
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);
																				
																			}
						| ID 												{
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1)+1);
																				strcpy(temp1->string,$1);
																				
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);
																				
																			}
						;

statement_star			: 														{
																					$$ = NULL;	
																				}				
						| statement statement_star  							{
																					$$ = NULL;
																					$$ = LinkingLists($$,$1);
																					$$ = LinkingLists($$,$2);
																					
																				}		
						;

statement 				: SYSTEM DOT OUT DOT PRINTLN LP expression RP SC 		{
																				
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1)+1);
																				strcpy(temp1->string,$1);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($2)+1);
																				strcpy(temp2->string,$2);
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen($3)+1);
																				strcpy(temp3->string,$3);
																				struct node* temp4 = malloc(sizeof(struct node));
																				temp4->string = malloc(strlen($4)+1);
																				strcpy(temp4->string,$4);
																				struct node* temp5 = malloc(sizeof(struct node));
																				temp5->string = malloc(strlen($5)+1);
																				strcpy(temp5->string,$5);
																				struct node* temp6 = malloc(sizeof(struct node));
																				temp6->string = malloc(strlen($6)+1);
																				strcpy(temp6->string,$6);
																				struct node* temp7 = malloc(sizeof(struct node));
																				temp7->string = malloc(strlen($8)+1);
																				strcpy(temp7->string,$8);
																				struct node* temp8 = malloc(sizeof(struct node));
																				temp8->string = malloc(strlen($9)+1);
																				strcpy(temp8->string,$9);
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);
																				$$ = ListInsert($$,temp2);
																				$$ = ListInsert($$,temp3);
																				$$ = ListInsert($$,temp4);
																				$$ = ListInsert($$,temp5);
																				$$ = ListInsert($$,temp6);
																				$$ = LinkingLists($$,$7);
																				$$ = ListInsert($$,temp7);
																				$$ = ListInsert($$,temp8);
																				
																				 





																			}
						| ID EQUAL expression SC 							{
																				
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1)+1);
																				strcpy(temp1->string,$1);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($2)+1);
																				strcpy(temp2->string,$2);
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen($4)+1);
																				strcpy(temp3->string,$4);
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);
																				$$ = ListInsert($$,temp2);
																				$$ = LinkingLists($$,$3);
																				$$ = ListInsert($$,temp3);
																				
																			
																			}
						| ID LSB expression RSB EQUAL expression SC  		{
																				
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1)+1);
																				strcpy(temp1->string,$1);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($2)+1);
																				strcpy(temp2->string,$2);
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen($4)+1);
																				strcpy(temp3->string,$4);
																				struct node* temp4 = malloc(sizeof(struct node));
																				temp4->string = malloc(strlen($5)+1);
																				strcpy(temp4->string,$5);
																				struct node* temp5 = malloc(sizeof(struct node));
																				temp5->string = malloc(strlen($7));
																				strcpy(temp5->string,$7);
																				
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);
																				$$ = ListInsert($$,temp2);
																				$$ = LinkingLists($$,$3);
																				$$ = ListInsert($$,temp3);
																				$$ = ListInsert($$,temp4);
																				$$ = LinkingLists($$,$6); 
																				$$ = ListInsert($$,temp5);
																				
																				
																			}	
						| IF LP expression RP statement  					{
																				
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1)+1);
																				strcpy(temp1->string,$1);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($2)+1);
																				strcpy(temp2->string,$2);
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen($4)+1);
																				strcpy(temp3->string,$4);
																				
																				
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);
																				$$ = ListInsert($$,temp2);
																				$$ = LinkingLists($$,$3);
																				$$ = ListInsert($$,temp3);
																				$$ = LinkingLists($$,$5);
																				
																				
																			}	
						| LB statement_star RB 								{
																				
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1)+1);
																				strcpy(temp1->string,$1);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($3)+1);
																				strcpy(temp2->string,$3);
																				
																				
																				
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);
																				$$ = LinkingLists($$,$2);
																				$$ = ListInsert($$,temp2);
																				
																			
																				
																				
																			}	
						| IF LP expression RP statement ELSE statement 		{
																				
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1)+1);
																				strcpy(temp1->string,$1);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($2)+1);
																				strcpy(temp2->string,$2);
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen($4)+1);
																				strcpy(temp3->string,$4);
																				struct node* temp4 = malloc(sizeof(struct node));
																				temp4->string = malloc(strlen($6)+1);
																				strcpy(temp4->string,$6);
																				
																				
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);
																				$$ = ListInsert($$,temp2);
																				$$ = LinkingLists($$,$3);
																				$$ = ListInsert($$,temp3);
																				$$ = LinkingLists($$,$5);
																				$$ = ListInsert($$,temp4);
																				$$ = LinkingLists($$,$7); 
																				
																				
																			}	
						| WHILE LP expression RP statement 					{
																				
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1)+1);
																				strcpy(temp1->string,$1);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($2)+1);
																				strcpy(temp2->string,$2);
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen($4)+1);
																				strcpy(temp3->string,$4);
																				
																				
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);
																				$$ = ListInsert($$,temp2);
																				$$ = LinkingLists($$,$3);
																				$$ = ListInsert($$,temp3);
																				$$ = LinkingLists($$,$5);
																				
																				
																			}	
						| ID LP arg_E RP SC 								{
																				
																				/*struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1));
																				strcpy(temp1->string,$1);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($2));
																				strcpy(temp2->string,$2);
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen($4));
																				strcpy(temp3->string,$4);
																				struct node* temp4 = malloc(sizeof(struct node));
																				temp4->string = malloc(strlen($5));
																				strcpy(temp4->string,$5);
																				
																				
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);
																				$$ = ListInsert($$,temp2);
																				$$ = LinkingLists($$,$3);
																				$$ = ListInsert($$,temp3);
																				$$ = LinkingLists($$,temp4);*/
																				/*struct macro* temp = search(head,$1);
																				char* statements = malloc(strlen(temp->statements));
																				strcpy(statements,temp->statements);
																				
																				char* arguments = convertToString($3);
																				
																				strcpy(statements,temp->statements);
																				
																				char* string = replace(statements,temp->args,arguments);
																				int n = strlen(string);
																				char* temp_string = malloc(n+1);
																				strcpy(temp_string,string);
																				temp_string[n] = '\0';
																				struct node* temp1 = (struct node*)calloc(1, sizeof(struct node)) ;
																				
																				temp1->string = temp_string;*/
																				
																				struct macro* temp = search(head,$1);
																				
																				char* arguments = convertToString($3);
																				
																				struct node* string = replace(temp->statements,temp->args,arguments);
																																								$$ = NULL;
																				$$ = NULL;
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(2);
																				strcpy(temp1->string,"{");
																				$$ = ListInsert($$,temp1);
																				$$ = LinkingLists($$,string);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(2);
																				strcpy(temp2->string,"}");
																				$$ = ListInsert($$,temp2);
																				//print($$);
																				//$$ = LinkingLists($$,temp1);
																				

																				
																			}	
						;

						

expression				: primaryExpression AND primaryExpression			{
																				struct node* temp = malloc(sizeof(struct node));
																				temp->string = malloc(strlen($2)+1);
																				strcpy(temp->string,$2);
																				$$ = NULL;
																				$$ = LinkingLists($$,$1);
																				$$ = ListInsert($$,temp);
																				$$ = LinkingLists($$,$3);

																			}		
						| primaryExpression LESSTHAN primaryExpression		{
																				struct node* temp = malloc(sizeof(struct node));
																				temp->string = malloc(strlen($2)+1);
																				strcpy(temp->string,$2);
																				$$ = NULL;
																				$$ = LinkingLists($$,$1);
																				$$ = ListInsert($$,temp);
																				$$ = LinkingLists($$,$3);


																			}
						| primaryExpression PLUS primaryExpression			{
																				struct node* temp = malloc(sizeof(struct node));
																				temp->string = malloc(strlen($2)+1);
																				strcpy(temp->string,$2);
																				$$ = NULL;
																				$$ = LinkingLists($$,$1);
																				$$ = ListInsert($$,temp);
																				$$ = LinkingLists($$,$3);

																			}
						| primaryExpression MINUS primaryExpression			{
																				struct node* temp = malloc(sizeof(struct node));
																				temp->string = malloc(strlen($2)+1);
																				strcpy(temp->string,$2);
																				$$ = NULL;
																				$$ = LinkingLists($$,$1);
																				$$ = ListInsert($$,temp);
																				$$ = LinkingLists($$,$3);

																			}
						| primaryExpression STAR primaryExpression			{
																				struct node* temp = malloc(sizeof(struct node));
																				temp->string = malloc(strlen($2)+1);
																				strcpy(temp->string,$2);
																				$$ = NULL;
																				$$ = LinkingLists($$,$1);
																				$$ = ListInsert($$,temp);
																				$$ = LinkingLists($$,$3);

																			}
						| primaryExpression DIV primaryExpression			{
																				struct node* temp = malloc(sizeof(struct node));
																				temp->string = malloc(strlen($2)+1);
																				strcpy(temp->string,$2);
																				$$ = NULL;
																				$$ = LinkingLists($$,$1);
																				$$ = ListInsert($$,temp);
																				$$ = LinkingLists($$,$3);

																			}
						| primaryExpression LSB primaryExpression RSB		{
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($2)+1);
																				strcpy(temp1->string,$2);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($4)+1);
																				strcpy(temp2->string,$4);
																				$$ = NULL;
																				$$ = LinkingLists($$,$1);
																				$$ = ListInsert($$,temp1);
																				$$ = LinkingLists($$,$3);
																				$$ = ListInsert($$,temp2);

																			}
						| primaryExpression DOT LENGTH						{
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($2)+1);
																				strcpy(temp1->string,$2);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($3)+1);
																				strcpy(temp2->string,$3);
																				$$ = NULL;
																				$$ = LinkingLists($$,$1);
																				$$ = ListInsert($$,temp1);
																				$$ = ListInsert($$,temp2);

																			}
						| primaryExpression									{
																				$$ = NULL;
																				$$ = LinkingLists($$,$1);

																			}
						| primaryExpression DOT ID LP arg_E RP				{
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($2)+1);
																				strcpy(temp1->string,$2);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($3)+1);
																				strcpy(temp2->string,$3);
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen($4)+1);
																				strcpy(temp3->string,$4);
																				struct node* temp4 = malloc(sizeof(struct node));
																				temp4->string = malloc(strlen($6)+1);
																				strcpy(temp4->string,$6);
																				$$ = NULL;
																				$$ = LinkingLists($$,$1);
																				$$ = ListInsert($$,temp1);
																				$$ = ListInsert($$,temp2);
																				$$ = ListInsert($$,temp3);
																				$$ = LinkingLists($$,$5);
																				$$ = ListInsert($$,temp4);
																				 





																			}
						| ID LP arg_E RP									{
																				/*struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1));
																				strcpy(temp1->string,$1);
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(strlen($2));
																				strcpy(temp2->string,$2);
																				struct node* temp3 = malloc(sizeof(struct node));
																				temp3->string = malloc(strlen($4));
																				strcpy(temp3->string,$4);
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);
																				$$ = ListInsert($$,temp2);
																				$$ = LinkingLists($$,$3);
																				$$ = ListInsert($$,temp3);*/
																				struct macro* temp = search(head,$1);
																				
																				char* arguments = convertToString($3);
																				
																				struct node* string = replace(temp->statements,temp->args,arguments);
																																								$$ = NULL;
																				$$ = NULL;

																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(2);
																				strcpy(temp1->string,"(");
																				$$ = ListInsert($$,temp1);
																				$$ = LinkingLists($$,string);			
																				
																				struct node* temp2 = malloc(sizeof(struct node));
																				temp2->string = malloc(2);
																				strcpy(temp2->string,")");
																				$$ = ListInsert($$,temp2);

																				
																			}
						;

arg_E    				:													{
																				$$ = NULL;	
																			}
						| expression commaExpression_star					{

																				$$ = NULL;
																				$$ = LinkingLists($$,$1);
																				$$ = LinkingLists($$,$2);
																				
																			}
						;
arg_I    				:													{
																				$$ = malloc(1);
																				$$[0] = '\0';			
																			}							
						| ID commaID_star 									{
																				$$ = malloc(strlen($1)+strlen($2)+1);
																				strcat($$,$1);
																				strcat($$,$2);
																				free($2);
																			}
						;

commaID_star			:													{
																				$$ = malloc(1);
																				$$[0] = '\0';			
																			}
						| COMMA ID commaID_star 							{
																				$$ = malloc(strlen($1)+strlen($2)+strlen($3)+1);
																				strcat($$,$1);
																				strcat($$,$2);
																				strcat($$,$3);
																				free($3);
																			}			
						;

commaExpression_star    :													{
																				$$ = NULL;	
																			}
						| COMMA expression commaExpression_star 		 	{
																				struct node* temp1 = malloc(sizeof(struct node));
																				temp1->string = malloc(strlen($1)+1);
																				strcpy(temp1->string,$1);
																				$$ = NULL;
																				$$ = ListInsert($$,temp1);
																				$$ = LinkingLists($$,$2);
																				$$ = LinkingLists($$,$3);

																			}
						;

primaryExpression		: NUM 										{
																		struct node* temp = malloc(sizeof(struct node));
																		temp->string = malloc(strlen($1)+1);
																		strcpy(temp->string,$1);
																		$$ = NULL;
																		$$ = ListInsert($$,temp);

																	}			
						| TRUE          							{
																		struct node* temp = malloc(sizeof(struct node));
																		temp->string = malloc(strlen($1)+1);
																		strcpy(temp->string,$1);
																		$$ = NULL;
																		$$ = ListInsert($$,temp);

																	}
						| FALSE										{
																		struct node* temp = malloc(sizeof(struct node));
																		temp->string = malloc(strlen($1)+1);
																		strcpy(temp->string,$1);
																		$$ = NULL;
																		$$ = ListInsert($$,temp);

																	}
						| ID 										{
																		struct node* temp = malloc(sizeof(struct node));
																		temp->string = malloc(strlen($1)+1);
																		strcpy(temp->string,$1);
																		$$ = NULL;
																		$$ = ListInsert($$,temp);

																	}
						| THIS										{
																		struct node* temp = malloc(sizeof(struct node));
																		temp->string = malloc(strlen($1)+1);
																		strcpy(temp->string,$1);
																		$$ = NULL;
																		$$ = ListInsert($$,temp);

																	}
						| NEW INT LSB expression RSB				{
																		struct node* temp1 = malloc(sizeof(struct node));
																		temp1->string = malloc(strlen($1)+1);
																		strcpy(temp1->string,$1);
																		struct node* temp2 = malloc(sizeof(struct node));
																		temp2->string = malloc(strlen($2)+1);
																		strcpy(temp2->string,$2);
																		struct node* temp3 = malloc(sizeof(struct node));
																		temp3->string = malloc(strlen($3)+1);
																		strcpy(temp3->string,$3);
																		struct node* temp5 = malloc(sizeof(struct node));
																		temp5->string = malloc(strlen($5)+1);
																		strcpy(temp5->string,$5);
																		$$ = NULL;
																		$$ = ListInsert($$,temp1);
																		$$ = ListInsert($$,temp2);
																		$$ = ListInsert($$,temp3);
																		$$ = LinkingLists($$,$4);
																		$$ = ListInsert($$,temp5);
																		

																	}
						| NEW ID LP RP								{
																		struct node* temp1 = malloc(sizeof(struct node));
																		temp1->string = malloc(strlen($1)+1);
																		strcpy(temp1->string,$1);
																		struct node* temp2 = malloc(sizeof(struct node));
																		temp2->string = malloc(strlen($2)+1);
																		strcpy(temp2->string,$2);
																		struct node* temp3 = malloc(sizeof(struct node));
																		temp3->string = malloc(strlen($3)+1);
																		strcpy(temp3->string,$3);
																		struct node* temp4 = malloc(sizeof(struct node));
																		temp4->string = malloc(strlen($4)+1);
																		strcpy(temp4->string,$4);
																		$$ = NULL;
																		$$ = ListInsert($$,temp1);
																		$$ = ListInsert($$,temp2);
																		$$ = ListInsert($$,temp3);
																		$$ = ListInsert($$,temp4);

																	}
						| NOT expression 							{
																		struct node* temp1 = malloc(sizeof(struct node));
																		temp1->string = malloc(strlen($1)+1);
																		strcpy(temp1->string,$1);
																		$$ = NULL;
																		$$ = ListInsert($$,temp1);
																		$$ = LinkingLists($$,$2);

																	}
						| LP expression RP      					{
																		struct node* temp1 = malloc(sizeof(struct node));
																		temp1->string = malloc(strlen($1)+1);
																		strcpy(temp1->string,$1);
																		struct node* temp3 = malloc(sizeof(struct node));
																		temp3->string = malloc(strlen($3)+1);
																		strcpy(temp3->string,$3);
																		
																		$$ = NULL;
																		$$ = ListInsert($$,temp1);
																		$$ = LinkingLists($$,$2);
																		$$ = ListInsert($$,temp3);
																		

																	}
						;

macroDefinition_star	:											
						| macroDefinition_star macroDefinition        			
						;


macroDefinition 		: macroDefExpression 												
						| macroDefStatement 							
						;

macroDefExpression		: MACRO ID LP arg_I RP LP expression RP 		 {
																			struct macro* x;
																			x = malloc(sizeof(struct macro));
																			x->name = malloc(strlen($2)+1);
																			strcpy(x->name,$2);
																			x->args = malloc(strlen($4)+1);
																			strcpy(x->args,$4);
																			x->statements = $7;
																			head = insert(head,x);	
																		}     	
						;

macroDefStatement		: MACRO ID LP arg_I RP LB statement_star RB 	{
																			struct macro* x;
																			x = malloc(sizeof(struct macro));
																			x->name = malloc(strlen($2)+1);
																			strcpy(x->name,$2);
																			x->args = malloc(strlen($4)+1);
																			strcpy(x->args,$4);
																			x->statements = $7;
																			head = insert(head,x);	
																		}

%%

main (int argc, char **argv) {
  yyparse();
  
}

yyerror (char *s) {
  fprintf(stderr, "error: %s\n", s);
}
